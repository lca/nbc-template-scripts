' Desc: BBC Polls Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 

Sub InitForm
	PositionChanged(Sender)
End Sub

' TRIGGERED WHEN THE NUMBER OF HEADSHOTS HAS BEEN CHANGED
Sub numberOfHeadshotDropdownChanged(Sender)
	if not CenterBigRadio.checked then
		if numberOfHeadshotDropdown.ItemIndex = 2 then
			numberOfHeadshotDropdown.ItemIndex = 1
			numberOfHeadshotDropdownChanged(Sender)
		end if
	end if

	NumberPollsControl.Value = numberOfHeadshotDropdown.ItemIndex

	SetVisibility "HeadshotPanel", true, 1, CInt(numberOfHeadshotDropdown.Text)
	SetVisibility "HeadshotPanel", false, CInt(numberOfHeadshotDropdown.Text) + 1, 4
End sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Text = ""
	next
End Sub

' TRIGGERED WHEN THE POSITIONING OF THE SCREEN ELEMENTS
Sub PositionChanged(Sender)
	centerSource.visible = false
	sideSource.visible = false

	if CenterBigRadio.checked then
		' set main layer active id
		MainLayerControl.ActiveLayerId = "1120x"
		
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2014x"
		
		' hide bg image panel
		BGImagePanel.visible = false
		
		' set side panels to out
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		
		ImageChoicePanel.visible = false
		
		titleTypeControl.Value = 1

		centerSource.visible = true
		
	elseif LeftRadio.checked then
		' set main layer active id
		MainLayerControl.ActiveLayerId = "1119x"
		
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2011x"
		
		RightPlateControl.ActiveLayerId = "2241x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		ImageChoicePanel.visible = true
		
		titleTypeControl.Value = 0
		
		if numberOfHeadshotDropdown.ItemIndex = 2 then
			numberOfHeadshotDropdown.ItemIndex = 1
			numberOfHeadshotDropdownChanged(Sender)
		end if

		sideSource.visible = true
	else
		' set main layer active id
		MainLayerControl.ActiveLayerId = "1119x"
		
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2012x"
		
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2251x"
		
		ImageChoicePanel.visible = true
		
		titleTypeControl.Value = 0
		
		if numberOfHeadshotDropdown.ItemIndex = 2 then
			numberOfHeadshotDropdown.ItemIndex = 1
			numberOfHeadshotDropdownChanged(Sender)
		end if

		sideSource.visible = true
	end if
	
	ImageChoiceChanged(Sender)
End Sub

' TRIGGERED WHEN THE OVER LIVE VIDEO CHECKBOX HAS BEEN CHANGED
Sub OverLiveVideoCheckboxClicked(Sender)
	if overLiveVideoCheckbox.checked then
		ImageChoicePanel.visible = false
		BGImagePanel.visible = false
		
		BPControl.ActiveLayerId = "2220x"
		BGControl.ActiveLayerId = "2000x"
		
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		
	else
		BGControl.ActiveLayerId = "2001x"
	
		if not CenterBigRadio.checked then
			ImageChoicePanel.visible = true
			ImageChoiceChanged(Sender)
		end if
	end if
End sub


Sub ImageChoiceChanged(Sender)
	if BGRadio.checked then
		if not CenterBigRadio.checked then
			firstHeadshotPanel.visible = false
			secondHeadshotPanel.visible = false
			HeadshotCountPanel.visible = false
			BGImagePanel.visible = true
			
			' set bp active layer id to IN
			BPControl.ActiveLayerId = "2221x"
			' set bg active layer id to IN
			BGControl.ActiveLayerId = "2001x"
			
			RightPlateControl.ActiveLayerId = "2240x"
			LeftPlateControl.ActiveLayerId = "2250x"
		end if
	else
		BGImagePanel.visible = false
		HeadshotCountPanel.visible = true
		
		BPControl.ActiveLayerId = "2220x"
		
		if LeftRadio.checked then
			RightPlateControl.ActiveLayerId = "2241x"
			LeftPlateControl.ActiveLayerId = "2250x"
		elseif RightRadio.checked then
			RightPlateControl.ActiveLayerId = "2240x"
			LeftPlateControl.ActiveLayerId = "2251x"
		end if
		
		select case HeadshotCountDrop.Text
			case "1"
				firstHeadshotPanel.visible = true
				secondHeadshotPanel.visible = false
				RightHeadshotCountControl.value = 0
				LeftHeadshotCountControl.value = 0
			case "2"
				firstHeadshotPanel.visible = true
				secondHeadshotPanel.visible = true
				RightHeadshotCountControl.value = 1
				LeftHeadshotCountControl.value = 1
		end select
	end if
	FirstHeadshotImageChanged(Sender)
	SecondHeadshotImageChanged(Sender)
End Sub

Sub ImageAsTitleCheckboxClicked(Sender)
	if imageAsTitleCheckbox.checked then
		' set omo
		imageAsTitleControl.value = 1
		
		' hide title panel
		TitlePanel.visible = false
		
		' show image title panel
		TitleImagePanel.visible = true
	else
		' set omo
		imageAsTitleControl.value = 0
		
		' show title panel
		TitlePanel.visible = true
		
		' hide image title panel
		TitleImagePanel.visible = false
	end if
End sub

' SETS ALL BAR DATA VALUES
Sub BarDataChanged(Sender)
	for i=1 to 4
		'FindComponent("BarDataControl" & i).Value = FindComponent("BarDataInput" & i).Value
		FindComponent("DataControl" & i).Value = FindComponent("BarDataInput" & i).Value
	next
	DataControlA1.Value = BarDataInput1.Value
End Sub

' TRIGGERED WHEN SECOND HEADSHOT IMAGE IS CHANGED
Sub SecondHeadshotImageChanged(Sender)
	if LeftRadio.checked then
		RightImageControl2.PicFilename = HeadshotImage2.PicFilename
		RightImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
	else
		LeftImageControl2.PicFilename = HeadshotImage2.PicFilename
		LeftImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
	end if
End Sub

' TRIGGERED WHEN FIRST HEADSHOT IMAGE IS CHANGED
Sub FirstHeadshotImageChanged(Sender)
	if LeftRadio.checked then
		RightImageControl1.PicFilename = HeadshotImage1.PicFilename
		RightImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
	else
		LeftImageControl1.PicFilename = HeadshotImage1.PicFilename
		LeftImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
	end if  
End Sub

