' Desc: Text Over Photo Template Script
	' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
	' Timestamp: 1411578075

Dim ptbState : ptbState = true
Dim debug: debug = true
debug = false

Sub InitForm

	ExitTextField(Sender)
	TextTypeChanged(Sender) 
End Sub

Function SC_SaveName
	SC_SaveName = "FS_TEXT OVER PHOTO - "
	if bigTextRadio.checked then
		SC_SaveName = SC_SaveName & "TEXT BOX"
	else
		SC_SaveName = SC_SaveName & "BIG TEXT"
	end if
	
	nextValue = GetSaveCount()
	
	' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
	' if the template has already been saved once, use the old prefix
		SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
	else
	' if the template is new then generate a new prefix
		SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
	end if
	
	' increments db sequence value
	IncrementSequenceValue()
end function

Sub TextTypeChanged(Sender)
	If textBoxRadio.checked Then
		textOptionsGroupBox.Visible = false
		textBoxMemo.Visible = true
		scalingPanel.Visible = false

		styleDrag.Value = 0
	else
		textOptionsGroupBox.Visible = true
		textBoxMemo.Visible = false
		scalingPanel.Visible = true
		styleDrag.Value = 1
	End If
End sub

' ================================================================================
' Triggered when either of the text options (light/dark) radio buttons are clicked
' ================================================================================
Sub TextBrightnessChanged(Sender)
	if lightRadioButton.checked then
		brightnessControl.Value = 0
	else
		brightnessControl.Value = 1
	end if
End Sub

' ================================================================================
' Triggered when the position or scaling has been changed.
' ================================================================================
Sub PositionScalingChanged(Sender)
	textPosition.XValue = positionTrackBarX.Position
	textPosition.YValue = positionTrackBarY.Position

	textScaling.XValue = (scalingTrackBar.Position/100)
	textScaling.YValue = textScaling.XValue
	scalingText.UTF8Text = scalingTrackBar.Position & "%"
End Sub

' ================================================================================
' Resets X Position on text
' ================================================================================
Sub ResetXPositionClick(Sender)
	positionTrackBarX.Position = 0

	PositionScalingChanged(Sender)
End sub

' ================================================================================
' Resets Y Position on text
' ================================================================================
Sub ResetYPositionClick(Sender)
	positionTrackBarY.Position = 0

	PositionScalingChanged(Sender)
End sub

'======================================================================================================
'===================================== PTB CONTROLS ===================================================
'======================================================================================================

Sub PTBButtonClick(Sender)
	if ptbState then
		if not debug then
			set wshShell = CreateObject("WSCript.shell")
			wshshell.run "PTPlugin.exe"
			set wshshell = nothing
		end if

		PTBButton.Picture = PTBPressedIcon.Picture
		PTBTimer.Enabled = true
	end if

End Sub

Sub PTBButtonRelease(Sender)
	PTBButton.Picture = PTBRegularIcon.Picture
	ptbState = true
	PTBTimer.Enabled = false
End sub

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	IncrementSequenceValue = CStr(newId)
End Function

' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================

' CHARACTER COUNT MONITOR
Sub CharsLeft(Sender)
	Dim componentNumber, componentDesc
	
	CharLeft.Caption = Sender.Hint & " - Characters Left" & ":  " & (Sender.MaxLength - 2) - Len(Sender.Text)

	if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
		CharLeft.Font.Color = clRed
	else
		CharLeft.Font.Color = clNavy
	end if
End Sub

Sub ExitTextField(Sender)
	CharLeft.Caption = "Text Character Count:"
	CharLeft.Font.Color = clInactiveCaption
End Sub
