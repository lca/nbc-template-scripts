' Desc: BBC Polls Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 

Sub InitForm
	OverLiveVideoCheckboxClicked(Sender)
	MainLayerControl.ActiveLayerId = "1121x"
	PositionChanged(Sender)
End Sub

' TRIGGERED WHEN THE NUMBER OF HEADSHOTS HAS BEEN CHANGED
Sub numberOfLinesDropdownChanged(Sender)
	NumberPollsControl.Value = numberOfLinesDropdown.ItemIndex
	NumberLinesOMOControl.Value = CInt(numberOfLinesDropdown.Text) - 1

	SetVisibility "TextLinePanel", true, 1, CInt(numberOfLinesDropdown.Text)
	SetVisibility "TextLinePanel", false, CInt(numberOfLinesDropdown.Text) + 1, 5
End sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Text = ""
	next
End Sub

' TRIGGERED WHEN THE POSITIONING OF THE SCREEN ELEMENTS
Sub PositionChanged(Sender)
	OMOSizeControl.value = 1
	
	SubtitleLabel.visible = false
	SubtitleBioText.visible = false
	
	if CenterBigRadio.checked then
	
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2014x"
		
		' hide bg image panel
		BGImagePanel.visible = false
		
		' set side panels to out
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		ImageChoicePanel.visible = false
		
		titleTypeControl.Value = 1
		
		OMOSizeControl.value = 0
		
		BPControl.ActiveLayerId = "2220x"
		
	elseif LeftRadio.checked then
		
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2011x"
		
		RightPlateControl.ActiveLayerId = "2241x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		ImageChoicePanel.visible = true
		
		titleTypeControl.Value = 0
	else
		
		' set plate position active layer id
		FSPlateControl.ActiveLayerId = "2012x"
		
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2251x"
		
		ImageChoicePanel.visible = true
		
		titleTypeControl.Value = 0
	end if
	
	ImageChoiceChanged(Sender)
End Sub

' TRIGGERED WHEN THE OVER LIVE VIDEO CHECKBOX HAS BEEN CHANGED
Sub OverLiveVideoCheckboxClicked(Sender)
	if overLiveVideoCheckbox.checked then
		ImageChoicePanel.visible = false
		BGImagePanel.visible = false
		
		BPControl.ActiveLayerId = "2220x"
		BGControl.ActiveLayerId = "2000x"
		
		RightPlateControl.ActiveLayerId = "2240x"
		LeftPlateControl.ActiveLayerId = "2250x"
		
		
	else
		BGControl.ActiveLayerId = "2001x"
	
		if not CenterBigRadio.checked then
			ImageChoicePanel.visible = true
			ImageChoiceChanged(Sender)
		end if
	end if
End sub


Sub ImageChoiceChanged(Sender)
	if BGRadio.checked then
		if not CenterBigRadio.checked then
			firstHeadshotPanel.visible = false
			secondHeadshotPanel.visible = false
			HeadshotCountPanel.visible = false
			BGImagePanel.visible = true
			
			' set bp active layer id to IN
			BPControl.ActiveLayerId = "2221x"
			' set bg active layer id to IN
			BGControl.ActiveLayerId = "2001x"
			
			RightPlateControl.ActiveLayerId = "2240x"
			LeftPlateControl.ActiveLayerId = "2250x"
		end if
	else
		BGImagePanel.visible = false
		HeadshotCountPanel.visible = true
		
		BPControl.ActiveLayerId = "2220x"
		
		if LeftRadio.checked then
			RightPlateControl.ActiveLayerId = "2241x"
			LeftPlateControl.ActiveLayerId = "2250x"
		elseif RightRadio.checked then
			RightPlateControl.ActiveLayerId = "2240x"
			LeftPlateControl.ActiveLayerId = "2251x"
		end if
		
		select case HeadshotCountDrop.Text
			case "1"
				firstHeadshotPanel.visible = true
				secondHeadshotPanel.visible = false
				RightHeadshotCountControl.value = 0
				LeftHeadshotCountControl.value = 0
			case "2"
				firstHeadshotPanel.visible = true
				secondHeadshotPanel.visible = true
				RightHeadshotCountControl.value = 1
				LeftHeadshotCountControl.value = 1
		end select
	end if
	
	
	if CenterBigRadio.checked then
		firstHeadshotPanel.visible = false
		secondHeadshotPanel.visible = false
	end if
	
	FirstHeadshotImageChanged(Sender)
	SecondHeadshotImageChanged(Sender)
End Sub

Sub ImageAsTitleCheckboxClicked(Sender)
	if imageAsTitleCheckbox.checked then
		' set omo
		imageAsTitleControl.value = 1
		
		' hide title panel
		TitlePanel.visible = false
		
		' show image title panel
		TitleImagePanel.visible = true
	else
		' set omo
		imageAsTitleControl.value = 0
		
		' show title panel
		TitlePanel.visible = true
		
		' hide image title panel
		TitleImagePanel.visible = false
	end if
End sub


' TRIGGERED WHEN SECOND HEADSHOT IMAGE IS CHANGED
Sub SecondHeadshotImageChanged(Sender)
	if LeftRadio.checked then
		RightImageControl2.PicFilename = HeadshotImage2.PicFilename
		RightImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
	else
		LeftImageControl2.PicFilename = HeadshotImage2.PicFilename
		LeftImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
	end if
End Sub

' TRIGGERED WHEN FIRST HEADSHOT IMAGE IS CHANGED
Sub FirstHeadshotImageChanged(Sender)
	if LeftRadio.checked then
		RightImageControl1.PicFilename = HeadshotImage1.PicFilename
		RightImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
	else
		LeftImageControl1.PicFilename = HeadshotImage1.PicFilename
		LeftImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
	end if  
End Sub

' TRIGGERED ON CLICK OF LAUNCH PBT
Sub PTBlauncherClick(Sender)
	set wshShell = CreateObject("WSCript.shell")
	wshshell.run "PTPlugin.exe"
	set wshshell = nothing
	
End sub

