Dim s0 : s0 = subjectText0.text
Dim s1 : s1 = subjectText1.text
Dim s2 : s2 = subjectText2.text

Dim curIndex
Dim curText
Dim Array0(12)
Dim Array1(12)
Dim Array2(12)

Sub SubjectTextExit(Sender)
	for i = 0 to 11
		curIndex = findcomponent("SubjectChoice" & cstr(i)).itemindex
		findcomponent("SubjectChoice" & cstr(i)).Items.Clear
		findcomponent("SubjectChoice" & cstr(i)).Items.add = ""
		for x = 0 to 2
			findcomponent("SubjectChoice" & cstr(i)).Items.add = findcomponent("SubjectText" & cstr(x)).text
		next
		findcomponent("SubjectChoice" & cstr(i)).itemindex =  curIndex
	next
	SubjectSelect(Sender)
End sub

Sub SubjectSelect(Sender)
	Dim currentSubjectComponent, prefix, currentDropIndex, lastPrefixIndex, content
	Dim lastDropIndex : lastDropIndex = SubjectChoice0.ItemIndex
	
	lastPrefixIndex = 0
	
' clear out main text fields
	SubjectDummy0.text = ""
	SubjectDummy1.text = ""
	SubjectDummy2.text = ""
	SubjectDummy0_0.text = ""
	SubjectDummy0_1.text = ""
	SubjectDummy0_2.text = ""
	
	for i = 0 to 11
		currentSubjectComponent = FindComponent("SubjectChoice" & i)
		currentDropIndex = currentSubjectComponent.ItemIndex
		content = FindComponent("TextLine" & i).Text
		
		if currentDropIndex < 0 then
			currentDropIndex = 0
		end if
		
' set the prefix string
		if currentDropIndex > 0 then
			prefix = FindComponent("SubjectText" & currentDropIndex - 1).Text & ": "
			lastPrefixIndex = currentDropIndex - 1
		else
			prefix = ""
		end if
		
' perform copying
		if currentDropIndex = 0 then
' same header as before, apply text to
			UpdateText lastPrefixIndex, prefix, content
		elseif currentDropIndex > 0 then
			UpdateText currentDropIndex - 1, prefix, content
		end if
	next
End sub

Sub textChange(Sender)
	SubjectSelect()
End sub

Sub UpdateText(index, prefix, content)
	FindComponent("SubjectDummy" & index).Text = FindComponent("SubjectDummy" & index).Text & prefix & content & vbNewLine
	FindComponent("SubjectDummy0_" & index).Text = FindComponent("SubjectDummy0_" & index).Text & prefix & vbNewLine
	for j = 0 to 2
		if j <> index then
			FindComponent("SubjectDummy" & j).Text = FindComponent("SubjectDummy" & j).Text & vbNewLine
			FindComponent("SubjectDummy0_" & j).Text = FindComponent("SubjectDummy0_" & j).Text & vbNewLine
		end if
	next
End Sub


