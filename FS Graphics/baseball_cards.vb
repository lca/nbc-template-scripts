' Desc: Baseball Card Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 121914T1116
' Edited: Lindy Zee -122914T1100

Dim ptbState : ptbState = false
Dim templateName : templateName = "FS_BASEBALL_CARDS"

' initialization
Sub InitForm
    BaseballCardsCountChange(Sender)
    DefaultBackgroundChanged(Sender)
    TitleTypeChanged(Sender)
    'DevTrigger()
    'AddHints()
    'SetMaxLengths 17, 32
End Sub

Function SC_SaveName
    Dim oldSaveNameSplit, nextValue, value

    SC_SaveName = "FS_BASEBALL CARDS (" & BaseballCardsCount.UTF8Text & ") "
    if imageTitleRadio.checked then
        SC_SaveName = SC_SaveName & "[TITLE IMAGE]"
    else
        SC_SaveName = SC_SaveName & titleText.Text
    end if

    nextValue = GetSaveCount()

    ' split old save name to get the generated # prefix
    oldSaveNameSplit = Split(SCV_OldSaveName, "_")

    if SCV_OldSaveName <> "" then
    ' if the template has already been saved once, use the old prefix
        SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
    else
    ' if the template is new then generate a new prefix
        SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
    end if

    ' increments db sequence value
    IncrementSequenceValue()
End Function

'======================================================================================
' =============================== EVENT TRIGGERS ======================================
'======================================================================================

' backdrop image selection has been made
Sub DefaultBackgroundChanged(Sender)
    BP1_LayerDrop.ActiveLayerID = "2220x"
    MAIN_BG.ActiveLayerID = "2000x"
    if middleSelect.checked then
        fsPlateLayerControl.ActiveLayerID ="2014x"

        LeftLayerDrop.ActiveLayerID = "2250x"
        RightLayerDrop.ActiveLayerID = "2240x"
        BP1_LayerDrop.ActiveLayerID = "2220x"
        LiveVideoChecked(Sender)
    elseif leftRadio.checked and not backgroundSelect.checked then
        fsPlateLayerControl.ActiveLayerID ="2011x"
        sideImageCache.ControlObjectName = "R_IMAGE1"
        BP1_LayerDrop.ActiveLayerID = "2220x"
        LeftLayerDrop.ActiveLayerID = "2250x"
        RightLayerDrop.ActiveLayerID = "2241x"
        MAIN_BG.ActiveLayerID = "2001x"

        sideImagePanel.visible = true
        bgImagePanel.visible = false
    elseif rightRadio.checked and not backgroundSelect.checked then

        fsPlateLayerControl.ActiveLayerID ="2012x"
        sideImageCache.ControlObjectName = "L_IMAGE1"
        BP1_LayerDrop.ActiveLayerID = "2220x"
        RightLayerDrop.ActiveLayerID = "2240x"
        LeftLayerDrop.ActiveLayerID = "2251x"
        MAIN_BG.ActiveLayerID = "2001x"

        sideImagePanel.visible = false
        bgImagePanel.visible = true
    end if

    if backgroundSelect.checked and not middleSelect.checked then
        BP1_LayerDrop.ActiveLayerID = "2221x"
        RightLayerDrop.ActiveLayerID = "2240x"
        LeftLayerDrop.ActiveLayerID = "2250x"
        MAIN_BG.ActiveLayerID = "2001x"

        sideImagePanel.visible = false
        bgImagePanel.visible = true
    end if
End sub

Sub LiveVideoChecked(Sender)
    if overLiveVideoCheckbox.checked then
        MAIN_BG.ActiveLayerID = "2000x"
    else
        MAIN_BG.ActiveLayerID = "2001x"
    end if
End Sub

Sub TitleTypeChanged(Sender)
        positionControl.YValue = "20"
        if imageTitleRadio.checked then
            titleTypeGroupBox.visible = true

            titleOff.checked = true
            textOrImage.Value = 1
            titleText.visible = false
            'titleTypeGroupBox.visible = false  (Commented out by Lindy)
            titleImage.visible = true
            textOrImage.Text = "1"
            positionControl.YValue = "0"
            titleTypeGroupBox.Caption = "Drag Masthead Into This Panel"
            titleTypeGroupBox.Font.Color = clWhite

        elseif textTitleRadio.checked then
            titleTypeGroupBox.visible = true
            titleTextControl.Text = titleText.Text

            titleTypeGroupBox.Caption = "Title - Characters Left: " & (titleText.MaxLength - 2) - Len(titleText.Text)

            if titleText.Text = "" then
                titleOff.checked = false
            else
                positionControl.YValue = "0"
                titleOff.checked = true
            end if

            titleText.visible = true
            titleTypeGroupBox.visible = true
            titleImage.visible = false
            textOrImage.Text = "0"

            if (titleText.MaxLength - 2) - Len(titleText.Text) < 0 then
                titleTypeGroupBox.Font.Color = clRed
            end if

        else
            titleTypeGroupBox.visible = false
            titleOff.checked = false
            titleTextControl.Text = ""
        end if

        PlatePositionChanged(Sender)
End Sub

' Set current active layer id
Sub PlatePositionChanged(Sender)
    MAIN_BG.ActiveLayerId = "2001x"
    if sideSelect.checked then
        titleTypeOmo.text = "0"
        PTBButton.Picture = PTBRegularIcon.Picture
        ptbState = true
        ChangeDefaultBackgroundPanel.visible = true
        FS_2Drop.ActiveLayerID = "1106x"

            if rightRadio.checked then
                fsPlateLayerControl.ActiveLayerID ="2012x"

                'middleOrSideOmo.text = "0"
                if BaseballCardsCount.itemindex > 4 then
                    BaseballCardsCount.itemindex = 4
                end if
            else
                fsPlateLayerControl.ActiveLayerID ="2011x"
                'middleOrSideOmo.text = "0"
                if BaseballCardsCount.itemindex > 4 then
                    BasebaallCardsCount.itemindex = 4
                end if

        end if
    else
        if BaseballCardsCount.ItemIndex = 0 and not imageTitleRadio.checked then
            PTBButton.Picture = PTBRestingIcon.Picture
            ptbState = false
        else
            PTBButton.Picture = PTBRegularIcon.Picture
            ptbState = true
        end if
        ChangeDefaultBackgroundPanel.visible = false
        fsPlateLayerControl.ActiveLayerID ="2014x"
        titleTypeOmo.text = "1"
        'middleOrSideOmo.text = "1"
        'BackdropPanel.Visible = false
        MAIN_BG.ActiveLayerId = "2000x"

        if BaseballCardsCount.ItemIndex > 6 then
            FS_2Drop.ActiveLayerID = "1107x"
        else
            FS_2Drop.ActiveLayerID = "1105x"
        end if
    end if

    DefaultBackgroundChanged(Sender)

    ' set visibilities on cards
    if BaseballCardsCount.ItemIndex <> 0 then
        launchInstructionPanel.visible = true
        SetCardVisibilities (CInt(BaseballCardsCount.UTF8Text) + 1), 12
    else
        launchInstructionPanel.visible = false
        CharLeft.Caption = "Baseball Card Character Count:"
        CharLeft.Font.Color = clInactiveCaption
        SetCardVisibilities 1, 12
    end if
End sub

' visible cards number changed event
Sub BaseballCardsCountChange(Sender)
    if BaseballCardsCount.ItemIndex <> 0 then
        numberHSOmo.text = BaseballCardsCount.itemindex
        PTBButton.Picture = PTBRegularIcon.Picture
        ptbState = true
        middleSelect.Visible = 1

        ' set panel visibilities
        if CInt(BaseballCardsCount.UTF8Text) > 4 then
           'PositionPanel.Visible = 0
            PositionPanel.Enabled = false
            PositionLabel.Font.Color = clInactiveCaption
            middleSelect.Font.Color = clInactiveCaption
            sideSelect.Font.Color = clInactiveCaption
            middleSelect.checked = true
        else
            'PositionPanel.Visible = 1
            PositionPanel.Enabled = true
            PositionLabel.Font.Color = clBlack
            middleSelect.Font.Color = clBlack
            sideSelect.Font.Color = clBlack
        end if

        PositionBaseballCards(BaseballCardsCount.UTF8Text)

    else
        PTBButton.Picture = PTBRestingIcon.Picture
        ptbState = false
    end if

    PlatePositionChanged(Sender)
End sub

Sub PositionBaseballCards(numCards)
    select case numCards
        case "1"
            SetObjectParameters "BaseballCardImage001", 235, 73, 200, 200
            SetObjectParameters "BaseballCardLineTextB001", 287, 287, "", 110
            SetObjectParameters "BaseballCardLineTextA001", 310, 287, "", 110
            SetObjectParameters "BaseballCardLineTextC001", 333, 287, "", 110
            SetObjectParameters "BaseballCardLineTextD001", 356, 287, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 467, 17, 41, 475

        case "2"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 73, 150, 150
            SetObjectParameters "BaseballCardLineTextB001", 392, 95, "", 110
            SetObjectParameters "BaseballCardLineTextA001", 415, 95, "", 110
            SetObjectParameters "BaseballCardLineTextC001", 438, 95, "", 110
            SetObjectParameters "BaseballCardLineTextD001", 461, 95, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 260, 150, 150
            SetObjectParameters "BaseballCardLineTextB002", 392, 280, "", 110
            SetObjectParameters "BaseballCardLineTextA002", 415, 280, "", 110
            SetObjectParameters "BaseballCardLineTextC002", 438, 280, "", 110
            SetObjectParameters "BaseballCardLineTextD002", 461, 280, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "3"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 8, 150, 150
            SetObjectParameters "BaseballCardLineTextB001", 392, 30, "", 110
            SetObjectParameters "BaseballCardLineTextA001", 415, 30, "", 110
            SetObjectParameters "BaseballCardLineTextC001", 438, 30, "", 110
            SetObjectParameters "BaseballCardLineTextD001", 461, 30, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 170, 150, 150
            SetObjectParameters "BaseballCardLineTextB002", 392, 190, "", 110
            SetObjectParameters "BaseballCardLineTextA002", 415, 190, "", 110
            SetObjectParameters "BaseballCardLineTextC002", 438, 190, "", 110
            SetObjectParameters "BaseballCardLineTextD002", 461, 190, "", 110

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 332, 150, 150
            SetObjectParameters "BaseballCardLineTextB003", 392, 354, "", 110
            SetObjectParameters "BaseballCardLineTextA003", 415, 354, "", 110
            SetObjectParameters "BaseballCardLineTextC003", 438, 354, "", 110
            SetObjectParameters "BaseballCardLineTextD003", 461, 354, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "4"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 27, 100, 100
            SetObjectParameters "BaseballCardLineTextB001", 240, 134, "", 110
            SetObjectParameters "BaseballCardLineTextA001", 262, 134, "", 110
            SetObjectParameters "BaseballCardLineTextC001", 284, 134, "", 110
            SetObjectParameters "BaseballCardLineTextD001", 306, 134, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 255, 100, 100
            SetObjectParameters "BaseballCardLineTextB002", 240, 362, "", 110
            SetObjectParameters "BaseballCardLineTextA002", 262, 362, "", 110
            SetObjectParameters "BaseballCardLineTextC002", 284, 362, "", 110
            SetObjectParameters "BaseballCardLineTextD002", 306, 362, "", 110

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 360, 27, 100, 100
            SetObjectParameters "BaseballCardLineTextB003", 365, 134, "", 110
            SetObjectParameters "BaseballCardLineTextA003", 387, 134, "", 110
            SetObjectParameters "BaseballCardLineTextC003", 409, 134, "", 110
            SetObjectParameters "BaseballCardLineTextD003", 431, 134, "", 110

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 360, 255, 100, 100
            SetObjectParameters "BaseballCardLineTextB004", 365, 362, "", 110
            SetObjectParameters "BaseballCardLineTextA004", 387, 362, "", 110
            SetObjectParameters "BaseballCardLineTextC004", 409, 362, "", 110
            SetObjectParameters "BaseballCardLineTextD004", 431, 362, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "5"

            ' Head 1
            SetObjectParameters "BaseballCardImage001", 238, 1, 80, 80
            SetObjectParameters "BaseballCardLineTextB001", 235, 84, "", 80
            SetObjectParameters "BaseballCardLineTextA001", 257, 84, "", 80
            SetObjectParameters "BaseballCardLineTextC001", 279, 84, "", 80
            SetObjectParameters "BaseballCardLineTextD001", 301, 84, "", 80

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 238, 166, 80, 80
            SetObjectParameters "BaseballCardLineTextB002", 235, 249, "", 80
            SetObjectParameters "BaseballCardLineTextA002", 257, 249, "", 80
            SetObjectParameters "BaseballCardLineTextC002", 279, 249, "", 80
            SetObjectParameters "BaseballCardLineTextD002", 301, 249, "", 80

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 238, 334, 80, 80
            SetObjectParameters "BaseballCardLineTextB003", 235, 417, "", 80
            SetObjectParameters "BaseballCardLineTextA003", 257, 417, "", 80
            SetObjectParameters "BaseballCardLineTextC003", 279, 417, "", 80
            SetObjectParameters "BaseballCardLineTextD003", 301, 417, "", 80

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 378, 88, 80, 80
            SetObjectParameters "BaseballCardLineTextB004", 375, 171, "", 80
            SetObjectParameters "BaseballCardLineTextA004", 397, 171, "", 80
            SetObjectParameters "BaseballCardLineTextC004", 419, 171, "", 80
            SetObjectParameters "BaseballCardLineTextD004", 441, 171, "", 80

            ' Head 5
            SetObjectParameters "BaseballCardImage005", 378, 253, 80, 80
            SetObjectParameters "BaseballCardLineTextB005", 375, 338, "", 80
            SetObjectParameters "BaseballCardLineTextA005", 397, 338, "", 80
            SetObjectParameters "BaseballCardLineTextC005", 419, 338, "", 80
            SetObjectParameters "BaseballCardLineTextD005", 441, 338, "", 80

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "6"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 238, 1, 80, 80
            SetObjectParameters "BaseballCardLineTextB001", 235, 84, "", 80
            SetObjectParameters "BaseballCardLineTextA001", 257, 84, "", 80
            SetObjectParameters "BaseballCardLineTextC001", 279, 84, "", 80
            SetObjectParameters "BaseballCardLineTextD001", 301, 84, "", 80

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 238, 166, 80, 80
            SetObjectParameters "BaseballCardLineTextB002", 235, 249, "", 80
            SetObjectParameters "BaseballCardLineTextA002", 257, 249, "", 80
            SetObjectParameters "BaseballCardLineTextC002", 279, 249, "", 80
            SetObjectParameters "BaseballCardLineTextD002", 301, 249, "", 80

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 238, 334, 80, 80
            SetObjectParameters "BaseballCardLineTextB003", 235, 417, "", 80
            SetObjectParameters "BaseballCardLineTextA003", 257, 417, "", 80
            SetObjectParameters "BaseballCardLineTextC003", 279, 417, "", 80
            SetObjectParameters "BaseballCardLineTextD003", 301, 417, "", 80

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 363, 1, 80, 80
            SetObjectParameters "BaseballCardLineTextB004", 361, 84, "", 80
            SetObjectParameters "BaseballCardLineTextA004", 383, 84, "", 80
            SetObjectParameters "BaseballCardLineTextC004", 405, 84, "", 80
            SetObjectParameters "BaseballCardLineTextD004", 427, 84, "", 80

            ' Head 5
            SetObjectParameters "BaseballCardImage005", 363, 166, 80, 80
            SetObjectParameters "BaseballCardLineTextB005", 361, 249, "", 80
            SetObjectParameters "BaseballCardLineTextA005", 383, 249, "", 80
            SetObjectParameters "BaseballCardLineTextC005", 405, 249, "", 80
            SetObjectParameters "BaseballCardLineTextD005", 427, 249, "", 80

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 363, 334, 80, 80
            SetObjectParameters "BaseballCardLineTextB006", 361, 417, "", 80
            SetObjectParameters "BaseballCardLineTextA006", 383, 417, "", 80
            SetObjectParameters "BaseballCardLineTextC006", 405, 417, "", 80
            SetObjectParameters "BaseballCardLineTextD006", 427, 417, "", 80

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "7"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 8, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 135, "", 110

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 255, "", 110

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 319, 375, "", 110

            ' Head 5
            SetObjectParameters "BaseballCardImage005", 360, 86, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 445, 69, "", 110

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 360, 211, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 445, 196, "", 110

            ' Head 7
            SetObjectParameters "BaseballCardImage007", 360, 332, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 445, 316, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "8"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 8, "", 110

         ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 135, "", 110

         ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 255, "", 110

         ' Head 4
            SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 319, 375, "", 110

         ' Head 5
            SetObjectParameters "BaseballCardImage005", 369, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 453, 8, "", 110

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 369, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 453, 135, "", 110

            ' Head 7
            SetObjectParameters "BaseballCardImage007", 369, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 453, 255, "", 110

         ' Head 8
            SetObjectParameters "BaseballCardImage008", 369, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA008", 453, 375, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 500, 17, 41, 475

        case "9"
         ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 86, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 69, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 211, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 196, "", 110

         ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 332, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 316, "", 110

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 370, 86, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 453, 69, "", 110

         ' Head 5
            SetObjectParameters "BaseballCardImage005", 370, 211, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 453, 196, "", 110

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 370, 332, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 453, 316, "", 110

         ' Head 7
            SetObjectParameters "BaseballCardImage007", 501, 86, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 584, 69, "", 110

            ' Head 8
            SetObjectParameters "BaseballCardImage008", 501, 211, 80, 80
            SetObjectParameters "BaseballCardLineTextA008", 584, 196, "", 110

            ' Head 9
            SetObjectParameters "BaseballCardImage009", 501, 332, 80, 80
            SetObjectParameters "BaseballCardLineTextA009", 584, 316, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 635, 17, 41, 475

        case "10"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 8, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 135, "", 110

         ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 255, "", 110

         ' Head 4
            SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 319, 375, "", 110

         ' Head 5
            SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 453, 8, "", 110

         ' Head 6
            SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 453, 135, "", 110

            ' Head 7
            SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 453, 255, "", 110

            ' Head 8
            SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA008", 453, 375, "", 110

         ' Head 9
            SetObjectParameters "BaseballCardImage009", 501, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA009", 584, 135, "", 110

            ' Head 10
            SetObjectParameters "BaseballCardImage010", 501, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA010", 584, 255, "", 110

         'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 635, 17, 41, 475

        case "11"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 8, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 135, "", 110

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 255, "", 110

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 319, 375, "", 110

             ' Head 5
                SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 453, 8, "", 110

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 453, 135, "", 110

            ' Head 7
            SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 453, 255, "", 110

                ' Head 8
            SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA008", 453, 375, "", 110

          ' Head 9
            SetObjectParameters "BaseballCardImage009", 501, 86, 80, 80
            SetObjectParameters "BaseballCardLineTextA009", 584, 69, "", 110

         ' Head 10
            SetObjectParameters "BaseballCardImage010", 501, 211, 80, 80
            SetObjectParameters "BaseballCardLineTextA010", 584, 196, "", 110

            ' Head 11
            SetObjectParameters "BaseballCardImage011", 501, 332, 80, 80
            SetObjectParameters "BaseballCardLineTextA011", 584, 316, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 635, 17, 41, 475

        case "12"
            ' Head 1
            SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA001", 319, 8, "", 110

            ' Head 2
            SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA002", 319, 135, "", 110

            ' Head 3
            SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA003", 319, 255, "", 110

            ' Head 4
            SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA004", 319, 375, "", 110

            ' Head 5
            SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA005", 453, 8, "", 110

            ' Head 6
            SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA006", 453, 135, "", 110

            ' Head 7
            SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA007", 453, 255, "", 110

         ' Head 8
            SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA008", 453, 375, "", 110

            ' Head 9
            SetObjectParameters "BaseballCardImage009", 501, 25, 80, 80
            SetObjectParameters "BaseballCardLineTextA009", 584, 8, "", 110

            ' Head 10
            SetObjectParameters "BaseballCardImage010", 501, 150, 80, 80
            SetObjectParameters "BaseballCardLineTextA010", 584, 135, "", 110

         ' Head 11
            SetObjectParameters "BaseballCardImage011", 501, 270, 80, 80
            SetObjectParameters "BaseballCardLineTextA011", 584, 255, "", 110

            ' Head 12
            SetObjectParameters "BaseballCardImage012", 501, 390, 80, 80
            SetObjectParameters "BaseballCardLineTextA012", 584, 375, "", 110

            'Instructions Panel
            SetObjectParameters "launchInstructionPanel", 635, 17, 41, 475
    end select
End Sub
' =============================== HELPER FUNCTIONS ========================================

' Makes visible any cards from 0->start, hides all cards from start->finish
Sub SetCardVisibilities(start, finish)
    prefix = ""
    visibility = 1
    objectName = ""
    for i=1 to finish
        ' if a single digit number prepend a "0"
        if i < 10 then
            prefix = "0"
        else
            prefix = ""
        end if

        ' if reached the start index then disable visibility
        if i < start then
            visibility = 1
            'msgbox "show: " & prefix & i
        else
            visibility = 0
            'msgbox "hide: " & prefix & i

            ' hide elements by setting width to zero
            ' width
            objectName = "BaseballCardLineTextA0" & prefix & i
            SetObjectParameters objectName, "", "", "", 0

            objectName = "BaseballCardLineTextB0" & prefix & i
            SetObjectParameters objectName, "", "", "", 0

            objectName = "BaseballCardImage0" & prefix & i
            SetObjectParameters objectName, "", "", "", 0

            if i < 7 then
                objectName = "BaseballCardLineTextC0" & prefix & i
                SetObjectParameters objectName, "", "", "", 0

                objectName = "BaseballCardLineTextD0" & prefix & i
                SetObjectParameters objectName, "", "", "", 0
            end if
        end if

        ' image
        FindComponent("BaseballCardImage0" & prefix & i).Visible = visibility
        FindComponent("BaseballCardImage0" & prefix & i).Enabled = visibility
        ' fname
        FindComponent("BaseballCardLineTextA0" & prefix & i).Visible = visibility
        FindComponent("BaseballCardLineTextA0" & prefix & i).EnableSpellcheck = visibility
        ' lname
        FindComponent("BaseballCardLineTextB0" & prefix & i).Visible = visibility
        FindComponent("BaseballCardLineTextB0" & prefix & i).EnableSpellcheck = visibility

        ' dropline
        if i < 7 then
            FindComponent("BaseballCardLineTextC0" & prefix & i).Visible = visibility
            FindComponent("BaseballCardLineTextC0" & prefix & i).EnableSpellcheck = visibility
            FindComponent("BaseballCardLineTextD0" & prefix & i).EnableSpellcheck = visibility
        end if

        if BaseballCardsCount.ItemIndex <> 0 then
            if (CInt(BaseballCardsCount.UTF8Text)) >= 7 then
                objectName = "BaseballCardLineTextB0" & prefix & i
                SetObjectParameters objectName, "", "", "", 0

                if i < 7 then
                    objectName = "BaseballCardLineTextC0" & prefix & i
                    SetObjectParameters objectName, "", "", "", 0

                    objectName = "BaseballCardLineTextD0" & prefix & i
                    SetObjectParameters objectName, "", "", "", 0
                end if
            end if
        end if
    next
End Sub

' set specific object properties (top, left, height, width)
Sub SetObjectParameters(objectName, top, left, height, width)
    'msgbox objectName
    if top <> "" then
        FindComponent(objectName).Top = top
    end if
    if left <> "" then
        FindComponent(objectName).Left = left
    end if
    if height <> "" then
        FindComponent(objectName).Height = height
    end if
    if width <> "" then
        if width = 0 then
            FindComponent(objectName).Visible = 0
            if InStr(objectName, "BaseballCardLineText") then
                FindComponent(objectName).EnableSpellcheck = 0
            end if
        else
            FindComponent(objectName).Width = width
            FindComponent(objectName).Visible = 1
            if InStr(objectName, "BaseballCardLineText") then
                FindComponent(objectName).EnableSpellcheck = 1
            end if
        end if

    end if
end sub

Sub UpdateCases(Sender)
    ' list of text fields referenced by 'global' functions
    Dim textFields : textFields = Array("BaseballCardLineTextA001", "BaseballCardLineTextB001", _
                                        "BaseballCardLineTextC001", "BaseballCardLineTextD001", _
                                        "BaseballCardLineTextA002", "BaseballCardLineTextB002", _
                                        "BaseballCardLineTextC002", "BaseballCardLineTextD002", _
                                        "BaseballCardLineTextA003", "BaseballCardLineTextB003", _
                                        "BaseballCardLineTextC003", "BaseballCardLineTextD003", _
                                        "BaseballCardLineTextA004", "BaseballCardLineTextB004", _
                                        "BaseballCardLineTextC004", "BaseballCardLineTextD004", _
                                        "BaseballCardLineTextA005", "BaseballCardLineTextB005", _
                                        "BaseballCardLineTextC005", "BaseballCardLineTextD005", _
                                        "BaseballCardLineTextA006", "BaseballCardLineTextB006", _
                                        "BaseballCardLineTextC006", "BaseballCardLineTextD006", _
                                        "BaseballCardLineTextA007", "BaseballCardLineTextA008", _
                                        "BaseballCardLineTextA009", "BaseballCardLineTextA010", _
                                        "BaseballCardLineTextA011", "BaseballCardLineTextA012")

    Dim caseType : caseType = -1
    if upperCaseRadio.checked then
        caseType = 1
    elseif normalCaseRadio.checked then
        caseType = 0
    end if

    for i = 0 to UBound(textFields)
        FindComponent(textFields(i)).CharCase = caseType
        if caseType = -1 then
            FindComponent(textFields(i)).Text = LCase(FindComponent(textFields(i)).Text)
        end if

        'logText.Text = logText.Text & textFields(i) & vbNewLine
    next

    ' title/subtitles/source
    if includeMainTextsCheckbox.checked then
        titleText.CharCase = caseType
        if caseType = -1 then
            titleText.Text = LCase(titleText.Text)
        end if
    end if
End Sub

Sub PTBbuttonClick(Sender)
    if ptbState then
        set wshShell = CreateObject("WSCript.shell")
        wshshell.run "PTPlugin.exe"
        set wshshell = nothing

        PTBButton.Picture = PTBPressedIcon.Picture
        PTBTimer.Enabled = true
    end if

End Sub

Sub PTBButtonRelease(Sender)
    PTBButton.Picture = PTBRegularIcon.Picture
    ptbState = true
    PTBTimer.Enabled = false
End sub
'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

'SAVE NAME CONVENTION:
'Template Name + Number of Pages + Title + Subtitle'

Function SC_SaveName
        Dim prefix : prefix = ""
        SC_SaveName = templateName

        if textTitleRadio.checked then
                SC_SaveName = SC_SaveName & " " & CStr(BaseballCardsCount.ItemIndex) & " " & titleText.Text
        else
                SC_SaveName = SC_SaveName & " " & CStr(BaseballCardsCount.ItemIndex)
        end if

        nextValue = GetSaveCount()

' split old save name to get the generated # prefix
        oldSaveNameSplit = Split(SCV_OldSaveName, "_")

        if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
                SC_SaveName = oldSaveNameSplit(0) & "_" & oldSaveNameSplit(1) & "_" & SC_SaveName
                newTemplate = false
        else
' if the template is new then generate a new prefix
            nextValue = nextValue + 1

            if Len(nextValue) < 5 then
                select case Len(nextValue)
                    case 1
                        prefix = "0000"
                    case 2
                        prefix = "000"
                    case 3
                        prefix = "00"
                    case 4
                        prefix = "0"
                end select
            end if

            SC_SaveName = GetWeekday() & "_" & prefix & nextValue & "_" & SC_SaveName
        end if

' increments db sequence value
        'IncrementSequenceValue()
end function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "00001"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                newId = recordset("ID")
        end if

        GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "00001"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                newId = recordset("ID")
        end if

        IncrementSequenceValue = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
    Dim number : number = ""
    Dim prefix : prefix = "_"
    if Len(savedOver) = 0 or savedOver = True then
        'msgbox CStr(saveId) & " - OVERWRITE HIT"
    elseif savedOver = false then
        'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
        
        
        ' if not a new template then overwrite the recently saved element with a 
        if not newTemplate then
            oldSaveNameSplit = Split(SCV_OldSaveName, "_")
            oldSaveName = SCV_OldSaveName
            number = CStr(GetSaveCount() + 1)
            
            if Len(number) < 5 then
                select case Len(number)
                    case 1
                        prefix = "_0000"
                    case 2
                        prefix = "_000"
                    case 3
                        prefix = "_00"
                    case 4
                        prefix = "_0"
                end select
            end if

           if textTitleRadio.checked then
                newSaveName = GetWeekday() & prefix & number & "_" & templateName & " " & CStr(BaseballCardsCount.ItemIndex) & " " & titleText.Text
            else
                newSaveName = GetWeekday() & prefix & number & "_" & templateName & " " & CStr(BaseballCardsCount.ItemIndex)
            end if

            
            
            'msgbox "Save As New hit on a previously saved template, update id"
            RenameDatabaseEntry newSaveName, oldSaveName
        end if
        
        IncrementSequenceValue()
    end if

End Sub

Function RenameDatabaseEntry(newSaveName, oldSaveName)
    connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

    Set DBConnection = CreateObject("ADODB.Connection")
    DBConnection.Open connectStr
    
    ' get id of the most recently saved element that matches this description
    Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
    

    Set recordset = DBConnection.Execute(Query)

    if not recordset.EOF then
        newId = recordset("DNR")
        
        'msgbox "FOUND DNR: " & newId
        ' update that element
        Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
        
        msgbox "QUERY: " & Query
        
        DBConnection.Execute(Query)
    end if
End Function

' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
    Dim dayAbbv : dayAbbv = Array("N/A", "SU", "MO", "TU", "WE", "TH", "FR", "SA")
    GetWeekday = dayAbbv(Weekday(Date))
End Function

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
    Dim comp
    for i=start to finish
        comp = FindComponent(componentName & CStr(i))
        ConnectVTWEvent [_scripter], comp, eventType, eventName
    next
End Sub

' CHARACTER COUNT MONITOR
Sub CharsLeft(Sender)
    Dim componentNumber, componentDesc
    if InStr(Sender.Name, "BaseballCardLineText") then
        componentNumber = Right(Sender.Name, 2)
        if Mid(componentNumber, 1, 1) = "0" then
            componentNumber = Replace(componentNumber, "0", "")
        end if
        if InStr(Sender.Name, "TextA") then
            componentDesc = "Last Name"
        elseif InStr(Sender.Name, "TextB") then
            componentDesc = "First Name"
        elseif InStr(Sender.Name, "TextC") then
            componentDesc = "Dropline 1"
        else
            componentDesc = "Dropline 2"
        end if

        CharLeft.Caption = " Card #" & componentNumber & " " & componentDesc  & " Characters Left" & ":  " & (Sender.MaxLength - 2) - Len(Sender.Text)

        if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
            CharLeft.Font.Color = clRed
        else
            CharLeft.Font.Color = clNavy
        end if
    else
        if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
            titleTypeGroupBox.Font.Color = clRed
        else
            titleTypeGroupBox.Font.Color = clWhite
        end if

        titleTypeGroupBox.Caption = "Title - Characters Left: " & (Sender.MaxLength - 2) - Len(Sender.Text)

        TitleTypeChanged(Sender)
    end if
End Sub

' DEV TEST FUNCTION - IGNORE
Sub DevTrigger()
    ApplyEvent "BaseballCardLineTextA00", "CharsLeft", "OnEnter", 1, 9
    ApplyEvent "BaseballCardLineTextB00", "CharsLeft", "OnEnter", 1, 9
    ApplyEvent "BaseballCardLineTextA0", "CharsLeft", "OnEnter", 10, 12
    ApplyEvent "BaseballCardLineTextB0", "CharsLeft", "OnEnter", 10, 12
    ApplyEvent "BaseballCardLineTextC00", "CharsLeft", "OnEnter", 1, 6
    ApplyEvent "BaseballCardLineTextD00", "CharsLeft", "OnEnter", 1, 6

     ApplyEvent "BaseballCardLineTextA00", "CharsLeft", "OnEnter", 1, 9
    ApplyEvent "BaseballCardLineTextB00", "CharsLeft", "OnEnter", 1, 9
    ApplyEvent "BaseballCardLineTextA0", "CharsLeft", "OnEnter", 10, 12
    ApplyEvent "BaseballCardLineTextB0", "CharsLeft", "OnEnter", 10, 12
    ApplyEvent "BaseballCardLineTextC00", "CharsLeft", "OnEnter", 1, 6
    ApplyEvent "BaseballCardLineTextD00", "CharsLeft", "OnEnter", 1, 6
End Sub

' ADD HINTS
Sub AddHints()
    Dim prefix : prefix = "00"
    CharLeft.ShowHint = true

    ' loop through all elements that require hints and add/enable them
    for i = 1 to 12
        if i > 9 then
            prefix = "0"
        end if

        if i < 7 then
            FindComponent("BaseballCardLineTextC" & prefix & i).ShowHint = true
            FindComponent("BaseballCardLineTextD" & prefix & i).ShowHint = true

            FindComponent("BaseballCardLineTextC" & prefix & i).hint = "Enter Dropline 1 Content Here"
            FindComponent("BaseballCardLineTextD" & prefix & i).hint = "Enter Dropline 2 Content Here"
        end if

        FindComponent("BaseballCardImage" & prefix & i).ShowHint = true

        FindComponent("BaseballCardLineTextB" & prefix & i).ShowHint = true
        FindComponent("BaseballCardLineTextA" & prefix & i).ShowHint = true

        FindComponent("BaseballCardLineTextB" & prefix & i).hint = "Enter First Name Content Here"
        FindComponent("BaseballCardLineTextA" & prefix & i).hint = "Enter Last Name Content Here"
    next
End Sub

' SET MAX LENGTHS
Sub SetMaxLengths(maxLength, titleLength)
    Dim prefix : prefix = "00"

    titleText.MaxLength = titleLength
    ' loop through all text elements to set max lengths
    for i = 1 to 12
        if i > 9 then
            prefix = "0"
        end if

        if i < 7 then
            FindComponent("BaseballCardLineTextD" & prefix & i).MaxLength = maxLength
            FindComponent("BaseballCardLineTextC" & prefix & i).MaxLength = maxLength
        end if

        FindComponent("BaseballCardLineTextB" & prefix & i).MaxLength = maxLength
        FindComponent("BaseballCardLineTextA" & prefix & i).MaxLength = maxLength
    next
End Sub
