' Desc: Bar Graph Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411576578

largeValues = ""
prefix = 0

Dim politicalColorMap, defaultColorMap, previouslySaved, previousSaveName, oldSaveId

previouslySaved = false
previousSaveName = ""
oldSaveId = 0

' Called on initialization, creates dictionary of color codes
Sub InitForm
	PiesPositionRadio(Sender)
	
' build political color map (dictionary)
	Set politicalColorMap = CreateObject("Scripting.Dictionary")
	politicalColorMap.Add "DARK_BLUE1", "0.066667 0.423529 0.635294"
	politicalColorMap.Add "DARK_BLUE2", "0.062745 0.243137 0.415686"
	politicalColorMap.Add "GRAY", "0.682353 0.686275 0.686275"
	politicalColorMap.Add "DARK_GRAY1", "0.368627 0.372549 0.372549"
	politicalColorMap.Add "DARK_GRAY2", "0.168627 0.172549 0.168627"
	politicalColorMap.Add "DARK_RED1", "0.658824 0.121569 0.184314"
	politicalColorMap.Add "DARK_RED2", "0.458824 0.062745 0.094118"
	politicalColorMap.Add "RED", "0.796078 0.400000 0.458824"
	politicalColorMap.Add "BLUE", "0.470588 0.721569 0.807843"
	
' build default color map (dictionary)
	Set defaultColorMap = CreateObject("Scripting.Dictionary")
	defaultColorMap.Add "YELLOW", "255 221 0"
	defaultColorMap.Add "RED", "178 64 64"
	defaultColorMap.Add "PURPLE", "125 74 176"
	defaultColorMap.Add "PINK", "255 122 160"
	defaultColorMap.Add "GREEN", "106 190 122"
	defaultColorMap.Add "ORANGE", "255 142 74"
	defaultColorMap.Add "BLUE", "45 150 171"
	
	if SCV_OldSaveName <> "" then
' split old save name to get the generated # prefix
		oldSaveNameSplit = Split(SCV_OldSaveName, "_")
		
		prefix = oldSaveNameSplit(0)
	else
		prefix = GetSaveCount() + 1
	end if
	
'msgbox SCV_IsPreview
End Sub

Function SC_SaveName
'msgbox "SaveName called"
	Dim oldSaveNameSplit, nextValue, value
	prefix = GetSaveCount() + 1
	value = 0
	
	for i = 1 to 7
		value = value + Cdbl(FindComponent("DragEdit" & CStr(i)).text)
	next
	
	SC_SaveName = "FS_BAR GRAPH"
	
	if imageOrTitleCheckbox.checked then
		SC_SaveName = SC_SaveName & "*** IMAGE TITLE ***"
	else
		if PiesCenter.checked then
			SC_SaveName = SC_SaveName & "***" & PieTitleCenter.Text & "***"
		else
			SC_SaveName = SC_SaveName & "***" & PieTitleLR.Text & "***"
		end if
	end if
	
	SC_SaveName = SC_SaveName & " - " & PieSubtxt.Text & " : " & PieSourceTxt.Text
	
' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
		prefix = oldSaveNameSplit(0)
'msgbox "old: " & SCV_OldSaveName
		previouslySaved = true
	end if
	
	if previousSaveName <> "" then
		oldSaveNameSplit = Split(previousSaveName, "_")
		prefix = oldSaveNameSplit(0)
		
'msgbox "prev: " & previousSaveName
	end if
	
	SC_SaveName = (prefix) & "_" & GetWeekday() & "_" & SC_SaveName
	
end function

' triggered when a textfield has been modified
Sub TEXT_MODIFIED(Sender)
	FindComponent(Sender.Name & "Label").Caption = Sender.Hint &  " - Characters Left: " & Sender.MaxLength - Len(Sender.Text)
	
	if InStr(Sender.Name, "Title") then
		titleTextControl.Text = Sender.Text
	end if
	
	if Sender.Name = "PieTitleLR" then
		PieTitleCenter.Text = Sender.Text
	elseif Sender.Name = "PieTitleCenter" then
		PieTitleLR.Text = Sender.Text
	end if
End Sub

' triggered when pie chart label radio buttons hit
Sub PIE_LABELS(Sender)
	if pieLabels.checked then
		pieLabelsControl.checked = true
		legendLabelsControl.checked = false
	else
		pieLabelsControl.checked = false
		legendLabelsControl.checked = true
	end if
End sub

' triggered when number of headshots changed
Sub HeadshotRadio(Sender)
	rightHeadshot1.visible = true
	rightHeadshot2.visible = true
	leftHeadshot1.visible = true
	leftHeadshot2.visible = true
	
	if singleRadio.checked then
		if PiesPicLeft.checked then
			rightHeadshots.visible = true
			leftHeadshots.visible = false
			rightPlateCountControl.Text = "0"
			rightHeadshot2.visible = false
		else
			rightHeadshots.visible = false
			leftHeadshots.visible = true
			leftPlateCountControl.Text = "0"
			leftHeadshot2.visible = false
		end if
		
		bgImageCheckbox.checked = false
'BACKGROUND_CHECKBOX_EVENT(Sender)
		bgImageCheckbox.visible = false
		backPlateControl.ActiveLayerId = "2220x"
	elseif doubleRadio.checked then
		if PiesPicLeft.checked then
			rightHeadshots.visible = true
			leftHeadshots.visible = false
			rightPlateCountControl.Text = "1"
			rightHeadshot2.visible = true
		else
			leftPlateCountControl.Text = "1"
			leftHeadshots.visible = true
			rightHeadshots.visible = false
			leftHeadshot2.visible = true
		end if
		
		bgImageCheckbox.checked = false
'BACKGROUND_CHECKBOX_EVENT(Sender)
		bgImageCheckbox.visible = false
		backPlateControl.ActiveLayerId = "2220x"
	else
		if not PiesCenter.checked then
			leftPlateControl.ActiveLayerId = "2250x"
			rightPlateControl.ActiveLayerId = "2240x"
		end if
		
		bgImageCheckbox.visible = true
		
	end if
	
	if PiesCenter.checked or noRadio.checked then
		rightHeadshots.visible = false
		leftHeadshots.visible = false
	end if
End Sub

' activate line highlights
Sub ActivateLineHighlight(Sender)
	index = Right(Sender.name, 1)
	if Sender.checked then
		FindComponent("DragEdit" & index ).color = clYellow
		FindComponent("TTWUniCheckBox" & index).checked = true
		if index < 4 then
'FindComponent("sideHightlight" & index).checked = true
		end if
	else
		FindComponent("TTWUniCheckBox" & index).checked = false
		FindComponent("DragEdit" & index ).color = clInactiveCaption
		if index < 4 then
'FindComponent("sideHightlight" & index).checked = false
		end if
	end if
End Sub

' set colors
Sub SetColors(Sender)
	Dim nameSplit, materialName
	
	
	
	if PieColorCombo.itemIndex = 0 then
		
		for i=1 to 7
			nameSplit = Split(FindComponent("regularColor" & CStr(i)).picFilename, "/")
			materialName = nameSplit(UBound(nameSplit))
			if defaultColorMap.Item(materialName) <> "" then
				FindComponent("materialColor" & CStr(i)).Text = RGB_To_EngineValues(defaultColorMap.Item(materialName))
			end if
		next
	else
		for i=1 to 7
			nameSplit = Split(FindComponent("politicalColor" & CStr(i)).picFilename, "/")
			materialName = nameSplit(UBound(nameSplit))
			if politicalColorMap.Item(materialName) <> "" then
				FindComponent("materialColor" & CStr(i)).Text = politicalColorMap.Item(materialName)'RGB_To_EngineValues()
			end if
		next
	end if
End Sub

' when title changes
Sub TITLE_CHANGED(Sender)
	titleTextControl.Text = Sender.text
End Sub

' pie value changed
Sub BarValueChanged(Sender)
	index  =  Right(Sender.name, 1)
	if IsNumeric(FindComponent("DragEdit" & index).Text) then
		if FindComponent("DragEdit" & index).Text = "" then
			FindComponent("valueDragEdit" & index).Text = "0"
			FindComponent("valueDragEdit3_" & index).Text = "0"
			exit sub
		end if
		
		FindComponent("valueDragEdit" & index).Text = FindComponent("DragEdit" & index).Text
		FindComponent("NDragEdit" & index).Text = FindComponent("DragEdit" & index).Text
		if index < 6 then
			FindComponent("valueDragEdit3_" & index).Text = FindComponent("DragEdit" & index).Text
		end if
		
	' if value already in string
		if InStr(CStr(index & ":"), largeValues) = 0 then
			if FindComponent("DragEdit" & index).Text <> "" then
				if CDbl(FindComponent("DragEdit" & index).Text) < 80 then
					largeValues = Replace(largeValues, index & ":", "")
				end if
			end if
		else
			if FindComponent("DragEdit" & index).Text <> "" then
				if CDbl(FindComponent("DragEdit" & index).Text) > 79 then
					largeValues = largeValues + index & ":"
				end if
			end if
		end if
		
	' if the lenght of the value is > 3 chars then hide the value
		
		if PercentCheckbox.checked and decimalCheckbox.checked then
			if Len(FindComponent("DragEdit" & index).Text) > 2 then
				FindComponent("highlightCheckbox" & index).Checked = false
			else
				FindComponent("highlightCheckbox" & index).Checked = true
			end if
		elseif PercentCheckbox.checked = false and decimalCheckbox.checked = false then
			if Len(FindComponent("DragEdit" & index).Text) > 4 then
				FindComponent("highlightCheckbox" & index).Checked = false
			else
				FindComponent("highlightCheckbox" & index).Checked = true
			end if
		else
			if Len(FindComponent("DragEdit" & index).Text) > 3 then
				FindComponent("highlightCheckbox" & index).Checked = false
			else
				FindComponent("highlightCheckbox" & index).Checked = true
			end if
		end if
		CheckBarValues()
	end if
End Sub

Sub CheckBarValues()
	Dim maxVal
	maxVal = YDragEditMax.text
	Dim larger : larger = false
	
	
	
	for i=1 to CInt(NumPiesCombo.Text)
		if IsNumeric(FindComponent("DragEdit" & CStr(i)).Text) then
			if Cdbl(FindComponent("DragEdit" & CStr(i)).Text) > (maxVal*.8) then
				larger = true
			end if
		end if
	next
	
	if larger then
' no large values
		if PiesCenter.checked then
			if revealRadio.checked then
				barSizeControl.ActiveLayerId = "1111b"
			else
				barSizeControl.ActiveLayerId = "1111x"
			end if
		else
			if revealRadio.checked then
				barSizeControl.ActiveLayerId = "1110b"
			else
				barSizeControl.ActiveLayerId = "1110x"
			end if
		end if
	else
' large values
		if PiesCenter.checked then
			if revealRadio.checked then
				barSizeControl.ActiveLayerId = "1113b"
			else
				barSizeControl.ActiveLayerId = "1113x"
			end if
		else
			if revealRadio.checked then
				barSizeControl.ActiveLayerId = "1112b"
			else
				barSizeControl.ActiveLayerId = "1112x"
			end if
		end if
	end if
End Sub


'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
	if Len(savedOver) = 0 or savedOver = True then
'msgbox CStr(saveId) & " - OVERWRITE HIT"
	elseif savedOver = false then
'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
		
		
' if not a new template then overwrite the recently saved element with a
		if not newTemplate then
			oldSaveNameSplit = Split(SCV_OldSaveName, "_")
			oldSaveName = SCV_OldSaveName
			newSaveName = CStr(GetSaveCount() + 1) & "_"
			
			for i=1 to UBound(oldSaveNameSplit)
				if i <> UBound(oldSaveNameSplit) then
					newSaveName = newSaveName & oldSaveNameSplit(i) & "_"
				else
					newSaveName = newSaveName & oldSaveNameSplit(i)
				end if
			next
			
			msgbox "Save As New hit on a previously saved template, update id"
			RenameDatabaseEntry newSaveName, oldSaveName
		end if
		
		IncrementSequenceValue()
	end if
	
End Sub

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 10000
	
	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
'msgbox "Count: " & newId
	GetSaveCount = CStr(newId)
End Function

Function UpdateColumn(tableName, columnName, savedId, newValue)
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	
	Query = "UPDATE " & tableName & " SET " & columnName & " = '" & newValue & "' WHERE DNR = '" & savedId & "'"
	
	Set recordset = DBConnection.Execute(Query)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = 10000
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	
	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	IncrementSequenceValue = CStr(newId)
End Function

'======================================================================================================
'===================================== NBC ORIGINAL ===================================================
'======================================================================================================

'pie chart-------------------------------------------------------------------------------------------------

Sub NumPiesComboChange(Sender)
	svAL =  NumPiesCombo.UTF8Text
	numberOfBars.Text = sVal
	
	if CInt(sVal) > 5 then
		PiesCenter.checked = true
	end if
	
	Select Case svAL
	Case "1"
		grpBarLabel1.visible = true
		grpBarLabel2.visible = false
		grpBarLabel3.visible = false
		grpBarLabel4.visible = false
		grpBarLabel5.visible = false
		grpBarLabel6.visible = false
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = true
		
		PiesPositionRadio(Sender)
		
	Case "2"
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = false
		grpBarLabel4.visible = false
		grpBarLabel5.visible = false
		grpBarLabel6.visible = false
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = true
		
		PiesPositionRadio(Sender)
		
	Case "3"
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = true
		grpBarLabel4.visible = false
		grpBarLabel5.visible = false
		grpBarLabel6.visible = false
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = true
		
		PiesPositionRadio(Sender)
	Case "4"
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = true
		grpBarLabel4.visible = true
		grpBarLabel5.visible = false
		grpBarLabel6.visible = false
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = true
		
		PiesPositionRadio(Sender)
		
	Case "5"
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = true
		grpBarLabel4.visible = true
		grpBarLabel5.visible = true
		grpBarLabel6.visible = false
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = true
		
		PiesPositionRadio(Sender)
		
	Case "6"
		backPlateControl.ActiveLayerId = "2220x"
		if revealRadio.checked then
			barSizeControl.ActiveLayerId = "1113b"
		else
			barSizeControl.ActiveLayerId = "1113x"
		end if
		
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		fsPlateControl.ActiveLayerId = "2014x"
		
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = true
		grpBarLabel4.visible = true
		grpBarLabel5.visible = true
		grpBarLabel6.visible = true
		grpBarLabel7.visible = false
		
		grpBarPos.Visible = false
		
	Case "7"
		backPlateControl.ActiveLayerId = "2220x"
		if revealRadio.checked then
			barSizeControl.ActiveLayerId = "1113b"
		else
			barSizeControl.ActiveLayerId = "1113x"
		end if
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		fsPlateControl.ActiveLayerId = "2014x"
		
		grpBarLabel1.visible = true
		grpBarLabel2.visible = true
		grpBarLabel3.visible = true
		grpBarLabel4.visible = true
		grpBarLabel5.visible = true
		grpBarLabel6.visible = true
		grpBarLabel7.visible = true
		
		grpBarPos.Visible = false
	End Select
	
'BarValueChanged(Sender)
End Sub

Sub RevealChecked(Sender)
	PiesPositionRadio(Sender)
	NumPiesComboChange(Sender)
End Sub

Sub PiesPositionRadio(Sender)
	MAIN_BG.ActiveLayerId = "2001x"
	HeadshotsChoice.visible = true
	
	if PiesPicLeft.checked then
		bgImageCheckbox.visible = true
		PieTitleLRLabel.visible = true
		PieTitleCenterLabel.visible = false
		
		fsPlateControl.ActiveLayerId = "2011x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2241x"
		
' set pie type
		if revealRadio.checked then
			barSizeControl.ActiveLayerId = "1112b"
		else
			barSizeControl.ActiveLayerId = "1112x"
		end if
		
' set title border
		titleBannerControl.Text = "4"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitleLR"))
		
	elseif PiesCenter.checked then
		PieBGimage.visible = false
		PieTitleCenterLabel.visible = true
		PieTitleLRLabel.visible = false
		fsPlateControl.ActiveLayerId = "2014x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		
' hide headshots and controls
		HeadshotsChoice.visible = false
		rightHeadshots.visible = false
		leftHeadshots.visible = false
		
' set pie type
		if revealRadio.checked then
			barSizeControl.ActiveLayerId = "1113b"
		else
			barSizeControl.ActiveLayerId = "1113x"
		end if
		
		
' set title border
		titleBannerControl.Text = "3"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitleCenter"))
		
		bgImageCheckbox.checked = false
		PieBGimage.visible = false
		backPlateControl.ActiveLayerId = "2220x"
		bgImageCheckbox.checked = false
		bgImageCheckbox.visible = false
	else
		bgImageCheckbox.visible = true
		PieTitleLRLabel.visible = true
		PieTitleCenterLabel.visible = false
		
		fsPlateControl.ActiveLayerId = "2012x"
		leftPlateControl.ActiveLayerId = "2251x"
		rightPlateControl.ActiveLayerId = "2240x"
		
		
' set pie type
		if revealRadio.checked then
			barSizeControl.ActiveLayerId = "1112b"
		else
			barSizeControl.ActiveLayerId = "1112x"
		end if
		
' set title border
		titleBannerControl.Text = "4"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitleLR"))
	end if
	HeadshotRadio(Sender)
	CheckBarValues()
End Sub

Sub PieTitleLRChange(Sender)
	PieTitleLRLabel.Caption = "Pie Title - Characters Left: " & PieTitleLR.MaxLength - Len(PieTitleLR.Text)
	
end Sub

Sub PieTitleCenterChange(Sender)
	PieTitleCenterLabel.Caption = "Pie Title - Characters Left: " & PieTitleCenter.MaxLength - Len(PieTitleCenter.Text)
End sub

Sub PieSubTxtChange(Sender)
	PieSubtxtLabel.Caption = "Subtitle - Characters Left: " & PieSubTxt.MaxLength - Len(PieSubTxt.Text)
End sub

Sub PieSourceTxtChange(Sender)
	PieSourcetxtLabel.Caption = "Source - Characters Left: " & PieSourceTxt.MaxLength - Len(PieSourceTxt.Text)
	
End sub

Sub ChartLabelPieTxtChange(Sender)
	PieChartLabel.Caption = "Chart Label - Characters Left: " & ChartLabelPieTxt.MaxLength - Len(ChartLabelPieTxt.Text)
End sub

Sub ImageOrTitle(Sender)
	if imageOrTitleCheckbox.checked = true then
		titleTypeControl.Text = "1"
		GetTitleImagePie.visible = true
		PieTitleLRLabel.visible = false
		PieTitleCenterLabel.visible = false
	else
		titleTypeControl.Text = "0"
		imageOrTitleCheckbox.checked = false
		GetTitleImagePie.visible = false
		PieTitleLRLabel.visible = true
		PieTitleCenterLabel.visible = true
	End if
	
	PiesPositionRadio(Sender)
end sub

Sub BarColorComboChange (Sender)
	svAL =  PieColorCombo.UTF8Text
	Select Case svAL
	Case "Default"
		DefaultColor1.Visible = true
		DefaultColor2.Visible = true
		DefaultColor3.Visible = true
		DefaultColor4.Visible = true
		DefaultColor5.Visible = true
		DefaultColor6.Visible = true
		DefaultColor7.Visible = true
		PolColor1.visible = false
		PolColor2.visible = false
		PolColor3.visible = false
		PolColor4.visible = false
		PolColor5.visible = false
		PolColor6.visible = false
		PolColor7.visible = false
		
	Case "Political"
		PolColor1.visible = true
		PolColor2.visible = true
		PolColor3.visible = true
		PolColor4.visible = true
		PolColor5.visible = true
		PolColor6.visible = true
		PolColor7.visible = true
		DefaultColor1.Visible = false
		DefaultColor2.Visible = false
		DefaultColor3.Visible = false
		DefaultColor4.Visible = false
		DefaultColor5.Visible = false
		DefaultColor6.Visible = false
		DefaultColor7.Visible = false
		
	end select
	
	SetColors(Sender)
end sub

Sub Labeltxt1Change(Sender)
	PieLabel1.Caption = "Label - Characters Left: " & Labeltxt1.MaxLength - Len(Labeltxt1.Text)
End sub

Sub Labeltxt2Change(Sender)
	PieLabel2.Caption = "Label - Characters Left: " & Labeltxt2.MaxLength - Len(Labeltxt2.Text)
End sub

Sub Labeltxt3Change(Sender)
	PieLabel3.Caption = "Label - Characters Left: " & Labeltxt3.MaxLength - Len(Labeltxt3.Text)
End sub

Sub Labeltxt4Change(Sender)
	PieLabel4.Caption = "Label - Characters Left: " & Labeltxt4.MaxLength - Len(Labeltxt4.Text)
End sub

Sub Labeltxt5Change(Sender)
	PieLabel5.Caption = "Label - Characters Left: " & Labeltxt5.MaxLength - Len(Labeltxt5.Text)
End sub

Sub PTBlaunchClick(Sender)
	set wshShell = CreateObject("WSCript.shell")
	wshshell.run "PTPlugin.exe"
	set wshshell = nothing
End sub

Sub BACKGROUND_CHECKBOX_EVENT(Sender)
	if bgImageCheckbox.checked then
		PieBGimage.visible = true
		HeadshotsChoice.visible = false
		leftHeadshots.visible = false
		rightHeadshots.visible = false
		backPlateControl.ActiveLayerId = "2221x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
	else
		HeadshotsChoice.visible = true
		PiesPositionRadio(Sender)
		PieBGimage.visible = false
		backPlateControl.ActiveLayerId = "2220x"
	end if
End sub

Sub LIVE_VIDEO_CHECKED(Sender)
	if liveVideoCheckbox.checked then
' all out and invisible
		leftHeadshots.visible = false
		rightHeadshots.visible = false
		HeadshotsChoice.visible = false
		PieBGimage.visible = false
		bgImageCheckbox.visible = false
		
		backPlateControl.ActiveLayerId = "2220x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		
'in - live
		MAIN_BG.ActiveLayerId = "2000x"
		
	else
' all in and visible
		HeadshotsChoice.visible = true
		PieBGimage.visible = true
		bgImageCheckbox.visible = true
		
' out - live
		MAIN_BG.ActiveLayerId = "2001x"
		
		
		PiesPositionRadio(Sender)
		BACKGROUND_CHECKBOX_EVENT(Sender)
	end if
	if PiesCenter.checked = true then
		bgImageCheckbox.checked = false
		bgImageCheckbox.visible = false
	end if
	
End sub

Function RGB_To_EngineValues(input)
	valueSplit = Split(input, " ")
	output = ""
	for i=0 to 2
		output = output & " " & CStr(CInt(valueSplit(i))/255)
	next
	
	RGB_To_EngineValues = output
	
End Function

Function RenameDatabaseEntry(newSaveName, oldSaveName)
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
' get id of the most recently saved element that matches this description
	Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
	
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("DNR")
		
'msgbox "FOUND DNR: " & newId
' update that element
		Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
		
'msgbox "QUERY: " & Query
		
		DBConnection.Execute(Query)
	end if
	
	
End Function

Sub YDragEditMaxChange(Sender)
	if YDragEditMax.Text <> "" and YDragEditMin.Text <> "" then
		TTWUniEdit5.text = YDragEditMax.text
		TTWUniEdit1.text = YDragEditMin.text
		
		dim maxVal, minVal, val2, val3, val4, valInc, numDecimals, decimalsExist, suffix
		numDecimals = 0
		decimalsExist = false
		
		numberFormat.Text = "###,###."
		
		if decimalCheckbox.checked then
			numDecimals = 1
			numberFormat.Text = "###,###.#"
		end if
		
' enable int mid
		for i = 1 to 7
			if numDecimals = 0 then
				FindComponent("DragEdit" & i).IntegerMode = true
			else
				FindComponent("DragEdit" & i).IntegerMode = false
			end if
		next
		
		if suffixValue.visible then
			suffix = suffixValue.Text
		end if
		
		
		maxVal = Cdbl(YDragEditMax.text)
		minVal = Cdbl(YDragEditMin.text)
		
		
		
'set drag edit max values
		for i=1 to 7
			FindComponent("DragEdit" & i).MaxValue = maxVal
		next
		
		valInc = ((maxVal-minVal)/4)
		val2 = FormatNumber(round(valInc, numDecimals), numDecimals)
		val3 = FormatNumber(round(valInc*2, numDecimals), numDecimals)
		val4 = FormatNumber(round(valInc*3, numDecimals), numDecimals)
		
		if minVal > 0 then
			minVal = FormatNumber(minVal, numDecimals)
		end if
		
		maxVal = FormatNumber(maxVal, numDecimals)
' check if vals have decimals, if at least one has decimals then fix the rest as well
		
		if PercentCheckbox.checked = true then
			TTWUniEdit5.text= prefixValue.Text & maxVal & "%"
			TTWUniEdit2.text= prefixValue.Text & val2 & "%"
			TTWUniEdit3.text= prefixValue.Text & val3 & "%"
			TTWUniEdit4.text= prefixValue.Text & val4 & "%"
			
			if minVal > 0 then
				TTWUniEdit1.text = prefixValue.Text & minVal & "%"
			end if
		else
			TTWUniEdit5.text= prefixValue.Text & maxVal & suffix
			TTWUniEdit2.text= prefixValue.Text & val2 & suffix
			TTWUniEdit3.text= prefixValue.Text & val3 & suffix
			TTWUniEdit4.text= prefixValue.Text & val4 & suffix
			TTWUniEdit1.text= minVal
			
			if minVal > 0 then
				TTWUniEdit1.text = prefixValue.Text & minVal & suffix
			end if
		end if
		
		CheckBarValues()
	end if
	BarValueChanged(DragEdit1)
End sub

Sub PercentCheckboxClick(Sender)
	if not PercentCheckbox.checked then
		
		suffixValue.visible = true
		suffixLabel.visible = true
	else
		suffixValue.visible = false
		suffixLabel.visible = false
	end if
	YDragEditMaxChange(Sender)
	BarValueChanged(DragEdit1)
End sub

