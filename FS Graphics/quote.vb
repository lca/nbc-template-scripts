' Desc: Quote Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 07112014T1238

Dim ptbState : ptbState = true

Dim debug : debug = true
debug = false

Dim templateName : templateName = "FS_QUOTE"

Dim oldSaveNameSplit, oldSaveName, newSaveName, newTemplate
newTemplate = true

sub InitForm
		PlatePositionChanged(Sender)
		'ApplyDefaultEvents()
end sub

' ======================================================================================
' ====================================== MAIN EVENTS ===================================
' ======================================================================================

'Screen Position Combobox
Sub PlatePositionChanged(Sender)
		backPlateLayerControl.ActiveLayerId = "2220x"
		sidePlateLeftLayerControl.ActiveLayerId= "2250x"
		sidePlateRightLayerControl.ActiveLayerId = "2240x"

		bigOrSmallTitle.Text = "0"
		bigOrSmallBody.Text = "0"

		sideImageGroupBox.visible = false
		bgImageGroupBox.visible = false

		if middleSelect.checked then
			if not imageTitleRadio.checked then
				ptbState = false
	        	PTBButton.Picture = PTBRestingIcon.Picture
        	end if

			ChangeDefaultBackgroundPanel.visible = false

			bigOrSmallTitle.Text = "1"
			bigOrSmallBody.Text = "1"

			pageLength1.value = 1
			pageLength2.value = 1
			pageLength3.value = 1
			pageLength4.value = 1
			pageLength5.value = 1

			positionLayerControl.ActiveLayerId = "2014x"
		else
			ChangeDefaultBackgroundPanel.visible = true
			ptbState = true
            PTBButton.Picture = PTBRegularIcon.Picture

            backPlateLayerControl.ActiveLayerId = "2221x"

            if leftRadio.checked then
				positionLayerControl.ActiveLayerId = "2011x"
				sidePlateLeftLayerControl.ActiveLayerId = "2250x"
				sidePlateRightLayerControl.ActiveLayerId = "2241x"

				sideImage1.ControlObjectName = "R_IMAGE1"
				sideImage2.ControlObjectName = "R_IMAGE2"
			else
				positionLayerControl.ActiveLayerId = "2012x"
				sidePlateLeftLayerControl.ActiveLayerId = "2251x"

				sideImage1.ControlObjectName = "L_IMAGE1"
				sideImage2.ControlObjectName = "L_IMAGE2"
			end if

			if bgImageRadio.checked then
				bgImageGroupBox.visible = true
			else
				sideImageGroupBox.visible = true
				NumberHeadshotsClick(Sender)
			end if
		end if
End sub

'headshot radio button single/double 'TODO: HERE'
Sub NumberHeadshotsClick(Sender)
		sideImagePanel2.visible = doubleImageRadio.checked
		sideImage1.visible = true
		sideImage2.visible = true
End Sub

' TRIGGERED ON CLICK OF A QUOTE ADD BUTTON
Sub QuoteAddClick(Sender)
	Dim buttonNumber : buttonNumber = CInt(Right(Sender.Name, 1))
	pageNumOmo.Text = buttonNumber

	SetVisibility "quotePanel", true, buttonNumber + 1, buttonNumber + 1
	if buttonNumber < 4 then
			SetVisibility "addButton", true, buttonNumber + 1, buttonNumber + 1
			SetVisibility "subtractButton", false, buttonNumber + 1, buttonNumber + 1
	end if
	
	SetVisibility "subtractButton", true, buttonNumber, buttonNumber
	SetVisibility "addButton", false, buttonNumber, buttonNumber

	select case buttonNumber
		case 1
			QuotePanel.Height = 263
			backPanel.Height = 771
		case 2
			QuotePanel.Height = 385
			backPanel.Height = 891
		case 3
			QuotePanel.Height = 506
			backPanel.Height = 1011
		case 4
			QuotePanel.Height = 621
			backPanel.Height = 1115
	end select
End Sub

' TRIGGERED ON CLICK OF A QUOTE SUBTRACT BUTTON
Sub QuoteSubtractClick(Sender)
	Dim buttonNumber : buttonNumber = Right(Sender.Name, 1)
	pageNumOmo.Text = CInt(buttonNumber) - 1

	SetVisibility "quotePanel", false, buttonNumber + 1, 5
	SetVisibility "addButton", true, buttonNumber, buttonNumber
	SetVisibility "subtractButton", false, buttonNumber, buttonNumber 
	ClearTexts "quoteText", buttonNumber + 1, 5

	select case buttonNumber
		case 1
			QuotePanel.Height = 141
			backPanel.Height = 643
		case 2
			QuotePanel.Height = 263
			backPanel.Height = 771
		case 3
			QuotePanel.Height = 385
			backPanel.Height = 891
		case 4
			QuotePanel.Height = 506
			backPanel.Height = 1011
	end select
End Sub

Sub TitleTypeChanged(Sender)
		ptbState = false
        PTBButton.Picture = PTBRestingIcon.Picture

        titleText.Color = clWhite
        subtitleCheckbox.Font.Color = clWhite
        titleTypeGroupBox.enabled = true
        titleTypeGroupBox.Font.Color = clWhite

        if imageTitleRadio.checked then
            titleTypeGroupBox.visible = true

            titleOff.checked = true
            textOrImageOmo.Value = 1
            titleText.visible = false
            titleImage.visible = true
            textOrImageOmo.Text = "1"
            titleTypeGroupBox.Caption = "Drag Masthead Into This Panel"
            ptbState = true
            PTBButton.Picture = PTBRegularIcon.Picture

        elseif textTitleRadio.checked then
            titleTypeGroupBox.visible = true
            titleTypeGroupBox.Caption = "Title - Characters Left:"
            if titleText.Text = "" then
                titleOff.checked = false
            else
                titleOff.checked = true
            end if

            titleText.visible = true
            titleTypeGroupBox.visible = true
            titleImage.visible = false
            textOrImageOmo.Text = "0"

        else
            titleTypeGroupBox.enabled = false
            titleOff.checked = false

            titleTypeGroupBox.Caption = "Title Disabled"
            titleTypeGroupBox.Font.Color = clInactiveCaption
            titleText.Color = clInactiveCaption
            subtitleCheckbox.Font.Color = clInactiveCaption

            subtitleCheckbox.checked = false
            subtitleCheckboxClicked(subtitleCheckbox)
        end if

        PlatePositionChanged(Sender)
End Sub

Sub subtitleCheckboxClicked(Sender)
		if subtitleCheckbox.checked then
			subtitleText.visible = true
		else
			subtitleText.visible = false
			subtitleText.Text = ""
		end if
End sub

Sub AttributionVisibilityChanged(Sender)
	if allPagesRadio.checked then
		attributeVisibilityControl.value = 0
	else
		attributeVisibilityControl.value = 1
	end if
End Sub
'======================================================================================================
'===================================== PTB CONTROLS ===================================================
'======================================================================================================

Sub PTBbuttonClick(Sender)
	if ptbState then
		if not debug then
			set wshShell = CreateObject("WSCript.shell")
			wshshell.run "PTPlugin.exe"
			set wshshell = nothing
		end if

		PTBButton.Picture = PTBPressedIcon.Picture
		PTBTimer.Enabled = true
	end if

End Sub

Sub PTBButtonRelease(Sender)
		PTBButton.Picture = PTBRegularIcon.Picture
		ptbState = true
		PTBTimer.Enabled = false
End sub

' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
		Dim dayAbbv : dayAbbv = Array("N/A", "SU", "MO", "TU", "WE", "TH", "FR", "SA")
		GetWeekday = dayAbbv(Weekday(Date))
End Function

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
		Dim comp
		for i=start to finish
				comp = FindComponent(componentName & CStr(i))
				ConnectVTWEvent [_scripter], comp, eventType, eventName
		next
End Sub

' CHARACTER COUNT MONITOR
Sub CharsLeft(Sender)
		Dim componentNumber, componentDesc, quoteStrings
		quoteStrings = ""
		if InStr(Sender.Name, "quoteText") then
				componentNumber = Right(Sender.Name, 1)
				
				if not middleSelect.checked and Len(Sender.Text) >= 148 then
					FindComponent("pageLength" & componentNumber).value = 0
				elseif not middleSelect.checked then
					FindComponent("pageLength" & componentNumber).value = 1
				end if

				if InStr(Sender.Text, vbNewLine) then
					charLeftPanel.Height = 45
					CharLeft.Caption = "Quote Page " & componentNumber	& " - Characters Left: Invalid - Hard Return Entered Reference Preview Window"
					CharLeft.Left = 26
					CharLeft.Width = 450

					CharLeft.Font.Color = clRed
				else
					charLeftPanel.Height = 26
					CharLeft.Caption = "Quote Page " & componentNumber	& " - Characters Left: " & (Sender.MaxLength - 2) - Len(Sender.Text)
					CharLeft.Left = 5
					CharLeft.Width = 260

					if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
						CharLeft.Font.Color = clRed
					else
						CharLeft.Font.Color = clNavy
					end if
				end if
		elseif InStr(Sender.Name, "title") then

				if InStr(Sender.Name, "sub") and subtitleCheckbox.checked then
					titleTypeGroupBox.Caption = "Subtitle - Characters Left: " & (Sender.MaxLength - 2) - Len(Sender.Text)
				else
					titleTypeGroupBox.Caption = "Title - Characters Left: " & (Sender.MaxLength - 2) - Len(Sender.Text)	
				end if

				if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
					titleTypeGroupBox.Font.Color = clRed
				else
					titleTypeGroupBox.Font.Color = clWhite
				end if

				subtitleCheckbox.Font.Color = clWhite

				TitleTypeChanged(Sender)
		elseif InStr(Sender.Name, "attribute") then
			componentNumber = Right(Sender.Name, 1)

			attributionGroupBox.Caption = "Attribute Line " & componentNumber & " - Characters Left: " & (Sender.MaxLength - 2) - Len(Sender.Text)

			if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
				attributionGroupBox.Font.Color = clRed
			else
				attributionGroupBox.Font.Color = clWhite
			end if
		end if
End Sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Text = ""
	next
End Sub

' TRIGGERED ON FOCUS OF A NON TEXT FIELD WHEN THE PREVIOUS FOCUS WAS ON A TEXT FIELD
Sub ExitTextField(Sender)
	if InStr(Sender.Name, "title") then
			CharsLeft(titleText)
	elseif InStr(Sender.Name, "quote") then
		CharLeft.Caption = "Quote Page - Characters Left: "
		CharLeft.Font.Color = clInactiveCaption
	end if
End Sub

Sub UpdateCases(Sender)
    ' list of text fields referenced by 'global' functions
    Dim textFields
    textFields = Array( "quoteText1", "quoteText2", _
	                    "quoteText3", "quoteText4", _
	                    "quoteText5")

    Dim caseType : caseType = -1
    if upperCaseRadio.checked then
        caseType = 1
    elseif normalCaseRadio.checked then
        caseType = 0
    end if

    for i = 0 to UBound(textFields)
        FindComponent(textFields(i)).CharCase = caseType
        if caseType = -1 then
            FindComponent(textFields(i)).Text = LCase(FindComponent(textFields(i)).Text)
        end if

        'logText.Text = logText.Text & textFields(i) & vbNewLine
    next

    ' title/subtitles/source
    if includeMainTextsCheckbox.checked then
        titleText.CharCase = caseType
        subtitleText.CharCase = caseType
        attributeLine1.CharCase = caseType
        attributeLine2.CharCase = caseType
        attributeLine3.CharCase = caseType

        if caseType = -1 then
            titleText.Text = LCase(titleText.Text)
            subtitleText.Text = LCase(subtitleText.Text)
	        attributeLine1.Text = LCase(attributeLine1.Text)
	        attributeLine2.Text = LCase(attributeLine2.Text)
	        attributeLine3.Text = LCase(attributeLine3.Text)
        end if
    end if
End Sub

' ADD HINTS
Sub AddHints(componentName, hint, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Hint = hint
	next
End Sub

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
	Dim comp

	if start = -1 then
		comp = FindComponent(componentName)
		ConnectVTWEvent [_scripter], comp, eventType, eventName
	else
		for i=start to finish
			comp = FindComponent(componentName & CStr(i))
			ConnectVTWEvent [_scripter], comp, eventType, eventName
		next
	end if
End Sub

Sub ApplyDefaultTextEvents()
	'ApplyEvent(componentName, eventName, eventType, start, finish)'
	ApplyEvent "titleText", "CharsLeft", "OnChange", -1, -1
	ApplyEvent "titleText", "CharsLeft", "OnClick", -1, -1
	ApplyEvent "titleText", "CharsLeft", "OnEnter", -1, -1
	ApplyEvent "titleText", "ExitTextField", "OnExit", -1, -1

	ApplyEvent "subtitleText", "CharsLeft", "OnChange", -1, -1
	ApplyEvent "subtitleText", "CharsLeft", "OnClick", -1, -1
	ApplyEvent "subtitleText", "CharsLeft", "OnEnter", -1, -1
	ApplyEvent "subtitleText", "ExitTextField", "OnExit", -1, -1

	ApplyEvent "attributeLine", "CharsLeft", "OnChange", 1, 3
	ApplyEvent "attributeLine", "CharsLeft", "OnClick", 1, 3
	ApplyEvent "attributeLine", "CharsLeft", "OnEnter", 1, 3
	ApplyEvent "attributeLine", "ExitTextField", "OnExit", 1, 3

	ApplyEvent "attributeLine", "CharsLeft", "OnChange", 1, 3
	ApplyEvent "attributeLine", "CharsLeft", "OnClick", 1, 3
	ApplyEvent "attributeLine", "CharsLeft", "OnEnter", 1, 3
	ApplyEvent "attributeLine", "ExitTextField", "OnExit", 1, 3	

	ApplyEvent "quoteText", "CharsLeft", "OnChange", 1, 5
	ApplyEvent "quoteText", "CharsLeft", "OnClick", 1, 5
	ApplyEvent "quoteText", "CharsLeft", "OnEnter", 1, 5
	ApplyEvent "quoteText", "ExitTextField", "OnExit", 1, 5
End Sub

Sub ApplyDefaultEvents()
	' checkboxes
	ApplyEvent "subtitleCheckbox", "subtitleCheckboxClicked", "OnClick", -1, -1
	ApplyEvent "attributionCheckbox", "attributionCheckboxClicked", "OnClick", -1, -1

	' radios
	ApplyEvent "allPagesRadio", "AttributionVisibilityChanged", "OnClick", -1, -1
	ApplyEvent "lastPageRadio", "AttributionVisibilityChanged", "OnClick", -1, -1

	ApplyEvent "noTitleRadio", "TitleTypeChanged", "OnClick", -1, -1
	ApplyEvent "textTitleRadio", "TitleTypeChanged", "OnClick", -1, -1
	ApplyEvent "imageTitleRadio", "TitleTypeChanged", "OnClick", -1, -1

	ApplyEvent "middleSelect", "PlatePositionChanged", "OnClick", -1, -1
	ApplyEvent "sideSelect", "PlatePositionChanged", "OnClick", -1, -1
	ApplyEvent "bgImageRadio", "PlatePositionChanged", "OnClick", -1, -1
	ApplyEvent "sideImageRadioButton", "PlatePositionChanged", "OnClick", -1, -1
	ApplyEvent "leftRadio", "PlatePositionChanged", "OnClick", -1, -1
	ApplyEvent "rightRadio", "PlatePositionChanged", "OnClick", -1, -1

	ApplyEvent "singleImageRadio", "NumberHeadshotsClick", "OnClick", -1, -1
	ApplyEvent "doubleImageRadio", "NumberHeadshotsClick", "OnClick", -1, -1

	' other
	ApplyEvent "addButton", "QuoteAddClick", "OnClick", 1, 4
	ApplyEvent "subtractButton", "QuoteSubtractClick", "OnClick", 1, 4

	ApplyEvent "PTBButton", "PTBbuttonClick", "OnClick", -1, -1

	ApplyEvent "PTBTimer", "PTBbuttonRelease", "OnTimer", -1, -1

	ApplyEvent "updateCasesButton", "UpdateCases", "OnClick", -1, -1	

	ApplyDefaultTextEvents()
End Sub

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

'SAVE NAME CONVENTION:
'Template Name + Number of Pages + Title + Subtitle'

Function SC_SaveName
		Dim prefix : prefix = ""
		SC_SaveName = templateName

		if textTitleRadio.checked then
				SC_SaveName = SC_SaveName & " " & CStr(CInt(pageNumOmo.text) + 1) & " " & titleText.Text & " " & subtitleText.Text
		else
				SC_SaveName = SC_SaveName & " " & CStr(CInt(pageNumOmo.text) + 1)
		end if

		nextValue = GetSaveCount()

' split old save name to get the generated # prefix
		oldSaveNameSplit = Split(SCV_OldSaveName, "_")

		if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
				SC_SaveName = oldSaveNameSplit(0) & "_" & oldSaveNameSplit(1) & "_" & SC_SaveName
				newTemplate = false
		else
' if the template is new then generate a new prefix
			nextValue = nextValue + 1

			if Len(nextValue) < 5 then
				select case Len(nextValue)
					case 1
						prefix = "0000"
					case 2
						prefix = "000"
					case 3
						prefix = "00"
					case 4
						prefix = "0"
				end select
			end if

			SC_SaveName = GetWeekday() & "_" & prefix & nextValue & "_" & SC_SaveName
		end if

' increments db sequence value
		'IncrementSequenceValue()
end function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
		connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

		newId = "00001"
		Set DBConnection = CreateObject("ADODB.Connection")
		DBConnection.Open connectStr

		newId = 0

		Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

		Set recordset = DBConnection.Execute(Query)

		if not recordset.EOF then
				newId = recordset("ID")
		end if

		GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
		connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

		newId = "00001"
		Set DBConnection = CreateObject("ADODB.Connection")
		DBConnection.Open connectStr

		newId = 0

		Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

		Set recordset = DBConnection.Execute(Query)

		if not recordset.EOF then
				newId = recordset("ID")
		end if

		IncrementSequenceValue = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
	Dim number : number = ""
	Dim prefix : prefix = "_"
    if Len(savedOver) = 0 or savedOver = True then
		'msgbox CStr(saveId) & " - OVERWRITE HIT"
    elseif savedOver = false then
		'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
		
		
		' if not a new template then overwrite the recently saved element with a 
		if not newTemplate then
			oldSaveNameSplit = Split(SCV_OldSaveName, "_")
			oldSaveName = SCV_OldSaveName
			number = CStr(GetSaveCount() + 1)
			
			if Len(number) < 5 then
				select case Len(number)
					case 1
						prefix = "_0000"
					case 2
						prefix = "_000"
					case 3
						prefix = "_00"
					case 4
						prefix = "_0"
				end select
			end if

			if textTitleRadio.checked then
				newSaveName = GetWeekday() & prefix & number & templateName & CStr(CInt(pageNumOmo.text) + 1) & " " & titleText.Text & " " & subtitleText.Text
			else
				newSaveName = GetWeekday() & prefix & number & templateName & CStr(CInt(pageNumOmo.text) + 1)
			end if

			
			
			'msgbox "Save As New hit on a previously saved template, update id"
			RenameDatabaseEntry newSaveName, oldSaveName
		end if
		
		IncrementSequenceValue()
    end if

End Sub

Function RenameDatabaseEntry(newSaveName, oldSaveName)
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	' get id of the most recently saved element that matches this description
	Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
	

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
		newId = recordset("DNR")
		
		'msgbox "FOUND DNR: " & newId
		' update that element
		Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
		
		msgbox "QUERY: " & Query
		
		DBConnection.Execute(Query)
	end if
End Function