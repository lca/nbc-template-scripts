' Desc: Doc Tears Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075

Function SC_SaveName
	Dim pauseBool : pauseBool = pause_toggle.checked

	if stop2.checked then
		' stop2 checked
		if not pauseBool then
			if tear1b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***0***"
			elseif tear2a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***0***"
			elseif tear2b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear3a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear3b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***2***"
			else
				SC_SaveName = "FS_DOC TEAR: ***2***"
			end if
		else
			if tear1b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear2a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear2b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***2***"
			elseif tear3a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***2***"
			elseif tear3b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***3***"
			else
				SC_SaveName = "FS_DOC TEAR: ***3***"
			end if
		end if
	else
		' stop2 not checked
		if pauseBool then
			if tear1b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear2a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***2***"
			elseif tear2b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***3***"
			elseif tear3a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***4***"
			elseif tear3b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***5***"
			else
				SC_SaveName = "FS_DOC TEAR: ***6***"
			end if
		else
			if tear1b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***0***"
			elseif tear2a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***1***"
			elseif tear2b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***2***"
			elseif tear3a.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***3***"
			elseif tear3b.text = "" then
				SC_SaveName = "FS_DOC TEAR: ***4***"
			else
				SC_SaveName = "FS_DOC TEAR: ***5***"
			end if
		end if
	end if

	nextValue = GetSaveCount()
	
	' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
		' if the template has already been saved once, use the old prefix
		SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
	else
		' if the template is new then generate a new prefix
		SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
	end if

	' increments db sequence value
	IncrementSequenceValue()
end function

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	newId = 0

	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	newId = 0

	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	IncrementSequenceValue = CStr(newId)
End Function



Sub doc_numChange(Sender)
doc_num_omo.value = cint(sender.text) - 1
page2.visible = false
page3.visible = false
page4.visible = false
page5.visible = false

if Sender.value > 1 then page2.visible = true
if Sender.value > 2 then page3.visible = true
if Sender.value > 3 then page4.visible = true
if Sender.value > 4 then page5.visible = true

if Sender.value =1 then
  page1.top = 40
  page1.left = 66
end if

if Sender.value = 2 then
  page1.top = 24
  page1.left = 82
  page2.top = 40
  page2.left = 184
end if

if Sender.value = 3 then
  page1.top = 24
  page1.left = 130
  page2.top = 48
  page2.left = 32
  page3.top = 48
  page3.left = 232
end if

if Sender.value = 4 then
  page1.top = 24
  page1.left = 10
  page2.top = 32
  page2.left = 88
  page3.top = 40
  page3.left = 176
  page4.top = 56
  page4.left = 240
end if

if Sender.value = 5 then
  page1.top = 8
  page1.left = 130
  page2.top = 40
  page2.left = 64
  page3.top = 40
  page3.left = 192
  page4.top = 64
  page4.left = 8
  page5.top = 64
  page5.left = 240
end if

End sub

Sub add_tear(Sender)
  findcomponent("Panel" & mid(Sender.name,2,2)).visible = true
  char_counter(null)
  expander_shrinker
End sub

Sub delete_tear(Sender)
  findcomponent("Panel" & mid(Sender.name,2,2)).visible = false
  char_counter(null)
  expander_shrinker
End sub

Sub expander_shrinker()


if Panel1a.visible = true then
   doc_tears.height = 190
   BackdropImage.height = 312
end if
if Panel1b.visible = true then
   doc_tears.height = 294
   BackdropImage.height = 312
Else
   tear1b.UTF8Text=""
end if
if Panel2a.visible = true then
   doc_tears.height = 400
   BackdropImage.height = 410
Else
   tear2a.UTF8Text=""
end if
if Panel2b.visible = true then
   doc_tears.height = 499
   BackdropImage.height = 510
Else
   tear2b.UTF8Text=""
end if
if Panel3a.visible = true then
   doc_tears.height = 619
   BackdropImage.height = 626
Else
   tear3a.UTF8Text=""
end if
if Panel3b.visible = true then
   doc_tears.height = 715
   BackdropImage.height = 728
Else
   tear3b.UTF8Text=""
end if

end sub


Sub stop_per_tear_or_page(Sender)
    if stop1.checked = true then
        stop_omo.value = 0
        else
        stop_omo.value = 1
    end if
End sub

Sub pause_toggle_checked(Sender)
        if sender.checked = true then pause_omo.value = 1
        if sender.checked = false then pause_omo.value = 0
End sub

Sub char_counter(Sender)
  limit = 300

if Panel1b.visible = false then
 count1a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear1a.UTF8Text))
else
 count1a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear1a.UTF8Text) - Len(tear1b.UTF8Text))
 count1b.UTF8Text = "Characters Left: " & cstr(limit - Len(tear1a.UTF8Text) - Len(tear1b.UTF8Text))
end if

if Panel2b.visible = false then
 count2a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear2a.UTF8Text))
else
 count2a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear2a.UTF8Text) - Len(tear2b.UTF8Text))
 count2b.UTF8Text = "Characters Left: " & cstr(limit - Len(tear2a.UTF8Text) - Len(tear2b.UTF8Text))
end if

if Panel3b.visible = false then
 count3a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear3a.UTF8Text))
else
 count3a.UTF8Text = "Characters Left: " & cstr(limit - Len(tear3a.UTF8Text) - Len(tear3b.UTF8Text))
 count3b.UTF8Text = "Characters Left: " & cstr(limit - Len(tear3a.UTF8Text) - Len(tear3b.UTF8Text))
end if

End sub

Sub docNum_DropChange(Sender)

        doc_num.Value = docNum_Drop.ItemIndex + 1

End sub




Sub PTBlaunchClick(Sender)

      set wshShell = CreateObject("WSCript.shell")
      wshshell.run "PTPlugin.exe"
      set wshshell = nothing

End sub