' Desc: 'Custom' Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075

Function SC_SaveName
	Dim oldSaveNameSplit, nextValue, value	

	SC_SaveName = Int(Timer) & "FS_IN BACKPLATE "
	if BKDChouce.checked then
			SC_SaveName = SC_SaveName & "_BG IMAGE"
	else
			SC_SaveName = SC_SaveName & "_BB CARD"
	end if
	nextValue = GetSaveCount()
	
	' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
		' if the template has already been saved once, use the old prefix
		SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
	else
		' if the template is new then generate a new prefix
		SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
	end if

	' increments db sequence value
	IncrementSequenceValue()
end function

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	newId = 0

	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	newId = 0

	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	IncrementSequenceValue = CStr(newId)
End Function



Sub InitForm()
layoutDropChange(Sender)
SecondaryControl.ActiveLayerId = "1104x"
end sub

Sub layoutDropChange(Sender)
MAIN_BG.ActiveLayerId = "2001x"
if layoutDrop.itemindex = 0 then
BBCChoice.visible = true
Panel12.visible=true
TVTWLayerControl4.ActiveLayerID = "2250x"
lPanel.visible = false
TVTWLayerControl1.activeLayerID = "2011x"
        if BBCardChoice.checked = true then
                TVTWLayerControl3.ActiveLayerID = "2241x"
                rPanel.visible = true
                TTWImageInf5.Enabled = true
                TTWImageInf6.Enabled = true
                TTWImageInf5.visible = true
                TTWImageInf6.visible = true
                lPanel.visible = false
                BKD_IMAGE_PANEL.visible=false
                TVTWLayerControl2.activelayerID = "2220x"
        else
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl3.ActiveLayerID = "2240x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
        end if
elseif layoutDrop.itemindex = 1 then
BBCChoice.visible = true
TVTWLayerControl4.ActiveLayerID = "2250x"
rPanel.visible = false
TVTWLayerControl1.activeLayerID = "2014x"
TVTWLayerControl3.ActiveLayerID = "2240x"
BKD_IMAGE_PANEL.visible=false
TVTWLayerControl2.activelayerID = "2220x"
Panel12.visible=false
lPanel.visible = false
elseif layoutDrop.itemindex = 2 then
TVTWLayerControl1.activeLayerID = "2013x"
BBCChoice.visible = false
singleBBC.checked = true
Panel12.visible=true
        if BBCardChoice.checked = true then
                TVTWLayerControl3.ActiveLayerID = "2241x"
                TVTWLayerControl4.ActiveLayerID = "2251x"
                BKD_IMAGE_PANEL.visible=false
                rPanel.visible = true
                lPanel.visible = true
                TTWImageInf3.Enabled = true
                TTWImageInf4.Enabled = true
                TTWImageInf3.visible = true
                TTWImageInf4.visible = true
                TTWImageInf5.Enabled = true
                TTWImageInf6.Enabled = true
                TTWImageInf5.visible = true
                TTWImageInf6.visible = true
                TVTWLayerControl2.activelayerID = "2220x"
        else
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl4.ActiveLayerID = "2250x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
                TVTWLayerControl3.ActiveLayerID = "2240x"
        end if
elseif layoutDrop.itemindex = 3 then
BBCChoice.visible = true
Panel12.visible=true
TVTWLayerControl3.ActiveLayerID = "2240x"
rPanel.visible = false
TVTWLayerControl1.activeLayerID = "2012x"
        if BBCardChoice.checked = true then
                TVTWLayerControl4.ActiveLayerID = "2251x"
                rPanel.visible = false
                lPanel.visible = true
                TTWImageInf3.Enabled = true
                TTWImageInf4.Enabled = true
                TTWImageInf3.visible = true
                TTWImageInf4.visible = true
                BKD_IMAGE_PANEL.visible=false
                TVTWLayerControl2.activelayerID = "2220x"
        else
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl4.ActiveLayerID = "2250x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
        end if
end if
TWUniRadioButton1Click(Sender)
if liveVideoCheckbox.checked then
         LIVE_VIDEO_CHECKED(Sender)
end if
End sub

Sub BKDChouceClick(Sender)
layoutDropChange(Sender)
if BBCardChoice.checked = true then
BBCChoice.visible = true
        if layoutDrop.itemindex = 2 then
          BBCChoice.visible = false
        end if
else
BBCChoice.visible = false
end if

End sub

Sub TWUniRadioButton1Click(Sender)
if singleBBC.checked = true then
TDragEdit1.text = 0
TDragEdit2.text = 0
imageL2p.visible = false
imageR2p.visible = false
else
TDragEdit1.text = 1
TDragEdit2.text = 1
imageL2p.visible = true
imageR2p.visible = true
end if
End sub


Sub pToolboxClick(Sender)
      set wshShell = CreateObject("WSCript.shell")
      wshshell.run "PTPlugin.exe"
      set wshshell = nothing
End sub


Sub IMAGE_CHOICEClick(Sender)
if IMAGE_CHOICE.checked = true then
imageObjectChoice.text = "0"
panel10.visible= false
else
imageObjectChoice.text = "1"
panel10.visible= true
end if
End sub

Sub LIVE_VIDEO_CHECKED(Sender)
        if liveVideoCheckbox.checked then
                ' all out and invisible
                lPanel.visible = false
                rPanel.visible = false
                Panel15.visible = false
                BKD_IMAGE_PANEL.visible = false

                TVTWLayerControl2.ActiveLayerId = "2220x"
                TVTWLayerControl4.ActiveLayerId = "2250x"
                TVTWLayerControl3.ActiveLayerId = "2240x"

                ' in - live
                MAIN_BG.ActiveLayerId = "2000x"

        else
                ' all in and visible
                Panel15.visible = true
                BKD_IMAGE_PANEL.visible = true

                ' out - live
                MAIN_BG.ActiveLayerId = "2001x"


                layoutDropChange(Sender)
                BKDChouceClick(Sender)

        end if
End sub

Sub ImageOrTitle(Sender)
	if imageOrTitleCheckbox.checked = true then
		titlePanel.visible = false
		imageTitlePanel.visible = true
		TitleTypeControl.value = 1
	else
		titlePanel.visible = true
		imageTitlePanel.visible = false
		TitleTypeControl.value = 0
	End if
end sub