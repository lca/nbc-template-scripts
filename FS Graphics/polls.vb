' Desc: Polls Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075

Sub InitForm
	numLinesChange(Sender)
	headshotCheckboxClick(Sender)
	comparisonNumberChange(Sender)
	comparisonCheckboxClick(Sender)
	
End Sub

Function SC_SaveName
	if headshotCheckbox.checked = true then
		SC_SaveName = "FS_HS POLL: " & " (" & numLines.text & ") " & TXTTitleL1.text & " - " & subtitleText1.text
	else
		SC_SaveName = "FS_METER POLL: " & " (" & numLines.text & ") " & TXTTitleL1.text & " - " & subtitleText1.text
	end if
	
	nextValue = GetSaveCount()
	
' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
		SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
	else
' if the template is new then generate a new prefix
		SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
	end if
	
' increments db sequence value
	IncrementSequenceValue()
end function

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	IncrementSequenceValue = CStr(newId)
End Function

' BEGIN ======================================================================
' ============================= ACTIVE LAYER ID CONTROLS =====================
' ============================================================================

Sub ManageActiveLayerIds()
	'TVTWLayerControl1.ActiveLayerId = "2012x"
	'TVTWLayerControl2.ActiveLayerId = "2220x"
	'TVTWLayerControl3.ActiveLayerId = "2240x"
	'TVTWLayerControl4.ActiveLayerId = "2250x"
	
	' SET BACKGROUND ACTIVE LAYER ID
	ManageBackgroundActiveLayerId()
	
	' SET PLATE ACTIVE LAYER ID
	ManagePlateActiveLayerId()
	
	' SET POLLS PLATE ACTIVE LAYER ID
	ManagePollsPlateActiveLayerId()
	
	' SET LEFT/RIGHT PLATE CONTROL
	ManageLeftRightPlateActiveLayerId()

	' SET LIVE VIDEO BG ACTIVE LAYER ID
	ManageLiveBackgroundActiveLayerId()
	
End Sub

' CONTROL BACKGROUND ACTIVE LAYER ID
Sub ManageBackgroundActiveLayerId()
	Dim value : value = "2000x"
	
	' if headshot on left or headshot on right and background image checkbox selected then background IN
	if (PicLeftRadio.checked or PicRightRadio.checked) and bgImage.checked then
		value = "2001x"
	end if
	
	MAIN_BG.ActiveLayerId = value
End Sub

Sub ManageLiveBackgroundActiveLayerId()
	Dim value : value = "2220x"
	
	
	if not CenterRadio.checked and not CenterSmallRadio.checked and bgImage.checked then
		value = "2221x"
	end if
	
	backgroundImageControl.ActiveLayerId = value
End Sub

' CONTROL PLATE ACTIVE LAYER ID
Sub ManagePlateActiveLayerId()
	Dim value : value = ""
	
	if PicLeftRadio.checked then
		value = "2012x"
	elseif PicRightRadio.checked then
		value = "2011x"
	elseif CenterRadio.checked then
		value = "2014x"
	else
		value = "2013x"
	end if
	
	PlateControl.ActiveLayerId = value
End Sub

' CONTROL POLLS PLATE ACTIVE LAYER ID
Sub ManagePollsPlateActiveLayerId()
	Dim value : value = ""
	
	if PicLeftRadio.checked or PicRightRadio.checked then
		if headshotCheckbox.checked then
			value = "1012x"
		else
			value = "1015x"
		end if
	elseif CenterRadio.checked then
		if headshotCheckbox.checked then
			value = "1013x"
		else
			value = "1016x"
		end if
	else
		if headshotCheckbox.checked then
			value = "1011x"
		else
			value = "1014x"
		end if
	end if
	
	PollsControl.ActiveLayerId = value
End Sub

' CONTROL LEFT/RIGHT  PLATE ACTIVE LAYER ID
Sub ManageLeftRightPlateActiveLayerId()
	Dim leftValue : leftValue = ""
	Dim rightValue : rightValue = ""
	
	if PicLeftRadio.checked then
		if bgImage.checked then
			leftValue = "2250x"
		else
			leftValue = "2251x"
		end if
		
		rightValue = "2240x"
	elseif PicRightRadio.checked then
		if bgImage.checked then
			rightValue = "2240x"
		else
			rightValue = "2241x"
		end if
		leftValue = "2250x"
	elseif CenterRadio.checked or CenterSmallRadio.checked then
		leftValue = "2250x"
		rightValue = "2240x"
	end if
	
	LeftPlateControl.ActiveLayerId = leftValue
	RightPlateControl.ActiveLayerId = rightValue
End Sub

' HEADSHOT CHECKBOX CLICK EVENT
Sub headshotCheckboxClick(Sender)
	numLinesChange(Sender)
End sub

' END ========================================================================
' ============================= ACTIVE LAYER ID CONTROLS =====================
' ============================================================================

Sub VALUE_CHANGED(Sender)
	subline = Right(Sender.name, 1)
	line = Left(Right(Sender.name, 3), 1)
	'msgbox line & " - " & subline
	FindComponent("TXT_L" & line & "D" & subline & "_dec").Text = Sender.Text
	
	SetDivisoryLineVisibility()
End Sub

Sub SetDivisoryLineVisibility()
	dim result : result = true
	dim outerLimit : outerLimit = CInt(numLines.Text)
	dim innerLimit : innerLimit = CInt(comparisonNumberDrop.Text)
'msgbox CStr(outerLimit) & "_" & CStr(innerLimit)
	if comparisonCheckbox.checked = false then
		innerLimit = 1
	end if
	
	
	for i=1 to outerLimit
		for j=1 to innerLimit
			if InStr(FindComponent("value" & CStr(i) & "_" & CStr(j)).text, ".") or CDbl(FindComponent("value" & CStr(i) & "_" & CStr(j)).text) >= 100 then
				result = false
			end if
		next
	next
	
	divisoryLine.Checked = result
End Sub

' EVENT TRIGGERED WHEN NUMBER OF LINES IS CHANGED
Sub numLinesChange(Sender)
	
	if CenterSmallRadio.checked then
		SetCenterSmallDefaults()
	elseif PicRightRadio.checked then
		SetRightRadioDefaults()
	elseif PicLeftRadio.checked then
		SetLefttRadioDefaults()
	else
		SetCenterRadioDefaults()
	end if
	
	ManageActiveLayerIds()
End Sub

' SETS THE DEFAULT SETTINGS FOR A CENTER SMALL POSITIONING (i.e. enables/disables template functionality)
Sub SetCenterSmallDefaults()
	select case numLines.ItemIndex
		case 0
			if headshotCheckbox.checked then
				headshotCheckbox.checked = false
				headshotCheckboxClick(headshotCheckbox)
				SetCenterSmallDefaults()
			else
				ResetComparisonCheckboxValue()
			end if
		case 1
			ResetComparisonCheckboxValue()
		case 2
			ResetComparisonCheckboxValue()
		case 3
			ResetComparisonCheckboxValue()
		case 4
			ResetComparisonCheckboxValue()
		case 5
			ResetComparisonCheckboxValue()
	end select	
End Sub

Sub ResetComparisonCheckboxValue()
	' if comparison is 3 or more -> reset to 2
	if comparisonCheckbox.checked and comparisonNumberDrop.ItemIndex = 1 then
		comparisonNumberDrop.ItemIndex = 0
		
		' call dropdown changed event
		comparisonNumberChange(comparisonNumberDrop)
	end if
End Sub

' SETS THE DEFAULT SETTINGS FOR A RIGHT POSITIONING (i.e. enables/disables template functionality)
Sub SetRightRadioDefaults()
	select case numLines.ItemIndex
		case 0
			if headshotCheckbox.checked then
				headshotCheckbox.checked = false
				headshotCheckboxClick(headshotCheckbox)
				SetCenterSmallDefaults()
			else
				ResetComparisonCheckboxValue()
			end if
		case 1
			ResetComparisonCheckboxValue()
		case 2
			ResetComparisonCheckboxValue()
		case 3
			ResetComparisonCheckboxValue()
		case 4
			ResetComparisonCheckboxValue()
		case 5
			ResetComparisonCheckboxValue()
	end select
	
	
End Sub

' SETS THE DEFAULT SETTINGS FOR A LEFT POSITIONING (i.e. enables/disables template functionality)
Sub SetLefttRadioDefaults()
	
End Sub

' SETS THE DEFAULT SETTINGS FOR A CENTER POSITIONING (i.e. enables/disables template functionality)
Sub SetCenterRadioDefaults()
	
End Sub

Sub LinePollClick(Sender)
	
End sub



' triggered when comparison option is selected/deselected
Sub comparisonCheckboxClick(Sender)
	
End sub

Sub comparisonNumberChange(Sender)
	if comparisonCheckbox.checked then
		LineNumber.visible = true
	else
		LineNumber.visible = false
	end if
	
	numLinesChange(Sender)
End sub

Sub PositionRadioClick(Sender)

	ManageActiveLayerIds()
	
	ManageImageTypeVisibilities(Sender)
	
	numLinesChange(Sender)
End Sub

' SETS VISIBILITY OF IMAGE TYPE ITEMS
Sub ManageImageTypeVisibilities(Sender)
	if CenterRadio.checked or CenterSmallRadio.checked then
		imageTypeSelectionBox.visible = false
		bgImageBOx.visible = false
		
		liveVideoCheckbox.visible = true
		
		SetVisibility "rightHeadshotPanel", false, 1, 2
		SetVisibility "leftHeadshotPanel", false, 1, 2
	else
		imageTypeSelectionBox.visible = true
		
		if bgImage.checked then
			' if BG Image radio is selected then hide side images and # of images option
			imageNumberBox.visible = false
			bgImageBOx.visible = true
			liveVideoCheckbox.visible = false
			
			SetVisibility "rightHeadshotPanel", false, 1, 2
			SetVisibility "leftHeadshotPanel", false, 1, 2
			
		else
			imageNumberBox.visible = true
			bgImageBOx.visible = false
			liveVideoCheckbox.visible = true
			
			ManageLeftRightPlateActiveLayerId()
			
			if PicRightRadio.checked then
				' hide left inputs
				SetVisibility "leftHeadshotPanel", false, 1, 2
				
				if numImagesDrop.ItemIndex = 0 then
					SetVisibility "rightHeadshotPanel", true, 1, 1
					SetVisibility "rightHeadshotImage", true, 1, 1
					SetVisibility "rightHeadshotPanel", false, 2, 2
				else
					SetVisibility "rightHeadshotPanel", true, 1, 2
					SetVisibility "rightHeadshotImage", true, 1, 2
				end if
			else
				' hide right inputs
				SetVisibility "rightHeadshotPanel", false, 1, 2
			
				if numImagesDrop.ItemIndex = 0 then
					SetVisibility "leftHeadshotPanel", true, 1, 1
					SetVisibility "leftHeadshotImage", true, 1, 1
					SetVisibility "leftHeadshotPanel", false, 2, 2
				else
					SetVisibility "leftHeadshotPanel", true, 1, 2
					SetVisibility "leftHeadshotImage", true, 1, 1
				end if
			end if
		end if
	end if
End Sub

' by number of images, display correct image inputs
Sub NumImages(Sender)
	
End Sub

Sub IMAGE_TYPE(Sender)
	
End sub

Sub SetYPositioning()
	
End Sub

Sub UpdateValues(Sender)
	for i=1 to 6
		for j=1 to 3
			FindComponent("value" & i & "_" & j)
		next
	next
End Sub

Sub LIVE_VIDEO_CHECKED(Sender)
	if liveVideoCheckbox.checked then
		'if imageTypeSelectionBox.visible and bgImage.checked then
			'liveVideoCheckbox.checked = false
			'msgbox "You cannot run live video and still have a background. Please disable the background option first.", 0, "WARNING"
		'else
			MAIN_BG.ActiveLayerId = "2001x"
		'end if
		
	else
		MAIN_BG.ActiveLayerId = "2000x"
	end if
	
	
End sub

Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub


' ============================================================================================
' ============================== TEXT BOX COUNT CONTROLS =====================================
' ============================================================================================

Sub sourceTextChange(Sender)
	LinePollSourceLabel.Caption = "Source - Characters Left: " & 20 - Len(sourceText.Text)
End sub

Sub First1Change(Sender)
	grpFirst1.Caption = "First Name Char Left: " & First1.MaxLength - Len(First1.Text)
End sub

Sub First2Change(Sender)
	grpFirst2.Caption = "First Name Char Left: " & First2.MaxLength - Len(First2.Text)
End sub

Sub First3Change(Sender)
	grpFirst3.Caption = "First Name Char Left: " & First3.MaxLength - Len(First3.Text)
End sub

Sub First4Change(Sender)
	grpFirst4.Caption = "First Name Char Left: " & First4.MaxLength - Len(First4.Text)
End sub

Sub First5Change(Sender)
	grpFirst5.Caption = "First Name Char Left: " & First5.MaxLength - Len(First5.Text)
End sub

Sub First6Change(Sender)
	grpFirst6.Caption = "First Name Char Left: " & First6.MaxLength - Len(First6.Text)
End sub

Sub Last1Change(Sender)
	grpLast1.Caption = "Last Name Char Left: " & Last1.MaxLength - Len(Last1.Text)
End sub

Sub Last2Change(Sender)
	grpLast2.Caption = "Last Name Char Left: " & Last2.MaxLength - Len(Last2.Text)
End sub

Sub Last3Change(Sender)
	grpLast3.Caption = "Last Name Char Left: " & Last3.MaxLength - Len(Last3.Text)
End sub

Sub Last4Change(Sender)
	grpLast4.Caption = "Last Name Char Left: " & Last4.MaxLength - Len(Last4.Text)
End sub

Sub Last5Change(Sender)
	grpLast5.Caption = "Last Name Char Left: " & Last5.MaxLength - Len(Last5.Text)
End sub

Sub Last6Change(Sender)
	grpLast6.Caption = "Last Name Char Left: " & Last6.MaxLength - Len(Last6.Text)
End sub

Sub Drop1Change(Sender)
	grpDrop1.Caption = "Dropline Characters Left: " & Drop1.MaxLength - Len(Drop1.Text)
End sub

Sub Drop2Change(Sender)
	grpDrop2.Caption = "Dropline Characters Left: " & Drop2.MaxLength - Len(Drop2.Text)
End sub

Sub Drop3Change(Sender)
	grpDrop3.Caption = "Dropline Characters Left: " & Drop3.MaxLength - Len(Drop3.Text)
End sub

Sub Drop4Change(Sender)
	grpDrop4.Caption = "Dropline Characters Left: " & Drop4.MaxLength - Len(Drop4.Text)
End sub

Sub Drop5Change(Sender)
	grpDrop5.Caption = "Dropline Characters Left: " & Drop5.MaxLength - Len(Drop5.Text)
End sub

Sub Drop6Change(Sender)
	grpDrop6.Caption = "Dropline Characters Left: " & Drop6.MaxLength - Len(Drop6.Text)
End sub

Sub name1Change(Sender)
	HeadNTLabel1.Caption = "Name - Characters Left: " & name1.MaxLength - Len(name1.Text)
End sub

Sub name2Change(Sender)
	HeadNTLabel2.Caption = "Name - Characters Left: " & name2.MaxLength - Len(name2.Text)
End sub

Sub name3Change(Sender)
	HeadNTLabel3.Caption = "Name - Characters Left: " & name3.MaxLength - Len(name3.Text)
End sub

Sub name4Change(Sender)
	HeadNTLabel4.Caption = "Name - Characters Left: " & name4.MaxLength - Len(name4.Text)
End sub

Sub name5Change(Sender)
	HeadNTLabel5.Caption = "Name - Characters Left: " & name5.MaxLength - Len(name5.Text)
End sub

Sub name6Change(Sender)
	HeadNTLabel6.Caption = "Name - Characters Left: " & name6.MaxLength - Len(name6.Text)
End sub

Sub titleText1Change(Sender)
	titleText1Label.Caption = "Line Poll Title - Characters Left: " &  CStr(30 - Len(titleText1.UTF8Text))
	TXTTitleL1.TExt = Sender.Text
	titleText2.Text = Sender.Text
End sub

Sub titleText2Change(Sender)
	titleText2Label.Caption = "Line Poll Title - Characters Left: " &  CStr(25 - Len(titleText2.UTF8Text))
	TXTTitleL1.TExt = Sender.Text
	titleText1.Text = Sender.Text
End sub

Sub SUBTITLE_CHANGED(Sender)
	subtitleTextArea.Text = subtitleText1.Text & vbCrLf & subtitleText2.Text
	SetYPositioning()
End Sub

Sub ST_1Change(Sender)
	TXTTitleL2.text = ST_1.text + vbNewLine + ST_2.text
End sub

Sub ST_2Change(Sender)
	TXTTitleL2.text = ST_1.text + vbNewLine + ST_2.text
End sub

Sub NT1Change(Sender)
	NTLabel1.Caption = "Name - Characters Left: " & NT1.MaxLength - Len(NT1.Text)
End sub

Sub NT2Change(Sender)
	NTLabel2.Caption = "Name - Characters Left: " & NT2.MaxLength - Len(NT2.Text)
End sub

Sub NT3Change(Sender)
	NTLabel3.Caption = "Name - Characters Left: " & NT3.MaxLength - Len(NT3.Text)
End sub

Sub NT4Change(Sender)
	NTLabel4.Caption = "Name - Characters Left: " & NT4.MaxLength - Len(NT4.Text)
End sub

Sub NT5Change(Sender)
	NTLabel5.Caption = "Name - Characters Left: " & NT5.MaxLength - Len(NT5.Text)
End sub

Sub NT6Change(Sender)
	NTLabel6.Caption = "Name - Characters Left: " & NT6.MaxLength - Len(NT6.Text)
End sub

Sub SubtitleText1Changed(Sender)
	LinePollSubLabel1.Caption = "Subtitle 1 - Characters Left: " & 25 - Len(subtitleText1.Text)
	SUBTITLE_CHANGED(Sender)
End Sub

Sub SubtitleText2Changed(Sender)
	LinePollSubLabel2.Caption = "Subtitle 2 - Characters Left: " & 25 - Len(subtitleText2.Text)
	SUBTITLE_CHANGED(Sender)
End Sub


Sub PTBlauncherClick(Sender)
	set wshShell = CreateObject("WSCript.shell")
	wshshell.run "PTPlugin.exe"
	set wshshell = nothing
	
End sub