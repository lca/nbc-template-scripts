' Desc: Line By Line Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 122914T1040

Sub InitForm
        EnablePagesChecked(Sender)
        PositionChanged(Sender)
End Sub

' TRIGGERED WHEN THE NUMBER OF HEADSHOTS HAS BEEN CHANGED
Sub numberOfLinesDropdownChanged(Sender)
        revealCheckbox.visible = true
        revealCheckboxChecked(Sender)
        ' trigger multi-page checkbox event
        EnablePagesChecked(Sender)

        NumberPollsControl.Value = numberOfLinesDropdown.ItemIndex
        NumberLinesOMOControl.Value = CInt(numberOfLinesDropdown.Text) - 2

        ' CASE: More then 4 lines and on side panel
        if CInt(numberOfLinesDropdown.Text) > 4 and not CenterBigRadio.checked then
                multipageCheckbox.visible = true
                'multipageCheckboxChecked(Sender)

                ' CASE: 6 lines
                if CInt(numberOfLinesDropdown.Text) > 5 then
                        ' show all subtitles
                        SubtitleLabel.visible = true
                        subtitle1.visible = true

                        subtitleLabel2.visible = true
                        subtitle2.visible = true
                end if

                ' disable reveal
                revealCheckbox.visible = true
                revealCheckboxChecked(Sender)

                ' trigger multi-page checkbox event
                EnablePagesChecked(Sender)




        else

                ' hide multipage checkbox
                multipageCheckbox.visible = false


                ' CASE: More then 4 lines and on center panel
                if CInt(numberOfLinesDropdown.Text) > 5 and CenterBigRadio.checked then

                        ' hide second subtitle
                        subtitleLabel2.visible = false
                        subtitle2.visible = false
                        subtitle2.Text = ""

                        ' trigger multi-page checkbox event
                        EnablePagesChecked(Sender)
                else
                        ' show all subtitles
                        SubtitleLabel.visible = true
                        subtitle1.visible = true

                        subtitleLabel2.visible = true
                        subtitle2.visible = true
                end if

                revealCheckboxChecked(Sender)
        end if


        ' show text panels and lines
        SetVisibility "TextLinePanel", true, 1, CInt(numberOfLinesDropdown.Text)
        SetVisibility "TextLinePanel", false, CInt(numberOfLinesDropdown.Text) + 1, 6
        ClearTexts "textLine", CInt(numberOfLinesDropdown.Text) + 1, 6

        ' if multi-page not selected
        if not multipageCheckbox.checked then

                ' if 6 lines selected
                if CInt(numberOfLinesDropdown.Text) = 6 then
                        ' show first subtitle
                        SubtitleLabel.visible = true
                        subtitle1.visible = true
                end if

                ' Shift positioning of graphic
                ' TODO: Consider moving this outside of the current if.
                ShiftPositioning(Sender)
        else
                ' Shift positioning of graphic
                ' TODO: Consider removing this line and calling the ShiftPositioning subroutine
                if CInt(numberOfLinesDropdown.Text) > 4 and multipageCheckbox.visible then
                        select case NumberOfSubtitles
                                case 0
                                        positionControl.YValue = 125
                                case 1
                                        positionControl.YValue = 110
                                case 2
                                        'positionControl.YValue = -10
                                        positionControl.YValue = 95
                        end select
                end if
        end if
End sub

' SETS THE Y POSITIONING OF THE TEXT FIELDS DEPENDING ON LINE NUMBERS, PAGE OPTIONS AND POSITIONING OF PANEL
Sub ShiftPositioning(Sender)
        Dim yValue : yValue = 0.0
        if CenterBigRadio.checked then
                if CInt(numberOfLinesDropdown.Text) = 2 then
                        select case NumberOfSubtitles
                                case 0
                                        yValue = 95
                                case 1
                                        yValue = 85
                                case 2
                                        yValue = 70
                        end select
                elseif CInt(numberOfLinesDropdown.Text) = 3 then
                        select case NumberOfSubtitles
                                case 0
                                        yValue = 100
                                case 1
                                        yValue = 85
                                case 2
                                        yValue = 80
                        end select
                elseif CInt(numberOfLinesDropdown.Text) = 4 then
                        select case NumberOfSubtitles
                                case 0
                                        yValue = 120
                                case 1
                                        yValue = 100
                                case 2
                                        yValue = 80
                        end select
                elseif CInt(numberOfLinesDropdown.Text) = 5 then
                        select case NumberOfSubtitles
                                case 0
                                        yValue = 120
                                case 1
                                        yValue = 95
                                case 2
                                        yValue = 75
                        end select
                elseif CInt(numberOfLinesDropdown.Text) = 6 then
                        select case NumberOfSubtitles
                                case 0
                                        yValue = 130
                                case 1
                                        yValue = 100
                        end select
                end if
        else
                select case CInt(numberOfLinesDropdown.Text)
                        case 2
                                select case NumberOfSubtitles
                                        case 0
                                                yValue = 100
                                        case 1
                                                yValue = 80
                                        case 2
                                                yValue = 75
                                end select
                        case 3
                                select case NumberOfSubtitles
                                        case 0
                                                yValue = 110
                                        case 1
                                                yValue = 100
                                        case 2
                                                yValue = 80
                                end select

                        case 4
                                select case NumberOfSubtitles
                                        case 0
                                                yValue = 125
                                        case 1
                                                yValue = 100
                                        case 2
                                                yValue = 80
                                end select

                        case 5
                                select case NumberOfSubtitles
                                        case 0
                                                yValue = 125
                                        case 1
                                                yValue = 95
                                        case 2
                                                yValue = 75
                                end select

                        case 6
                                select case NumberOfSubtitles
                                        case 0
                                                yValue = 110
                                        case 1
                                                yValue = 125
                                end select

                end select
        end if

        if CInt(numberOfLinesDropdown.Text) > 4 and multipageCheckbox.visible then
                select case NumberOfSubtitles
                        case 0
                                yValue = 125
                        case 1
                                yValue = 100
                        case 2
                                yValue = 95
                end select
        end if
        'msgbox "Setting position to: " & yValue
        positionControl.YValue = yValue
End Sub

' RETURNS THE NUMBER OF SUBTITLE BEING USED
Function NumberOfSubtitles()
        Dim returnVal : returnVal = 0

        if subtitle1.Text <> "" then
                returnVal = returnVal + 1
        end if

        if subtitle2.Text <> "" then
                returnVal = returnVal + 1
        end if

        NumberOfSubtitles = returnVal
End Function

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
        for i=start to finish
                FindComponent(componentName & CStr(i)).visible = value
        next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
        for i=start to finish
                FindComponent(componentName & CStr(i)).Text = ""
        next
End Sub

' TRIGGERED WHEN THE POSITIONING OF THE SCREEN ELEMENTS
Sub PositionChanged(Sender)
        OMOSizeControl.value = 0

        if CenterBigRadio.checked then

                ' set plate position active layer id
                FSPlateControl.ActiveLayerId = "2014x"

                ' hide bg image panel
                BGImagePanel.visible = false

                ' set side panels to out
                RightPlateControl.ActiveLayerId = "2240x"
                LeftPlateControl.ActiveLayerId = "2250x"

                ImageChoicePanel.visible = false

                titleTypeControl.Value = 1

                OMOSizeControl.value = 1

                BPControl.ActiveLayerId = "2220x"

        elseif LeftRadio.checked then

                ' set plate position active layer id
                FSPlateControl.ActiveLayerId = "2011x"

                RightPlateControl.ActiveLayerId = "2241x"
                LeftPlateControl.ActiveLayerId = "2250x"

                ImageChoicePanel.visible = true

                titleTypeControl.Value = 0
        else

                ' set plate position active layer id
                FSPlateControl.ActiveLayerId = "2012x"

                RightPlateControl.ActiveLayerId = "2240x"
                LeftPlateControl.ActiveLayerId = "2251x"

                ImageChoicePanel.visible = true

                titleTypeControl.Value = 0
        end if

        ImageChoiceChanged(Sender)
        numberOfLinesDropdownChanged(Sender)
        IconChanged(Sender)
End Sub

' TRIGGERED WHEN THE OVER LIVE VIDEO CHECKBOX HAS BEEN CHANGED
Sub OverLiveVideoCheckboxClicked(Sender)
        if overLiveVideoCheckbox.checked then
                ImageChoicePanel.visible = false
                BGImagePanel.visible = false

                BPControl.ActiveLayerId = "2220x"
                BGControl.ActiveLayerId = "2000x"

                RightPlateControl.ActiveLayerId = "2240x"
                LeftPlateControl.ActiveLayerId = "2250x"


        else
                BGControl.ActiveLayerId = "2001x"

                if not CenterBigRadio.checked then
                        ImageChoicePanel.visible = true
                        ImageChoiceChanged(Sender)
                end if
        end if
End sub

' TRIGGERED WHEN THE TYPE OF IMAGE TO DISPLAY IS CHANGED
Sub ImageChoiceChanged(Sender)
        if BGRadio.checked then
                if not CenterBigRadio.checked then
                        firstHeadshotPanel.visible = false
                        secondHeadshotPanel.visible = false
                        HeadshotCountPanel.visible = false
                        BGImagePanel.visible = true

                        ' set bp active layer id to IN
                        BPControl.ActiveLayerId = "2221x"
                        ' set bg active layer id to IN
                        BGControl.ActiveLayerId = "2001x"

                        RightPlateControl.ActiveLayerId = "2240x"
                        LeftPlateControl.ActiveLayerId = "2250x"
                end if
        else
                BGImagePanel.visible = false
                HeadshotCountPanel.visible = true

                BPControl.ActiveLayerId = "2220x"

                if LeftRadio.checked then
                        RightPlateControl.ActiveLayerId = "2241x"
                        LeftPlateControl.ActiveLayerId = "2250x"
                elseif RightRadio.checked then
                        RightPlateControl.ActiveLayerId = "2240x"
                        LeftPlateControl.ActiveLayerId = "2251x"
                end if

                select case HeadshotCountDrop.Text
                        case "1"
                                firstHeadshotPanel.visible = true
                                secondHeadshotPanel.visible = false
                                RightHeadshotCountControl.value = 0
                                LeftHeadshotCountControl.value = 0
                        case "2"
                                firstHeadshotPanel.visible = true
                                secondHeadshotPanel.visible = true
                                RightHeadshotCountControl.value = 1
                                LeftHeadshotCountControl.value = 1
                end select
        end if
        FirstHeadshotImageChanged(Sender)
        SecondHeadshotImageChanged(Sender)
End Sub

' TRIGGERED WHEN THE IMAGE TITLE CHECKBOX IS MODIFIED
Sub ImageAsTitleCheckboxClicked(Sender)
        if imageAsTitleCheckbox.checked then
                ' set omo
                imageAsTitleControl.value = 1

                ' hide title panel
                TitlePanel.visible = false

                ' show image title panel
                TitleImagePanel.visible = true
        else
                ' set omo
                imageAsTitleControl.value = 0

                ' show title panel
                TitlePanel.visible = true

                ' hide image title panel
                TitleImagePanel.visible = false
        end if
End sub


' TRIGGERED WHEN SECOND HEADSHOT IMAGE IS CHANGED
Sub SecondHeadshotImageChanged(Sender)
        if LeftRadio.checked then
                RightImageControl2.PicFilename = HeadshotImage2.PicFilename
                RightImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
        else
                LeftImageControl2.PicFilename = HeadshotImage2.PicFilename
                LeftImageControl2.ThumbFilename = HeadshotImage2.ThumbFilename
        end if
End Sub

' TRIGGERED WHEN FIRST HEADSHOT IMAGE IS CHANGED
Sub FirstHeadshotImageChanged(Sender)
        if LeftRadio.checked then
                RightImageControl1.PicFilename = HeadshotImage1.PicFilename
                RightImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
        else
                LeftImageControl1.PicFilename = HeadshotImage1.PicFilename
                LeftImageControl1.ThumbFilename = HeadshotImage1.ThumbFilename
        end if
End Sub

Sub SubtitleChanged(Sender)
        SubtitleControl.Text = subtitle1.Text & vbNewLine & subtitle2.Text
        ShiftPositioning(Sender)
End sub

' COPIES IMAGE ICON FROM VISIBLE IMAGE TO ACTUAL IMAGE CONTROLS
Sub UpdateImageIcons(Sender)
        for i=1 to 6
                FindComponent("ImageIcon" & i).PicFilename = ImageIcon.PicFilename
                FindComponent("ImageIcon" & i).ThumbFilename = ImageIcon.ThumbFilename
        next
End sub

' TRIGGERED WHEN THE ICON SET SELECTION IS CHANGED
Sub IconChanged(Sender)
        SetVisibility "ImageIcon", false, 1, 6
        if defaultBulletRadio.checked then
                IconOmo.Value = 0
        elseif imageBulletRadio.checked then
                SetVisibility "ImageIcon", true, 1, CInt(numberOfLinesDropdown.Text)
                SetVisibility "ImageIcon", false, CInt(numberOfLinesDropdown.Text) + 1, 6
                IconOmo.Value = 3
        elseif letterBulletRadio.checked then
                IconOmo.Value = 2
        else
                IconOmo.Value = 1
        end if
End sub

' TRIGGERED WHEN THE ENABLE PAGES CHECKBOX IS MODIFIED
Sub EnablePagesChecked(Sender)
        if multipageCheckbox.visible and multipageCheckbox.checked then
                MainLayerControl.ActiveLayerId = "1123xb"
                if not CenterBigRadio.checked then
                        SubtitleLabel.visible = true
                        subtitle1.visible = true
                        subtitleLabel2.visible = true
                        subtitle2.visible = true
                end if

        else


                if not CenterBigRadio.checked and (not multipageCheckbox.visible or not multipageCheckbox.checked ) then
                        if  CInt(numberOfLinesDropdown.Text) > 4 then
                                subtitleLabel2.visible = false
                                subtitle2.visible = false
                                subtitle2.Text = ""
                        end if
                else
                        'subtitleLabel2.visible = true
                        'subtitle2.visible = true
                        'subtitle2.Text = ""
                end if


        end if
End sub

' TRIGGERED WHEN THE MULTIPAGE CHECKBOX IS MODIFIED
Sub multipageCheckboxChecked(Sender)
        EnablePagesChecked(Sender)
        revealCheckboxChecked(Sender)
End sub

Sub revealCheckboxChecked(Sender)
        ' if revealCheckbox.checked then
        '         multipageCheckbox.visible = false
        '         multipageCheckbox.checked = false
        ' else
        '         if CInt(numberOfLinesDropdown.Text > 4) and not CenterBigRadio.checked then
        '                 multipageCheckbox.visible = true
        '         end if
        ' end if

        EnablePagesChecked(Sender)

        if revealCheckbox.checked then
                    MainLayerControl.ActiveLayerId = "1123xc"
                    revealNoRevealControl.Value = 0
            else
                    MainLayerControl.ActiveLayerId = "1123x"
                    revealNoRevealControl.Value = 1
            end if

           if multipageCheckbox.checked and multipageCheckbox.visible then
                    MainLayerControl.ActiveLayerId = "1123xb"
            end if
End Sub

' TRIGGERED ON CLICK OF LAUNCH PBT
Sub PTBlauncherClick(Sender)
        set wshShell = CreateObject("WSCript.shell")
        wshshell.run "PTPlugin.exe"
        set wshshell = nothing
End sub
