' FORM INITIALIZATION
Sub InitForm
	TabChanged(TabPane)
End Sub

' TRIGGERED WHEN A DIFFERENT TAB IS SELECTED
Sub TabChanged(Sender)
	
	select case TabPane.TabIndex
		case 0
			' set media type to book
			MediaTypeOMO.Text = 1
		case 1
			' set media type to magazine
			MediaTypeOMO.Text = 0
			
			' set active scene
			MediaLayerControl.ItemIndex = 2
		case 2
			' set active scene
			MediaLayerControl.ItemIndex = 5
			TitlePanel.visible = false
			SourcePanel.visible = false
			SourceText.Text = ""
			
			DVDImage.visible = true
			DVDImage.enabled = true
	end select
	
	AnimationTypeChange(Sender)
End sub

' TRIGGERED WHEN BOOKIO COVER IMAGE IS CHANGED
Sub BookFilenameChanged(Sender)
	BookImage.PicFilename = Sender.PicFilename
	BookImage.ThumbFilename = Sender.ThumbFilename
End sub

' TRIGGERED WHEN BOOKIO COVER IMAGE IS CHANGED
Sub MagazineFilenameChanged(Sender)
	MagazineImage.PicFilename = Sender.PicFilename
	MagazineImage.ThumbFilename = Sender.ThumbFilename
End sub

Sub AnimationTypeChange(Sender)
	magazineImageOptionPanel.visible = false
	ImageAsTitleCheckbox.visible = false
	select case TabPane.TabIndex
		case 0
			BookBodyTextPanel.visible = false
			
			' hide text panel(s)
			BookBodyTextPanel.visible = false
			BookPlateTextPanel.visible = false
			SourcePanel.visible = false
			SourceText.Text = ""
			' hide title panel
			TitlePanel.visible = false
					
			select case BookAnimationTypeDrop.ItemIndex 
				case 0
					' set active scene
					MediaLayerControl.ItemIndex = 1
					
				case 1
					' set active scene
					MediaLayerControl.ItemIndex = 2
					
					' set top pixels for text panel
					BookBodyTextPanel.Top = 93
					
					' show text panel
					BookBodyTextPanel.visible = true
					BookBodyNumberPagesChanged(Sender)
					CopyText "BookBody"
					
					' show title panel
					TitlePanel.visible = true
					SourcePanel.visible = true
					
					' enable option for image as title
					ImageAsTitleCheckbox.visible = true
				case 2
					' set active scene
					MediaLayerControl.ItemIndex = 4
					
					' show text panel
					BookPlateTextPanel.visible = true
					BookPlateNumberPagesChanged(Sender)
					CopyText "BookPlate"
					
			end select

		case 1
		
			' hide text panel(s)
			MagazineBodyTextPanel.visible = false
			MagazinePlateTextPanel.visible = false
			SourcePanel.visible = false
			SourceText.Text = ""
			
			' hide title panel
			TitlePanel.visible = false
			
			select case MagazineAnimationTypeDrop.ItemIndex 
				' CENTER
				case 0 
					' set active scene
					MediaLayerControl.ItemIndex = 1
				
				' SIDE PANEL
				case 1
					MagazineBodyTextPanel.visible = true
					
					' set active scene
					MediaLayerControl.ItemIndex = 2
					
					' set top pixels for text panel
					MagazineBodyTextPanel.Top = 93
					
					' show text panel
					MagazineBodyTextPanel.visible = true
					MagazineBodyNumberPagesChanged(Sender)
					
					' show title panel
					TitlePanel.visible = true
					
					SourcePanel.visible = true
					
					CopyText "MagazineBody"
					
					' enable option for image as title
					ImageAsTitleCheckbox.visible = true
				' PAGE QOUTE
				case 2
					' set active scene
					MediaLayerControl.ItemIndex = 4
					
					' show text panel
					MagazinePlateTextPanel.visible = true
					MagazinePlateNumberPagesChanged(Sender)
					
					' set top pixels for text panel
					MagazinePlateTextPanel.Top = 5
					
					CopyText "MagazinePlate"
					
					magazineImageOptionPanel.visible = true
					MagazineImageTypeChanged(Sender)
				' PAGE FLIP
				case 3
					
					' set active scene
					MediaLayerControl.ItemIndex = 3
					
					
			end select
	end select
	
	ImageAsTitleCheckboxClick(Sender)
End Sub


' TRIGGERED WHEN THE NUMBER OF PAGES ON THE BOOK BODY PANE HAS BEEN MODIFIED
Sub BookBodyNumberPagesChanged(Sender)
	NumLinesOMO.Text = BookBodyNumberPages.Text - 1
	
	SetVisibility "BookBody", true, 1, CInt(BookBodyNumberPages.Text)
	SetVisibility "BookBody", false, CInt(BookBodyNumberPages.Text) + 1, 5
	
	ClearTexts "BookBody", CInt(BookBodyNumberPages.Text) + 1, 5
	
	CopyText "BookBody"
End sub

' TRIGGERED WHEN THE NUMBER OF PAGES ON THE BOOK PLATE PANE HAS BEEN MODIFIED
Sub BookPlateNumberPagesChanged(Sender)
	NumLinesOMO.Text = BookPlateNumberPages.Text - 1
	
	SetVisibility "BookPlate", true, 1, CInt(BookPlateNumberPages.Text)
	SetVisibility "BookPlate", false, CInt(BookPlateNumberPages.Text) + 1, 5
	
	ClearTexts "BookPlate", CInt(BookPlateNumberPages.Text) + 1, 5
	
	CopyText "BookPlate"
End sub

' TRIGGERED WHEN THE NUMBER OF PAGES ON THE MAGAZINE PANE HAS BEEN MODIFIED
Sub MagazineBodyNumberPagesChanged(Sender)
	NumLinesOMO.Text = magazineBodyNumberPages.Text - 1
	
	SetVisibility "MagazineBody", true, 1, CInt(magazineBodyNumberPages.Text)
	SetVisibility "MagazineBody", false, CInt(magazineBodyNumberPages.Text) + 1, 5
	
	ClearTexts "MagazineBody", CInt(magazineBodyNumberPages.Text) + 1, 5
	
	CopyText "MagazineBody"
End sub

' TRIGGERED WHEN THE NUMBER OF PAGES ON THE MAGAZINE PLATE PANE HAS BEEN MODIFIED
Sub MagazinePlateNumberPagesChanged(Sender)
	NumLinesOMO.Text = magazinePlateNumberPages.Text - 1
	
	SetVisibility "MagazinePlate", true, 1, CInt(magazinePlateNumberPages.Text)
	SetVisibility "MagazinePlate", false, CInt(magazinePlateNumberPages.Text) + 1, 5
	
	ClearTexts "MagazinePlate", CInt(magazinePlateNumberPages.Text) + 1, 5
	
	CopyText "MagazinePlate"
End sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Text = ""
	next
End Sub

' COPIES TEXT FROM BODY OR TEXT PLATE FIELDS TO APPROPRIATE COMPONENTS
Sub CopyText(componentName)
	for i=1 to 5
		if InStr(componentName, "Body") > 0 then
			FindComponent("MainBodyText" & i).Text = FindComponent(componentName & i).Text
		else
			' plate text
			FindComponent("MainTextPlate" & i).Text =  FindComponent(componentName & i).Text
		end if
	next
End Sub

' TODO - COMPLETE THIS FUNCTION
Sub TextChanged(Sender)
	if InStr(Sender.Name, "Body") > 0 then
		FindComponent("MainBodyText" & Right(Sender.Name, 1)).Text = Sender.Text
	else
		FindComponent("MainTextPlate" & Right(Sender.Name, 1)).Text = Sender.Text
	end if
End Sub

' TRIGGERED WHEN THE TYPE OF IMAGE TO DISPLAY ON SCREEN IS CHANGED
Sub MagazineImageTypeChanged(Sender)
	if magazineOpenCheckbox.checked then
		MediaTypeOMO.value = 0
	else	
		MediaTypeOMO.value = 2
	end if
End sub

' TRIGGERED ON CHECK/UNCHECK OF IMAGE AS TITLE CHECKBOX
Sub ImageAsTitleCheckboxClick(Sender)
	if ImageAsTitleCheckbox.checked then
		TitleImage.visible = true
		TitleImage.enabled = true
		TITLE_IMAGE_OMO.value = 1
	else
		TitleImage.visible = false
		
		TITLE_IMAGE_OMO.value = 0
	end if
	
	if not ImageAsTitleCheckbox.visible then
		TITLE_IMAGE_OMO.value = 0
	end if
	
End sub

Sub TitleChanged(Sender)
	MainTitle.Text = Sender.Text
End Sub

Sub SubtitleChanged(Sender)
	MainSubtitle.Text = Sender.Text
End Sub

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
	Dim comp
	for i=start to finish
		comp = FindComponent(componentName & CStr(i))
		ConnectVTWEvent [_scripter], comp, eventType, eventName
	next
End Sub