' Desc: BIO Cards Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 120314T
' NOTE: DROPDOWNS AND TEXT COPIES'

Dim keyframeTimeValues

Sub InitForm
        'MainLayerControl.ActiveLayerId = "1124x"
        enableRevealsCheckboxClick(Sender)
        PositionChanged(Sender)
        SubtitleChanged(Sender)
        BuildKeyframeTimeDictionary()

        'DevTools()
        BGControl.ActiveLayerId = "2001x"

End Sub

Sub BuildKeyframeTimeDictionary()
        Set keyframeTimeValues = CreateObject("Scripting.Dictionary")

        ' key is <page_num>_<line_num>
        keyframeTimeValues.Add "1_3", "4.8|3.6|2.4|"
        keyframeTimeValues.Add "1_2", "4.0|2.4|"
        keyframeTimeValues.Add "1_1", "3.0|"

        keyframeTimeValues.Add "2_3", "11.0|9.8|8.6|"
        keyframeTimeValues.Add "2_2", "10.4|8.7|"
        keyframeTimeValues.Add "2_1", "8.8|"

        keyframeTimeValues.Add "3_3", "17.3|16.0|14.8|"
        keyframeTimeValues.Add "3_2", "16.6|15.0|"
        keyframeTimeValues.Add "3_1", "15.0|"

        keyframeTimeValues.Add "4_3", "23.6|22.2|21.0|"
        keyframeTimeValues.Add "4_2", "23.0|21.4|"
        keyframeTimeValues.Add "4_1", "21.6|"

        keyframeTimeValues.Add "5_3", "29.8|28.4|27.0|"
        keyframeTimeValues.Add "5_2", "29.0|27.4|"
        keyframeTimeValues.Add "5_1", "27.6|"
End Sub

Sub PageLineChange(Sender)
        ' loop through all number of line controls
        Dim currentPageValue

        timingControl.Text = ""
        for i=5 to 1 step -1
                currentPageValue = FindComponent("P" & i).Value
                if keyframeTimeValues.Exists(i & "_" & currentPageValue + 1) and ((i - (CInt(numberOfPagesControl.Value) + 1)) <= 0) and FindComponent("numberOfLinesDropdown" & i).ItemIndex <> 0 then
                        timingControl.Text = timingControl.Text & keyframeTimeValues.Item(i & "_" & currentPageValue + 1)
                end if
        next
End Sub

' ===================================================================================='
' ===================================================================================='
' ===================================================================================='

' TRIGGERED AFTER THE TEMPLATE HAS BEEN SAVED
Sub SC_AfterSave(savedId, savedOver)
        GlobalOMO.Value = 0
        'msgbox "GlobalOMO set to: " & GlobalOMO.value
End Sub

' TRIGGERED WHEN THE NUMBER OF HEADSHOTS HAS BEEN CHANGED
Sub numberOfLinesDropdownChanged(Sender)
        Dim tabIndex : tabIndex = CStr(MainTabs.TabIndex + 1)
        Dim indexMask : indexMask = FindComponent("numberOfLinesDropdown" & tabIndex).itemIndex - 1

        if indexMask > -1 then
                'NumberPollsControl.Value = indexMask
                'NumberLinesOMOControl.Value = indexMask
                FindComponent("P" & tabIndex).Value = FindComponent("numberOfLinesDropdown" & tabIndex).ItemIndex - 1

                SetVisibility "TextLinePanel" & tabIndex & "_", true, 1, CInt(FindComponent("numberOfLinesDropdown" & tabIndex).Text)
                SetVisibility "TextLinePanel" & tabIndex & "_", false, CInt(FindComponent("numberOfLinesDropdown" & tabIndex).Text) + 1, 4

                ' show/hide text inputs
                select case indexMask
                        case 0
                                SetVisibility "bulletText" & tabIndex & "_1_", true, 1, 3
                                SetVisibility "bulletText" & tabIndex & "_2_", true, 1, 3
                        case 1
                                SetVisibility "bulletText" & tabIndex & "_1_", true, 1, 2
                                SetVisibility "bulletText" & tabIndex & "_1_", false, 3, 3
                                ClearTexts "bulletText"  & tabIndex & "_1_", 3, 3

                                SetVisibility "bulletText" & tabIndex & "_2_", true, 1, 2
                                SetVisibility "bulletText" & tabIndex & "_2_", false, 3, 3
                                ClearTexts "bulletText"  & tabIndex & "_2_", 3, 3

                                SetVisibility "bulletText" & tabIndex & "_3_", true, 1, 2
                                SetVisibility "bulletText" & tabIndex & "_3_", false, 3, 3
                                ClearTexts "bulletText"  & tabIndex & "_3_", 3, 3

                        case 2
                                SetVisibility "bulletText" & tabIndex & "_1_", true, 1, 1
                                SetVisibility "bulletText" & tabIndex & "_1_", false, 2, 3
                                ClearTexts "bulletText"  & tabIndex & "_1_", 2, 3

                                SetVisibility "bulletText" & tabIndex & "_2_", true, 1, 1
                                SetVisibility "bulletText" & tabIndex & "_2_", false, 2, 3
                                ClearTexts "bulletText"  & tabIndex & "_2_", 2, 3

                                SetVisibility "bulletText" & tabIndex & "_3_", true, 1, 1
                                SetVisibility "bulletText" & tabIndex & "_3_", false, 2, 3
                                ClearTexts "bulletText"  & tabIndex & "_3_", 2, 3

                                SetVisibility "bulletText" & tabIndex & "_4_", true, 1, 1
                                SetVisibility "bulletText" & tabIndex & "_4_", false, 2, 3
                                ClearTexts "bulletText"  & tabIndex & "_4_", 2, 3
                end select

                SubtitleChanged(Sender)
                BulletTextChanged(Sender)
        else
                SetVisibility "TextLinePanel" & tabIndex & "_", false, 1, 4
        end if

        PageLineChange(Sender)
End sub

' TRIGGERED WHEN THE POSITIONING OF THE SCREEN ELEMENTS
Sub PositionChanged(Sender)
        'GlobalOMO.Value = 1
        OMOSizeControl.value = 1


        if sideSelect.checked then
                headshotImagePanel.visible = false
                titleTypeControl.Value = 0

                ChangeDefaultBackgroundPanel.visible = true

                if sideControl.ItemIndex = 0 then
                        'right'
                        FSPlateControl.ActiveLayerId = "2012x"
                else
                        ' left'
                        FSPlateControl.ActiveLayerId = "2011x"
                end if

                if sideImageRadioButton.checked then
                        BGImage.visible = false
                        headshotImagesPanel.visible = true
                else
                        BGImage.visible = true
                        headshotImagesPanel.visible = false
                end if
        else
                headshotImagePanel.visible = true

                ChangeDefaultBackgroundPanel.visible = false
                headshotImagesPanel.visible = false

                ' set plate position active layer id
                FSPlateControl.ActiveLayerId = "2014x"

                ' set side panels to out
                RightPlateControl.ActiveLayerId = "2240x"
                LeftPlateControl.ActiveLayerId = "2250x"

                FirstHeadshotPanel.visible = false
                SecondHeadshotPanel.visible = false

                titleTypeControl.Value = 1

                OMOSizeControl.value = 0

                BPControl.ActiveLayerId = "2220x"
        end if

        ImageChoiceChanged(Sender)

        SubtitleChanged(Sender)
        SetMaxCharacters()
End Sub

' TRIGGERED WHEN THE OVER LIVE VIDEO CHECKBOX HAS BEEN CHANGED
Sub OverLiveVideoCheckboxClicked(Sender)
        if overLiveVideoCheckbox.checked then
                ImageChoicePanel.visible = false
                BGImage.visible = false

                BPControl.ActiveLayerId = "2220x"
                BGControl.ActiveLayerId = "2000x"

                RightPlateControl.ActiveLayerId = "2240x"
                LeftPlateControl.ActiveLayerId = "2250x"


        else
                BGControl.ActiveLayerId = "2001x"

                if not middleSelect.checked then
                        ImageChoicePanel.visible = true
                        ImageChoiceChanged(Sender)
                end if
        end if
End sub

Sub ImageChoiceChanged(Sender)
        if BGRadio.checked then
                if not middleSelect.checked then
                        firstHeadshotPanel.visible = false
                        secondHeadshotPanel.visible = false
                        BGImage.visible = true

                        ' set bp active layer id to IN
                        BPControl.ActiveLayerId = "2221x"
                        ' set bg active layer id to IN
                        BGControl.ActiveLayerId = "2001x"
                end if
        else
                if not middleSelect.checked then
                        BPControl.ActiveLayerId = "2220x"

                        select case HeadshotCountDropdown.ItemIndex
                                case "0"
                                        firstHeadshotPanel.visible = false
                                        secondHeadshotPanel.visible = false
                                        rightOmoChoice.value = 0
                                        leftOmoChoice.value = 0

                                case "1"
                                        firstHeadshotPanel.visible = true
                                        headshotImage1.visible = true

                                        secondHeadshotPanel.visible = false
                                        rightOmoChoice.value = 0
                                        leftOmoChoice.value = 0
                                case "2"
                                        firstHeadshotPanel.visible = true
                                        headshotImage1.visible = true
                                        headshotImage2.visible = true
                                        secondHeadshotPanel.visible = true
                                        rightOmoChoice.value = 1
                                        leftOmoChoice.value = 1
                        end select

                        if sideControl.ItemIndex = 0 and HeadshotCountDropdown.ItemIndex <> 0 then
                                'right'
                                RightPlateControl.ActiveLayerId = "2240x"
                                LeftPlateControl.ActiveLayerId = "2251x"

                                rightSelect.checked = true

                                headshotImage1.ControlObjectName = "L_IMAGE1"
                                headshotImage2.ControlObjectName = "L_IMAGE2"

                        elseif sideControl.ItemIndex = 1 and HeadshotCountDropdown.ItemIndex <> 0 then
                                ' left'
                                RightPlateControl.ActiveLayerId = "2241x"
                                LeftPlateControl.ActiveLayerId = "2250x"

                                leftSelect.checked = true

                                headshotImage1.ControlObjectName = "R_IMAGE1"
                                headshotImage2.ControlObjectName = "R_IMAGE2"
                        else
                                RightPlateControl.ActiveLayerId = "2240x"
                                LeftPlateControl.ActiveLayerId = "2250x"
                        end if
                end if
        end if
End Sub

Sub ImageAsTitleCheckboxClicked(Sender)
        if imageAsTitleCheckbox.checked then
                ' set omo
                imageAsTitleControl.value = 1

                ' hide title panel
                TitlePanel.visible = false

                ' show image title panel
                TitleImagePanel.visible = true
        else
                ' set omo
                imageAsTitleControl.value = 0

                ' show title panel
                TitlePanel.visible = true

                ' hide image title panel
                TitleImagePanel.visible = false
        end if
End sub

' TRIGGERED WHEN THE MAIN IMAGE HAS BEEN CHANGED
Sub MainImageChanged(Sender)
        hiddenImage.PicFilename = BioCardImage.PicFileName
End Sub

' TRIGGERED ON CLICK OF LAUNCH PBT
Sub PTBlauncherClick(Sender)
        set wshShell = CreateObject("WSCript.shell")
        wshshell.run "PTPlugin.exe"
        set wshshell = nothing
End sub

' TRIGGERED WHEN A SUBTITLE HAS BEEN MODIFIED
Sub SubtitleChanged(Sender)
        Dim subtitleCount : subtitleCount = 0

        ' check which subtitles are 'enabled'
        if subtitle1.Text <> "" then
                subtitleCount = subtitleCount + 1
        end if

        if subtitle2.Text <> "" then
                subtitleCount = subtitleCount + 1
        end if

        ShiftPositioning subtitleCount
End Sub

' SHIFTS Y POSITION OF MAIN ELEMENTS
Sub ShiftPositioning(subtitleCount)
        Dim subtitleValue : subtitleValue = 0

        ' complete logic
        select case subtitleCount
                'no subtitle
                case 0
                        if middleSelect.checked = true then
                        subtitleValue = 60
                        else
                        subtitleValue = 40
                        end if
                '1 subtitle
                case 1
                        if middleSelect.checked = true then
                        subtitleValue = 40
                        else
                        subtitleValue = 20
                        end if

                '2 subtitles
                case 2
                        if middleSelect.checked = true then
                        subtitleValue = 30
                        else
                        subtitleValue = 0
                        end if
        end select

        ShiftPosition.YValue = subtitleValue
End Sub

' TRIGGERED WHEN A BULLETED TEXT AREA IS CHANGED
Sub BulletTextChanged(Sender)

        CopyTexts()
End Sub

Sub CopyTexts()
        Dim line1, line2, line3, output
        for i = 1 to numberOfPagesDropdown.ItemIndex
                for j = 1 to 4
                        line1 = FindComponent("bulletText" & i & "_" & j & "_1").Text
                        line2 = FindComponent("bulletText" & i & "_" & j & "_2").Text
                        line3 = FindComponent("bulletText" & i & "_" & j & "_3").Text

                        'msgbox "bulletText" & i & "_" & j & "_1(2/3)"
                        'msgbox line1 & " - " & line2 & " - " & line3
                        if line1 <> "" then
                                output = line1
                                if line2 <> "" then
                                        output = output & vbNewLine & line2
                                        if line3 <> "" then
                                                output = output & vbNewLine & line3
                                        end if
                                end if

                                FindComponent("bulletTextControl" & i & "_" & j).Text = output
                        end if

                next
        next
End Sub

Sub SetMaxCharacters()

End Sub

Sub numberOfPagesDropdownChange(Sender)
        if numberOfPagesDropdown.ItemIndex > 0 then
                numberOfPagesControl.Value = numberOfPagesDropdown.ItemIndex - 1

                MainTabs.Visible = true
                enableRevealsCheckbox.Visible = true

                for i=1 to numberOfPagesDropdown.ItemIndex
                        FindComponent( "TabSheet" & i).TabVisible = true
                next
                for i=numberOfPagesDropdown.ItemIndex + 1 to 5
                        FindComponent( "TabSheet" & i).TabVisible = false
                next
        else
                MainTabs.Visible = false
                enableRevealsCheckbox.Visible = false
        end if


End sub

Sub enableRevealsCheckboxClick(Sender)
        if enableRevealsCheckbox.checked then
                MainLayerControl.ActiveLayerID = "1124xb"
                for i=1 to 5
                        FindComponent("Position" & i).YValue = 0
                next

        else
                MainLayerControl.ActiveLayerID = "1124x"

                for i=1 to 5
                        FindComponent("Position" & i).YValue = 30
                next
        end if
        'msgbox MainLayerControl.ActiveLayerID
End sub

Sub TITLE_CHOICE(Sender)
        if imageTitleRadio.checked then
            'titleOff.checked = true
            imageAsTitleControl.Value = 1
            titleText.visible = false
            titleText.Text = ""
            'titleTypeGroupBox.visible = false  (Commented out by Lindy)
            titleImage.visible = true
            'textOrImage.Text = "1"
            'TTripletEditor1.YValue = "0"
            titleTypeGroupBox.Caption = "Drag Masthead Into This Panel"
        else
            'if titleText.Text = "" then
                'titleOff.checked = false
            'else
                'titleOff.checked = true
            'end if
            titleText.visible = true
            'titleTypeGroupBox.visible = true
            titleImage.visible = false
            imageAsTitleControl.Value = 0
                        'CharsLeft(titleText)
            'TEXT_MODIFIED(Sender)
        end if
        PositionChanged(Sender)
End Sub

' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
        Dim comp
        for i=start to finish
                comp = FindComponent(componentName & CStr(i))
                'msgbox comp.name
                ConnectVTWEvent [_scripter], comp, eventType, eventName
        next
End Sub

' CHARACTER COUNT MONITOR
Sub CharsLeft(name, max, length)
        FindComponent(name & "_CharCount").Caption = "Characters Left: " & max - length
End Sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
        for i=start to finish
                'msgbox componentName & CStr(i)
                FindComponent(componentName & CStr(i)).visible = value
        next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
        for i=start to finish
                FindComponent(componentName & CStr(i)).Text = ""
        next
End Sub

Sub DevTools()
        Dim componentName, hintString

        ' Apply Events
        for i=1 to 5
                for j=1 to 4
                        componentName = "bulletText" & i & "_" & j & "_"
                        ApplyEvent componentName, "BulletTextChanged","OnClick", 1, 3
                        ApplyEvent componentName, "BulletTextChanged","OnChange", 1, 3
                        ApplyEvent componentName, "BulletTextChanged","OnEnter", 1, 3
                next
        next

        ' Apply Hints
        for i=1 to 5
                for j=1 to 4
                        for k=1 to 3
                                componentName = "bulletText" & i & "_" & j & "_" & k
                                FindComponent(componentName).Hint = "Page " & i & " Bullet " & j & " Line " & k
                        next
                next
        next
End Sub

