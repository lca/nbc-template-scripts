' Desc: Photo Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075


Dim debug : debug = true
debug = false

Dim ptbState : ptbState = false

Dim templateName : templateName = "FS_PHOTO"

sub InitForm
    MainSelComboBoxChange(Sender)
end sub

'Main Dropbox Selection
Sub MainSelComboBoxChange(Sender)
        ptbState = true

        if mainSelComboBox.Itemindex > 1 then
            courtesyLineEditText2.visible = false
            PTBButton.Picture = PTBRegularIcon.Picture
            SetPositionPanelEnabled(true)

        elseif mainSelComboBox.ItemIndex = 0 then
            PTBButton.Picture = PTBRestingIcon.Picture
            ptbState = false
            SetPositionPanelEnabled(false)

        else
            courtesyLineEditText2.visible = true
            PTBButton.Picture = PTBRegularIcon.Picture
            SetPositionPanelEnabled(true)
        end if

        verticalLayerControl.ActiveLayerID = "2280x"
        squareLayerControl.ActiveLayerID = "2290x"
        wideLayerControl.ActiveLayerID = "2300x"
        smartphoneLayerControl.ActiveLayerID = "2270x"
        mainLayerControl.ActiveLayerID = "2230x"


        if mainSelComboBox.Itemindex = 1 then
                ImageControl.ControlObjectName = "PHOTO_FULL"

                photoPlateLayerControl.ActiveLayerID = "2310x"
                mainLayerControl.ActiveLayerID = "2275x"

        elseif mainSelComboBox.Itemindex = 2 then


                ImageControl.ControlObjectName = "PHOTO_WIDE"
                wideLayerControl.ActiveLayerID = "2271x"
                photoPlateLayerControl.ActiveLayerID = "2299x"

        elseif mainSelComboBox.Itemindex = 3 then

                ImageControl.ControlObjectName = "PHOTO_SQUARE"
                squareLayerControl.ActiveLayerID = "2272x"
                photoPlateLayerControl.ActiveLayerID = "2289x"

        elseif mainSelComboBox.Itemindex = 4 then

                ImageControl.ControlObjectName = "PHOTO_VERTICAL"
                verticalLayerControl.ActiveLayerID = "2273x"
                photoPlateLayerControl.ActiveLayerID = "2279x"

        elseif mainSelComboBox.Itemindex = 5 then

                ImageControl.ControlObjectName = "PHOTO_SMARTPHONE"
                smartphoneLayerControl.ActiveLayerID = "2274x"
                photoPlateLayerControl.ActiveLayerID = "2269x"
        End if

        CourtesyChanged(courtesyLineEditText1)
End sub

Sub SetCourtesyPositioning(Sender)
    if topRadioButton.checked then
        tabPositionOmo.value = 1
    else
        tabPositionOmo.value = 0
    end if
End Sub


' CHARACTER COUNT MONITOR
Sub CharsLeft(Sender)
    Dim componentNumber : componentNumber = Right(Sender.Name, 1)

    CourtesyBox.Caption = "Courtesy Text Line " & CStr(componentNumber) & " - Characters Left: " &  (Sender.MaxLength - 2) - Len(Sender.Text)

    if (Sender.MaxLength - 2) - Len(Sender.Text) < 0 then
        CourtesyBox.Font.Color = clRed
    else
        CourtesyBox.Font.Color = clWhite
    end if
End Sub

' Copies courtest text(s) to control objects
Sub CourtesyChanged(Sender)
    textControl1.Text = courtesyLineEditText1.Text

    if courtesyLineEditText2.Text <> "" and courtesyLineEditText2.visible then
        textControl1.Text = textControl1.Text & vbNewLine & courtesyLineEditText2.Text
    end if

    CharsLeft(Sender)
End Sub

Sub SetPositionPanelEnabled(enable)
        PositionPanel.enabled = enable

        if enable then
            positionPanelLabel.Font.Color = clBlack
            topRadioButton.Font.Color = clBlack
            bottomRadioButton.Font.Color = clBlack

        else
            positionPanelLabel.Font.Color = clInactiveCaption
            topRadioButton.Font.Color = clInactiveCaption
            bottomRadioButton.Font.Color = clInactiveCaption
        end if
End Sub

'======================================================================================================
'===================================== PTB CONTROLS ===================================================
'======================================================================================================

Sub PTBButtonClick(Sender)
    if ptbState then
        if not debug then
            set wshShell = CreateObject("WSCript.shell")
            wshshell.run "PTPlugin.exe"
            set wshshell = nothing
        end if

        PTBButton.Picture = PTBPressedIcon.Picture
        PTBTimer.Enabled = true
    end if

End Sub

Sub PTBButtonRelease(Sender)
    PTBButton.Picture = PTBRegularIcon.Picture
    ptbState = true
    PTBTimer.Enabled = false
End sub

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

'SAVE NAME CONVENTION:
'Template Name + Number of Pages + Title + Subtitle'

Function SC_SaveName
        Dim prefix : prefix = ""
        SC_SaveName = templateName

        SC_SaveName = SC_SaveName & " " & mainSelComboBox.Text & " " & courtesyLineEditText1.Text & " " & courtesyLineEditText2.Text

        nextValue = GetSaveCount()

' split old save name to get the generated # prefix
        oldSaveNameSplit = Split(SCV_OldSaveName, "_")

        if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
                SC_SaveName = oldSaveNameSplit(0) & "_" & oldSaveNameSplit(1) & "_" & SC_SaveName
                newTemplate = false
        else
' if the template is new then generate a new prefix
            nextValue = nextValue + 1

            if Len(nextValue) < 5 then
                select case Len(nextValue)
                    case 1
                        prefix = "0000"
                    case 2
                        prefix = "000"
                    case 3
                        prefix = "00"
                    case 4
                        prefix = "0"
                end select
            end if

            SC_SaveName = GetWeekday() & "_" & prefix & nextValue & "_" & SC_SaveName
        end if

' increments db sequence value
        'IncrementSequenceValue()
end function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "00001"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                newId = recordset("ID")
        end if

        GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "00001"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                newId = recordset("ID")
        end if

        IncrementSequenceValue = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
    Dim number : number = ""
    Dim prefix : prefix = "_"
    if Len(savedOver) = 0 or savedOver = True then
        'msgbox CStr(saveId) & " - OVERWRITE HIT"
    elseif savedOver = false then
        'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
        
        
        ' if not a new template then overwrite the recently saved element with a 
        if not newTemplate then
            oldSaveNameSplit = Split(SCV_OldSaveName, "_")
            oldSaveName = SCV_OldSaveName
            number = CStr(GetSaveCount() + 1)
            
            if Len(number) < 5 then
                select case Len(number)
                    case 1
                        prefix = "_0000"
                    case 2
                        prefix = "_000"
                    case 3
                        prefix = "_00"
                    case 4
                        prefix = "_0"
                end select
            end if

            newSaveName = GetWeekday() & prefix & number & "_" & templateName & " " & mainSelComboBox.Text & " " & courtesyLineEditText1.Text & " " & courtesyLineEditText2.Text
                        
            'msgbox "Save As New hit on a previously saved template, update id"
            RenameDatabaseEntry newSaveName, oldSaveName
        end if
        
        IncrementSequenceValue()
    end if

End Sub

Function RenameDatabaseEntry(newSaveName, oldSaveName)
    connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

    Set DBConnection = CreateObject("ADODB.Connection")
    DBConnection.Open connectStr
    
    ' get id of the most recently saved element that matches this description
    Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
    

    Set recordset = DBConnection.Execute(Query)

    if not recordset.EOF then
        newId = recordset("DNR")
        
        'msgbox "FOUND DNR: " & newId
        ' update that element
        Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
        
        msgbox "QUERY: " & Query
        
        DBConnection.Execute(Query)
    end if
End Function



' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
        Dim dayAbbv : dayAbbv = Array("N/A", "SU", "MO", "TU", "WE", "TH", "FR", "SA")
        GetWeekday = dayAbbv(Weekday(Date))
End Function

Sub UpdateCases(Sender)
    ' list of text fields referenced by 'global' functions
    Dim textFields : textFields = Array("courtesyLineEditText1", "courtesyLineEditText1")

    Dim caseType : caseType = -1
    if upperCaseRadio.checked then
        caseType = 1
    elseif normalCaseRadio.checked then
        caseType = 0
    end if

    for i = 0 to UBound(textFields)
        FindComponent(textFields(i)).CharCase = caseType
        if caseType = -1 then
            FindComponent(textFields(i)).Text = LCase(FindComponent(textFields(i)).Text)
        end if
    next
End Sub