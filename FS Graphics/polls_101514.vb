' Desc: Polls Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075

Sub InitForm
        numLinesChange(Sender)
        headshotCheckboxClick(Sender)
        comparisonCheckboxClick(Sender)
        comparisonNumberChange(Sender)
End Sub

Function SC_SaveName
        if headshotCheckbox.checked = true then
                SC_SaveName = "FS_HS POLL: " & " (" & numLines.text & ") " & TXTTitleL1.text & " - " & subtitleText1.text
        else
                SC_SaveName = "FS_METER POLL: " & " (" & numLines.text & ") " & TXTTitleL1.text & " - " & subtitleText1.text
        end if

        nextValue = GetSaveCount()

        ' split old save name to get the generated # prefix
        oldSaveNameSplit = Split(SCV_OldSaveName, "_")

        if SCV_OldSaveName <> "" then
                ' if the template has already been saved once, use the old prefix
                SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
        else
                ' if the template is new then generate a new prefix
                SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
        end if

        ' increments db sequence value
        IncrementSequenceValue()
end function

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
        Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
        GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "10000"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                        newId = recordset("ID")
        end if

        GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
        connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

        newId = "10000"
        Set DBConnection = CreateObject("ADODB.Connection")
        DBConnection.Open connectStr

        newId = 0

        Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

        Set recordset = DBConnection.Execute(Query)

        if not recordset.EOF then
                        newId = recordset("ID")
        end if

        IncrementSequenceValue = CStr(newId)
End Function

dim LinesCount,TitleL1Count, TitleL2Count, SourceCount

TitleL1Count=20
TitleL2Count=20
SourceCount=20
LinesCount=20

sub InitForm

  PositionRadioClick(Sender)
         'UpdateValues(Sender)
end sub

'==== Initial form set up ====
'**** Select the number of bars to be displayed ****
Sub DRPNumBarsChange(Sender)
    if (DRPNumBarsShort.visible = true)then
        DRPNumBars.itemindex = DRPNumBarsShort.itemindex
    else
        DRPNumBarsShort.itemindex = DRPNumBars.itemindex
    end if
    dim DrpValue
    DrpValue = DRPNumBars.UTF8Text
    Select Case DrpValue
        Case 1
             OMONumLines.Value = 1
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = FALSE
             PNLInfoL3.Visible = FALSE
             PNLInfoL4.Visible = FALSE
             PNLInfoL5.Visible = FALSE
             PNLInfoL6.Visible = FALSE
             call Show2ndLine()
        Case 2
             OMONumLines.Value = 2
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = TRUE
             PNLInfoL3.Visible = FALSE
             PNLInfoL4.Visible = FALSE
             PNLInfoL5.Visible = FALSE
             PNLInfoL6.Visible = FALSE
             call Show2ndLine()
        Case 3
             OMONumLines.Value = 3
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = TRUE
             PNLInfoL3.Visible = TRUE
             PNLInfoL4.Visible = FALSE
             PNLInfoL5.Visible = FALSE
             PNLInfoL6.Visible = FALSE
             call Show2ndLine()
        Case 4
             OMONumLines.Value = 4
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = TRUE
             PNLInfoL3.Visible = TRUE
             PNLInfoL4.Visible = TRUE
             PNLInfoL5.Visible = FALSE
             PNLInfoL6.Visible = FALSE
             call Show2ndLine()
        Case 5
             OMONumLines.Value = 5
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = TRUE
             PNLInfoL3.Visible = TRUE
             PNLInfoL4.Visible = TRUE
             PNLInfoL5.Visible = TRUE
             PNLInfoL6.Visible = FALSE
             call Hide2ndLine()
        Case 6
             OMONumLines.Value = 6
             PNLInfoL1.Visible = TRUE
             PNLInfoL2.Visible = TRUE
             PNLInfoL3.Visible = TRUE
             PNLInfoL4.Visible = TRUE
             PNLInfoL5.Visible = TRUE
             PNLInfoL6.Visible = TRUE
             call Hide2ndLine()
    End Select
End sub

'**** Compare data on/off ****
Sub TWUniCheckBox1Click(Sender)
    if TWUniCheckBox1.Checked = TRUE then
       OMONumComp.Visible = TRUE
    else
       OMONumComp.Visible = FALSE
       OMONumComp.Value = 1
    end if
End sub


'==== Top Panel / Template Setup ====
'**** Select whether the title is Text or an Image ****
Sub CHKTitleClick(Sender)
    if CHKTitle.Checked = TRUE then
       titlePanel.Visible = FALSE
       IMGTitle.Visible =  TRUE
'       IMGTitle.Top = 54
       TXTTitleL1.UTF8Text = ""
       TXTTitleL2.UTF8Text = ""
       OMOTitle.Value = 1
       POSY.YValue = 30
    else
       titlePanel.Visible = TRUE
       IMGTitle.Visible =  FALSE
'       IMGTitle.Top = 300
       OMOTitle.Value = 0
       POSY.YValue = 15
    end if
End sub


'Plate Placement Choice
Sub layoutDropChange(Sender)
if layoutDrop.itemindex = 0 then
OMOUseTitle.text = "0"
DRPNumBarsShort.visible = true
DRPNumBars.visible = false
DRPNumBarsShort.itemindex = 0
DRPNumBars.itemindex = 0
DRPNumBarsChange(Sender)

Panel15.visible = true
TVTWLayerControl5.activeLayerID="1012x"
BBCChoice.visible = true
Panel12.visible=true
TVTWLayerControl4.ActiveLayerID = "2250x"
lPanel.visible = false
TVTWLayerControl1.activeLayerID = "2011x"
        if BBCardChoice.checked = true then
                TVTWLayerControl3.ActiveLayerID = "2241x"
                rPanel.visible = true
                TTWImageInf5.Enabled = true
                TTWImageInf6.Enabled = true
                TTWImageInf5.visible = true
                TTWImageInf6.visible = true
                lPanel.visible = false
                BKD_IMAGE_PANEL.visible=false
                TVTWLayerControl2.activelayerID = "2220x"
        else
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl3.ActiveLayerID = "2240x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
        end if

elseif layoutDrop.itemindex = 1 then
OMOUseTitle.text = "1"
DRPNumBarsShort.visible = false
DRPNumBars.visible = true
DRPNumBarsShort.itemindex = 0
DRPNumBars.itemindex = 0
DRPNumBarsChange(Sender)

TVTWLayerControl5.activeLayerID="1013x"
BBCChoice.visible = false
Panel15.visible = false
TVTWLayerControl4.ActiveLayerID = "2250x"
rPanel.visible = false
TVTWLayerControl1.activeLayerID = "2014x"
TVTWLayerControl3.ActiveLayerID = "2240x"
BKD_IMAGE_PANEL.visible=false
TVTWLayerControl2.activelayerID = "2220x"
Panel12.visible=false
lPanel.visible = false

elseif layoutDrop.itemindex = 2 then
OMOUseTitle.text = "2"
DRPNumBarsShort.visible = true
DRPNumBars.visible = false
DRPNumBarsShort.itemindex = 0
DRPNumBars.itemindex = 0
DRPNumBarsChange(Sender)

Panel15.visible = true
TVTWLayerControl5.activeLayerID="1011x"
TVTWLayerControl1.activeLayerID = "2013x"
BBCChoice.visible = false
singleBBC.checked = true
Panel12.visible=true
        if BBCardChoice.checked = true then
                TVTWLayerControl3.ActiveLayerID = "2241x"
                TVTWLayerControl4.ActiveLayerID = "2251x"
                BKD_IMAGE_PANEL.visible=false
                rPanel.visible = true
                lPanel.visible = true
                TTWImageInf3.Enabled = true
                TTWImageInf4.Enabled = true
                TTWImageInf3.visible = true
                TTWImageInf4.visible = true
                TTWImageInf5.Enabled = true
                TTWImageInf6.Enabled = true
                TTWImageInf5.visible = true
                TTWImageInf6.visible = true
                TVTWLayerControl2.activelayerID = "2220x"
        else
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl4.ActiveLayerID = "2250x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
                TVTWLayerControl3.ActiveLayerID = "2240x"
        end if
elseif layoutDrop.itemindex = 3 then
OMOUseTitle.text = "0"
DRPNumBarsShort.visible = true
DRPNumBars.visible = false
DRPNumBarsShort.itemindex = 0
DRPNumBars.itemindex = 0
DRPNumBarsChange(Sender)

Panel15.visible = true
TVTWLayerControl5.activeLayerID="1012x"
BBCChoice.visible = true
Panel12.visible=true
TVTWLayerControl3.ActiveLayerID = "2240x"
rPanel.visible = false
TVTWLayerControl1.activeLayerID = "2012x"
        if BBCardChoice.checked = true then
                TVTWLayerControl4.ActiveLayerID = "2251x"
                rPanel.visible = false
                lPanel.visible = true
                TTWImageInf3.Enabled = true
                TTWImageInf4.Enabled = true
                TTWImageInf3.visible = true
                TTWImageInf4.visible = true
                BKD_IMAGE_PANEL.visible=false
                TVTWLayerControl2.activelayerID = "2220x"
        else
                rPanel.visible = false
                lPanel.visible = false
                TVTWLayerControl4.ActiveLayerID = "2250x"
                BKD_IMAGE_PANEL.visible=true
                TVTWLayerControl2.activelayerID = "2221x"
        end if
end if
TWUniRadioButton1Click(Sender)
End sub

Sub BKDChouceClick(Sender)
layoutDropChange(Sender)
if BBCardChoice.checked = true then
BBCChoice.visible = true
        if layoutDrop.itemindex = 2 then
          BBCChoice.visible = false
        end if
else
BBCChoice.visible = false
end if

End sub

Sub TWUniRadioButton1Click(Sender)
if singleBBC.checked = true then
DragEdit1.text = 0
TDragEdit2.text = 0
imageL2p.visible = false
imageR2p.visible = false
else
DragEdit1.text = 1
TDragEdit2.text = 1
imageL2p.visible = true
imageR2p.visible = true
end if
End sub






'**** Show the number of dates and comparison data ****
Sub OMONumCompChange(Sender)
    if OMONumComp.Value = 1 then
       TXTDate1.Visible = TRUE
       TXTDate2.Visible = FALSE
       TXTDate3.Visible = FALSE
       TXTDateLbl1.Visible = TRUE
       TXTDateLbl2.Visible = FALSE
       TXTDateLbl3.Visible = FALSE
       TXT_L1D2.Visible = FALSE
       TXT_L2D2.Visible = FALSE
       TXT_L3D2.Visible = FALSE
       TXT_L4D2.Visible = FALSE
       TXT_L5D2.Visible = FALSE
       TXT_L6D2.Visible = FALSE
       TXT_L1D3.Visible = FALSE
       TXT_L2D3.Visible = FALSE
       TXT_L3D3.Visible = FALSE
       TXT_L4D3.Visible = FALSE
       TXT_L5D3.Visible = FALSE
       TXT_L6D3.Visible = FALSE
       call Hide2ndLine()
    elseif OMONumComp.Value = 2 then
       TXTDate1.Visible = TRUE
       TXTDate2.Visible = TRUE
       TXTDate3.Visible = FALSE
       TXTDateLbl1.Visible = TRUE
       TXTDateLbl2.Visible = TRUE
       TXTDateLbl3.Visible = FALSE
       TXT_L1D2.Visible = TRUE
       TXT_L1D2.Visible = TRUE
       TXT_L2D2.Visible = TRUE
       TXT_L3D2.Visible = TRUE
       TXT_L4D2.Visible = TRUE
       TXT_L5D2.Visible = TRUE
       TXT_L6D2.Visible = TRUE
       TXT_L1D3.Visible = FALSE
       TXT_L2D3.Visible = FALSE
       TXT_L3D3.Visible = FALSE
       TXT_L4D3.Visible = FALSE
       TXT_L5D3.Visible = FALSE
       TXT_L6D3.Visible = FALSE
       call Show2ndLine()
    elseif OMONumComp.Value = 3 then
       TXTDate1.Visible = TRUE
       TXTDate2.Visible = TRUE
       TXTDate3.Visible = TRUE
       TXTDateLbl1.Visible = TRUE
       TXTDateLbl2.Visible = TRUE
       TXTDateLbl3.Visible = TRUE
       TXT_L1D2.Visible = TRUE
       TXT_L1D2.Visible = TRUE
       TXT_L2D2.Visible = TRUE
       TXT_L3D2.Visible = TRUE
       TXT_L4D2.Visible = TRUE
       TXT_L5D2.Visible = TRUE
       TXT_L6D2.Visible = TRUE
       TXT_L1D3.Visible = TRUE
       TXT_L2D3.Visible = TRUE
       TXT_L3D3.Visible = TRUE
       TXT_L4D3.Visible = TRUE
       TXT_L5D3.Visible = TRUE
       TXT_L6D3.Visible = TRUE
    end if
End sub

'==== Other Template Functions ====
'**** Logic for using decimals on percentage box ****





'==== Character Count ====
'**** Title & Source Section ****
Sub titleText1Change(Sender)
    titleText1Label.Caption = "Line Poll Title - Characters Left: " &  CStr(30 - Len(titleText1.UTF8Text))
    TXTTitleL1.TExt = Sender.Text
        titleText2.Text = Sender.Text

    'TITLE_CHANGED(Sender)
End sub
Sub titleText2Change(Sender)
    titleText2Label.Caption = "Line Poll Title - Characters Left: " &  CStr(25 - Len(titleText2.UTF8Text))
    TXTTitleL1.TExt = Sender.Text
        titleText1.Text = Sender.Text
    ' TITLE_CHANGED(Sender)
End sub

Sub TXTSourceChange(Sender)

End sub


'**** Lines Character Count ****

Sub TITLE_CHANGED(Sender)
        'TXTTitleL1.TExt = Sender.Text
ENd Sub

Sub VALUE_CHANGED(Sender)
        subline = Right(Sender.name, 1)
        line = Left(Right(Sender.name, 3), 1)
                'msgbox line & " - " & subline
        FindComponent("TXT_L" & line & "D" & subline & "_dec").Text = Sender.Text

                SetDivisoryLineVisibility()
End Sub

Sub SetDivisoryLineVisibility()
        dim result : result = true
        dim outerLimit : outerLimit = CInt(numLines.Text)
        dim innerLimit : innerLimit = CInt(comparisonNumberDrop.Text)
        'msgbox CStr(outerLimit) & "_" & CStr(innerLimit)
        if comparisonCheckbox.checked = false then
                innerLimit = 1
        end if


        for i=1 to outerLimit
                for j=1 to innerLimit
                        if InStr(FindComponent("value" & CStr(i) & "_" & CStr(j)).text, ".") or CDbl(FindComponent("value" & CStr(i) & "_" & CStr(j)).text) >= 100 then
                                result = false
                        end if
                next
        next

        divisoryLine.Checked = result
End Sub

Sub SUBTITLE_CHANGED(Sender)
        subtitleTextArea.Text = subtitleText1.Text & vbCrLf & subtitleText2.Text
                SetYPositioning()
End Sub

'==== Show/Hide 2nd line of information ====
'**** Show 2nd line ****



Sub CHKHeadshotClick(Sender)
    dim temp
    if CHKHeadshot.Checked = TRUE then
       For i = 1 to 4
          temp = ("txt" & i & ".visible = FALSE")
          Sender
       Next
    else

    end if
End sub

'Sub CHKHeadshotClick(Sender)
'    if CHKHeadshot.Checked = TRUE then
'       IMGLine1.Visible = TRUE
'       IMGLine2.Visible = TRUE
'       IMGLine3.Visible = TRUE
'       IMGLine4.Visible = TRUE
'       IMGLine5.Visible = TRUE
'       IMGLine6.Visible = TRUE
'    else
'       IMGLine1.Visible = FALSE
'       IMGLine2.Visible = FALSE
'       IMGLine3.Visible = FALSE
'       IMGLine4.Visible = FALSE
'       IMGLine5.Visible = FALSE
'       IMGLine6.Visible = FALSE
'    end if
'End sub


Sub DEC_CHANGE(Sender)
dim txt
dim myAnswer
txt= Sender.text

myAnswer=InStr(txt,".")

if myAnswer= 0 then
        findcomponent((Sender.name) & "_dec").Text = "###"
   else
        findcomponent((Sender.name) & "_dec").Text="##.#"
End If
End sub

Sub pToolboxClick(Sender)
      set wshShell = CreateObject("WSCript.shell")
      wshshell.run "PTPlugin.exe"
      set wshshell = nothing
End sub








Sub ST_1Change(Sender)
TXTTitleL2.text = ST_1.text + vbNewLine + ST_2.text
End sub

Sub ST_2Change(Sender)
TXTTitleL2.text = ST_1.text + vbNewLine + ST_2.text
End sub


'-----------------------------------------
'----------------NBC----------------------
'-----------------------------------------


'line poll---------------------------------------------------------------------------------------------

Sub numLinesChange(Sender)
svAL =  numLines.UTF8Text
        LinePollSubLabel2.visible = true
                 OMOUseTitle.value = 0
                                 titleText1Label.visible = false
                                 titleText2Label.visible = true

                                                                 ' disable options for anything but CENTER
                         PicLeftRadio.enabled = true
                         PicRightRadio.enabled = true
                         PicWingsRadio.enabled = true
   Select Case svAL

                     Case "1"
             grpLine1.visible = true
             grpLine2.visible = false
             grpLine3.visible = false
             grpLine4.visible = false
             grpLine5.visible = false
             grpLine6.visible = false

             PollPos.visible = true


                      Case "2"
             grpLine1.visible = true
             grpLine2.visible = true
             grpLine3.visible = false
             grpLine4.visible = false
             grpLine5.visible = false
             grpLine6.visible = false


             PollPos.visible = true

                        Case "3"
             grpLine1.visible = true
             grpLine2.visible = true
             grpLine3.visible = true
             grpLine4.visible = false
             grpLine5.visible = false
             grpLine6.visible = false

             PollPos.visible = true

                        Case "4"
             grpLine1.visible = true
             grpLine2.visible = true
             grpLine3.visible = true
             grpLine4.visible = true
             grpLine5.visible = false
             grpLine6.visible = false

             PollPos.visible = true

                         'LinePollSubLabel2.visible = false
                         'subtitleText2.Text = ""

                        Case "5"
                        titleText1Label.visible = true
                        titleText2Label.visible = false
             grpLine1.visible = true
             grpLine2.visible = true
             grpLine3.visible = true
             grpLine4.visible = true
             grpLine5.visible = true
             grpLine6.visible = false

             'PollPos.visible = false
                         ' disable options for anything but CENTER
                         PicLeftRadio.enabled = false
                         PicRightRadio.enabled = false
                         PicWingsRadio.enabled = false
             CenterRadio.checked = true

                        ' LinePollSubLabel2.visible = false
                         'subtitleText2.Text = ""
                                                 OMOUseTitle.value = 1
                                                                                                 headshotCheckboxClick(Sender)

                        Case "6"
                        titleText1Label.visible = true
                        titleText2Label.visible = false
             grpLine1.visible = true
             grpLine2.visible = true
             grpLine3.visible = true
             grpLine4.visible = true
             grpLine5.visible = true
             grpLine6.visible = true

             'PollPos.visible = false
                         ' disable options for anything but CENTER
                         PicLeftRadio.enabled = false
                         PicRightRadio.enabled = false
                         PicWingsRadio.enabled = false
             CenterRadio.checked = true

                         'LinePollSubLabel2.visible = false
                         'subtitleText2.Text = ""
                                                 OMOUseTitle.value = 1
                                                                                                 headshotCheckboxClick(Sender)


End Select
        OMONumLines.Text = svAL
        IMAGE_TYPE(Sender)
                PositionRadioClick(Sender)
                SetYPositioning()

                headshotCheckboxClick(Sender)
                                ComparisonCheckboxClick(Sender)
                                if PicWingsRadio.checked and numLines.ItemIndex = 0 then
                Date1.visible = false
                Date2.visible = false
                Date3.visible = false

                SetCenterSmallPresets(Sender)
        end if
End Sub


Sub LinePollClick(Sender)
        if LinePoll.checked = true then
                PollsInfo.visible = true
                numLines.visible = true
                PollsComparison.visible = true
                PollsHeadshot.visible = true
                PollsPosition.visible = true
        else
                PollsInfo.visible = false
                numLines.visible = false
                PollsComparison.visible = false
                PollsHeadshot.visible = false
                PollsPosition.visible = true
        End if
End sub

Sub BaseballCardClick(Sender)
        if BaseballCard.checked = true then
                PollsInfo.visible = false
                numLines.visible = false
                PollsComparison.visible = false
                PollsHeadshot.visible = false
                PollsPosition.visible = true
        else
                PollsInfo.visible = false
                numLines.visible = false
                PollsComparison.visible = false
                PollsHeadshot.visible = false
                PollsPosition.visible = false
        End if
End sub









'Baseball Card------------------------------------------------------------------------------


Sub NumBBCardsComboChange(Sender)
svAL =  NumBBCardsCombo.UTF8Text
   Select Case svAL

                     Case "1"
             grpBBCardLabel1.visible = true
             grpBBCardLabel2.visible = false
             grpBBCardLabel3.visible = false
             grpBBCardLabel4.visible = false

             grpBBCardsPos.Visible = true
             BBCardsPicLeft.checked = true
             BBCardsPicRight.checked = false
             BBCardsCenter.checked = false
             BBCBGimage.visible = true

                      Case "2"
             grpBBCardLabel1.visible = true
             grpBBCardLabel2.visible = true
             grpBBCardLabel3.visible = false
             grpBBCardLabel4.visible = false

             grpBBCardsPos.Visible = true
             BBCardsPicLeft.checked = true
             BBCardsPicRight.checked = false
             BBCardsCenter.checked = false
             BBCBGimage.visible = true

                        Case "3"
             grpBBCardLabel1.visible = true
             grpBBCardLabel2.visible = true
             grpBBCardLabel3.visible = true
             grpBBCardLabel4.visible = false

             grpBBCardsPos.Visible = true
             BBCardsPicLeft.checked = true
             BBCardsPicRight.checked = false
             BBCardsCenter.checked = false
             BBCBGimage.visible = true

                        Case "4"
             grpBBCardLabel1.visible = true
             grpBBCardLabel2.visible = true
             grpBBCardLabel3.visible = true
             grpBBCardLabel4.visible = true

             grpBBCardsPos.Visible = false
             BBCardsPicLeft.checked = false
             BBCardsPicRight.checked = false
             BBCardsCenter.checked = true
             BBCBGimage.visible = false
End Select
End Sub

Sub BBCardsPicLeftClick(Sender)
    BBCardTitleLR.visible = true
    BBCardTitleCenter.visible = false
    BBCBGimage.visible = true
End sub

Sub BBCardsPicRightClick(Sender)
    BBCardTitleLR.visible = true
    BBCardTitleCenter.visible = false
    BBCBGimage.visible = true
End sub

Sub BBCardsCenterClick(Sender)
        BBCardTitleCenter.visible = true
        BBCardTitleLR.visible = false
        BBCBGimage.visible = false
end sub

Sub BBCardTitle25Change(Sender)
    BBCardTitleLR.Caption = "BBCard Title - Characters Left: " & BBCardTitle25.MaxLength - Len(BBCardTitle25.Text)

end Sub

Sub BBCardTitle30Change(Sender)
        BBCardTitleCenter.Caption = "BBCard Title - Characters Left: " & BBCardTitle30.MaxLength - Len(BBCardTitle30.Text)
End sub

Sub BBCardSubTxtChange(Sender)
         BBCardSubLabel.Caption = "Subtitle - Characters Left: " & BBCardSubTxt.MaxLength - Len(BBCardSubTxt.Text)
End sub

Sub BBCardSourceTxtChange(Sender)
         BBCardSourceLabel.Caption = "Source - Characters Left: " & BBCardSourceTxt.MaxLength - Len(BBCardSourceTxt.Text)

End sub

Sub SubtitleText1Changed(Sender)
        LinePollSubLabel1.Caption = "Subtitle 1 - Characters Left: " & 25 - Len(subtitleText1.Text)
        SUBTITLE_CHANGED(Sender)
End Sub

Sub SubtitleText2Changed(Sender)
        LinePollSubLabel2.Caption = "Subtitle 2 - Characters Left: " & 25 - Len(subtitleText2.Text)
        SUBTITLE_CHANGED(Sender)
End Sub



Sub chBBCImageAsTitleClick(Sender)
    if chBBCImageAsTitle.checked = true then
        BBCGetTitleImage.visible = true
        BBCardTitleLR.visible = false
        BBCardTitleCenter.visible = false

    else chBBCImageAsTitle.checked = false
        BBCGetTitleImage.visible = false
        BBCardTitleLR.visible = true
        BBCardTitleCenter.visible = true
End if
end sub


Sub chLinePollClick(Sender)
        if chLinePoll.checked = true then
        LinePollPanel.visible = true
        chBaseballCard.checked = false
        else

        LinePollPanel.visible = false
end if
End sub

Sub chBaseballCardClick(Sender)
             if chBaseballCard.checked = true then
        BaseballCardPanel.visible = true
        chLinePoll.checked = false
        else
        BaseballCardPanel.visible = false

End if
end sub






Sub headshotCheckboxClick(Sender)
        if liveVideoCheckbox.checked then
                headshotCheckbox.checked = false
        end if
        If headshotCheckbox.checked = true and liveVideoCheckbox.checked = false then

                                        Head2.Visible = true
                                        Head3.Visible = true
                                        Head4.Visible = true
                                        Head5.Visible = true
                                        Head6.Visible = true

                                SetYPositioning()
                                PositionRadioClick(Sender)

        else
                Head1.Visible = false
                Head2.Visible = false
                Head3.Visible = false
                Head4.Visible = false
                Head5.Visible = false
                Head6.Visible = false

                ' check num of lines
                'if CInt(numLines.Utf8Text) > 4 then
                        'numLines.ItemIndex = 3
                     '   NumLinesChange(Sender)
                'end if

                if comparisonNumberDrop.ItemIndex = 1 then
                        comparisonNumberDrop.ItemIndex = 0
                end if

                                SetYPositioning()
                                PositionRadioClick(Sender)


        end if

End sub

Sub comparisonCheckboxClick(Sender)
        If comparisonCheckbox.checked = true then
                LineNumber.visible = true
                comparisonNumberChange(Sender)

        else
                comparisonNumber.Text = "1"
                LineNumber.visible = false
                Date2.visible = false
                Column21.Visible = false
                Column22.Visible = false
                Column23.Visible = false
                Column24.Visible = false
                Column25.Visible = false
                Column26.Visible = false
                Date3.visible = false
                Column31.Visible = false
                Column32.Visible = false
                Column33.Visible = false
                Column34.Visible = false
                Column35.Visible = false
                Column36.Visible = false
        end if


End sub

Sub SetCenterSmallPresets(Sender)
        if PicWingsRadio.checked and numLines.ItemIndex = 0 then
                Date1.visible = false
                Date2.visible = false
                Date3.visible = false
        else
                'ComparisonCheckboxClick(Sender)
                if comparisonCheckbox.checked then
                        Date1.visible = true
                        if comparisonNumberDrop.Text = "2" then
                                Date2.visible = true
                        else
                                Date2.visible = true
                                Date3.visible = true
                        end if
                else
                        Date1.visible = true
                end if
        end if
End Sub

Sub comparisonNumberChange(Sender)
        svAL =  comparisonNumberDrop.Utf8Text
   Select Case svAL
                Case "2"
                        Date2.visible = true
                        Column21.Visible = true
                        Column22.Visible = true
                        Column23.Visible = true
                        Column24.Visible = true
                        Column25.Visible = true
                        Column26.Visible = true
                        Date3.visible = false
                        Column31.Visible = false
                        Column32.Visible = false
                        Column33.Visible = false
                        Column34.Visible = false
                        Column35.Visible = false
                        Column36.Visible = false
                Case "3"
                        Date2.visible = true
                        Column21.Visible = true
                        Column22.Visible = true
                        Column23.Visible = true
                        Column24.Visible = true
                        Column25.Visible = true
                        Column26.Visible = true


                        if not PicWingsRadio.checked then
                                Date3.visible = true
                                Column31.Visible = true
                                Column32.Visible = true
                                Column33.Visible = true
                                Column34.Visible = true
                                Column35.Visible = true
                                Column36.Visible = true
                        else

                                comparisonNumberDrop.ItemIndex = 0
                                comparisonNumberChange(Sender)
                        end if

                        headshotCheckbox.checked = true

        end select
        comparisonNumber.Text = svAL
        headshotCheckboxClick(Sender)
        SetDivisoryLineVisibility()

        SetCenterSmallPresets(Sender)

End sub


Sub NT1Change(Sender)
    NTLabel1.Caption = "Name - Characters Left: " & NT1.MaxLength - Len(NT1.Text)
End sub

Sub NT2Change(Sender)
    NTLabel2.Caption = "Name - Characters Left: " & NT2.MaxLength - Len(NT2.Text)
End sub

Sub NT3Change(Sender)
    NTLabel3.Caption = "Name - Characters Left: " & NT3.MaxLength - Len(NT3.Text)
End sub

Sub NT4Change(Sender)
    NTLabel4.Caption = "Name - Characters Left: " & NT4.MaxLength - Len(NT4.Text)
End sub

Sub NT5Change(Sender)
    NTLabel5.Caption = "Name - Characters Left: " & NT5.MaxLength - Len(NT5.Text)
End sub

Sub NT6Change(Sender)
    NTLabel6.Caption = "Name - Characters Left: " & NT6.MaxLength - Len(NT6.Text)
End sub

Sub PositionRadioClick(Sender)
                MAIN_BG.ActiveLayerId = "2001x"
        OMOUseTitle.value = 0
        imageNumberBox.visible = true
        rightHeadshotPanel1.Top = 10
PollsHeadshot.visible = true

        if PicLeftRadio.checked then

                TITLE_CHANGED(FindComponent("titleText2"))

                TVTWLayerControl4.ActiveLayerId = "2251x"
                TVTWLayerControl3.ActiveLayerId = "2240x"
                TVTWLayerControl1.ActiveLayerId = "2012x"


                                ' DO NOT SHOW HEADSHOT IF THERE IS ONLY ONE LINE
                                if numLines.ItemIndex = 0 then
                                        PollsHeadshot.visible = false
                                        headshotCheckbox.checked = false
                                else
                                        if headshotCheckbox.checked then
                        TVTWLayerControl5.ActiveLayerId = "1012x"
                                        else
                                                        TVTWLayerControl5.ActiveLayerId = "1015x"
                                        end if
                                end if



                imageTypeSelectionBox.visible = true


 NumImages(Sender)
        elseif CenterRadio.checked then

                OMOUseTitle.value = 1
                TITLE_CHANGED(FindComponent("titleText1"))

                if headshotCheckbox.checked then
                        TVTWLayerControl5.ActiveLayerId = "1013x"
                else
                        TVTWLayerControl5.ActiveLayerId = "1016x"
                end if

                TVTWLayerControl4.ActiveLayerId = "2250x"
                TVTWLayerControl3.ActiveLayerId = "2240x"
                TVTWLayerControl1.ActiveLayerId = "2014x"

                imageTypeSelectionBox.visible = false

                ' hide images option
                imageNumberBox.visible = false
 NumImages(Sender)
        elseif PicRightRadio.checked then

                TITLE_CHANGED(FindComponent("titleText2"))

                TVTWLayerControl4.ActiveLayerId = "2250x"
                TVTWLayerControl3.ActiveLayerId = "2241x"
                TVTWLayerControl1.ActiveLayerId = "2011x"

                ' DO NOT SHOW HEADSHOT IF THERE IS ONLY ONE LINE
                                if numLines.ItemIndex = 0 then
                                        PollsHeadshot.visible = false
                                        headshotCheckbox.checked = false
                                else
                                        if headshotCheckbox.checked then
                        TVTWLayerControl5.ActiveLayerId = "1012x"
                                        else
                                                        TVTWLayerControl5.ActiveLayerId = "1015x"
                                        end if
                                end if
                imageTypeSelectionBox.visible = true

 NumImages(Sender)
        else
                                ' CENTER SMALL
                                if numLines.ItemIndex = 0 then
                                        PollsHeadshot.visible = false
                                else

                                end if
                                headshotCheckbox.checked = false
                TITLE_CHANGED(FindComponent("titleText2"))

                TVTWLayerControl4.ActiveLayerId = "2251x"
                TVTWLayerControl3.ActiveLayerId = "2241x"
                TVTWLayerControl1.ActiveLayerId = "2013x"

                if headshotCheckbox.checked then
                        TVTWLayerControl5.ActiveLayerId = "1011x"
                else
                        TVTWLayerControl5.ActiveLayerId = "1014x"
                end if

                imageTypeSelectionBox.visible = false
                OMOUseTitle.Text = "2"
                imageNumberBox.visible = false
                rightHeadshotPanel2.visible=false
                                rightHeadshotPanel1.Top = 99

                                leftHeadshotPanel1.visible= true
                                rightHeadshotPanel1.visible=true

                                Date3.Visible = false
                                Column31.Visible = false
                                Column32.Visible = false
                                Column33.Visible = false
                                Column34.Visible = false
                                Column35.Visible = false
                                Column36.Visible = false

                        if comparisonNumberDrop.ItemIndex = 1 then
                                comparisonNumberDrop.ItemIndex = 0
                                comparisonNumberChange(Sender)
                        end if


        end if

                'ComparisonCheckboxClick(Sender)



                        if not headShotCheckbox.checked then
                                        Date3.Visible = false
                                        Column31.Visible = false
                                        Column32.Visible = false
                                        Column33.Visible = false
                                        Column34.Visible = false
                                        Column35.Visible = false
                                        Column36.Visible = false
                        end if

        'IMAGE_TYPE(Sender)1


                if not PicWingsRadio.checked and comparisonNumberDrop.Text = "3" and headshotCheckbox.checked then
                        Date3.Visible = true
                        Column31.Visible = true
                        Column32.Visible = true
                        Column33.Visible = true
                        Column34.Visible = true
                        Column35.Visible = true
                        Column36.Visible = true
                end if

                If headshotCheckbox.checked = true and liveVideoCheckbox.checked = false then
                        if numLines.Text <> 1 and CenterRadio.checked then
                                Head1.Visible = true
                        elseif numLines.Text = 1 and not CenterRadio.checked then
                                        Head1.Visible = false
                        else
                                Head1.Visible = true
                        end if
                end if
                                SetCenterSmallPresets(Sender)
End Sub

' by number of images, display correct image inputs
Sub NumImages(Sender)
        leftHeadshotPanel1.visible = false
        leftHeadshotPanel2.visible = false
        rightHeadshotPanel1.visible = false
        rightHeadshotPanel2.visible = false
        bgIMageBox.visible = false

        IMAGE_TYPE(Sender)


        leftHeadshotImage2.visible = true
        rightHeadshotImage2.visible = true

        if PicWingsRadio.checked then
                lomoChoice.Value = 0
                romoChoice.Value = 0
                bgImageBOx.visible = false

                leftHeadshotPanel1.visible = true
                rightHeadshotPanel1.visible = true
                leftHeadshotImage2.visible = false
                rightHeadshotImage2.visible = false

                rightHeadshotPanel1.Top = 97
                TWImageInf10.visible = true
        TWImageInf11.visible = true
                TVTWLayerControl2.ActiveLayerId = "2220x"

                TVTWLayerControl3.ActiveLayerID = "2241x"
                                TVTWLayerControl4.ActiveLayerID = "2251x"
        end if

        if CenterRadio.checked then
                        bgImageBOx.visible = false
                                rightHeadshotPanel1.visible = false
                                leftHeadshotPanel1.visible = false
                                rightHeadshotPanel2.visible = false
                                leftHeadshotPanel2.visible = false
                                TVTWLayerControl2.ActiveLayerId = "2220x"
                                                                TVTWLayerControl3.ActiveLayerID = "2240x"
                                                                TVTWLayerControl3.ActiveLayerID = "2250x"


        end if

End Sub

Sub IMAGE_TYPE(Sender)
        if bgImage.checked then
                ' show bg
                                bgImageBOx.visible = true
                                rightHeadshotPanel1.visible = false
                                leftHeadshotPanel1.visible = false
                                rightHeadshotPanel2.visible = false
                                leftHeadshotPanel2.visible = false

                TVTWLayerControl2.ActiveLayerId = "2221x"
                TVTWLayerControl3.ActiveLayerID = "2240x"
                                TVTWLayerControl4.ActiveLayerID = "2250x"

                TWImageInf1.visible = true
        else
                TVTWLayerControl2.ActiveLayerId = "2220x"
                                bgImageBOx.visible = false

                                if numImagesDrop.ItemIndex = 0 then
                                        if PicLeftRadio.checked then
                                                leftHeadshotPanel1.visible = true
                                                leftHeadshotPanel2.visible = false
                                                lomoChoice.Value = 0
                                                 TVTWLayerControl3.ActiveLayerID = "2240x"
                                                TVTWLayerControl4.ActiveLayerID = "2251x"
                                        else
                                                romoChoice.Value = 0
                                                rightHeadshotPanel1.visible = true
                                                rightHeadshotPanel2.visible = false
                                                TVTWLayerControl3.ActiveLayerID = "2241x"
                                                TVTWLayerControl4.ActiveLayerID = "2250x"
                                        end if
                                else
                                        if PicLeftRadio.checked then
                                                leftHeadshotPanel1.visible = true
                                                leftHeadshotPanel2.visible = true
                                                lomoChoice.Value = 1
                                                TVTWLayerControl3.ActiveLayerID = "2240x"
                                                TVTWLayerControl4.ActiveLayerID = "2251x"
                                        else
                                                romoChoice.Value = 1
                                                rightHeadshotPanel1.visible = true
                                                rightHeadshotPanel2.visible = true
                                                TVTWLayerControl3.ActiveLayerID = "2241x"
                                                TVTWLayerControl4.ActiveLayerID = "2250x"
                                        end if
                                end if
                TWImageInf10.visible = true
                TWImageInf11.visible = true
                                leftHeadshotImage2.visible = true
                                rightHeadshotImage2.visible = true
        end if
End sub

'first last name character count changes - --------- - - - -------------------------------------------------------

Sub First1Change(Sender)
    grpFirst1.Caption = "First Name Char Left: " & First1.MaxLength - Len(First1.Text)
End sub

Sub First2Change(Sender)
    grpFirst2.Caption = "First Name Char Left: " & First2.MaxLength - Len(First2.Text)
End sub

Sub First3Change(Sender)
    grpFirst3.Caption = "First Name Char Left: " & First3.MaxLength - Len(First3.Text)
End sub

Sub First4Change(Sender)
    grpFirst4.Caption = "First Name Char Left: " & First4.MaxLength - Len(First4.Text)
End sub

Sub First5Change(Sender)
    grpFirst5.Caption = "First Name Char Left: " & First5.MaxLength - Len(First5.Text)
End sub

Sub First6Change(Sender)
    grpFirst6.Caption = "First Name Char Left: " & First6.MaxLength - Len(First6.Text)
End sub

Sub Last1Change(Sender)
    grpLast1.Caption = "Last Name Char Left: " & Last1.MaxLength - Len(Last1.Text)
End sub

Sub Last2Change(Sender)
    grpLast2.Caption = "Last Name Char Left: " & Last2.MaxLength - Len(Last2.Text)
End sub

Sub Last3Change(Sender)
    grpLast3.Caption = "Last Name Char Left: " & Last3.MaxLength - Len(Last3.Text)
End sub

Sub Last4Change(Sender)
    grpLast4.Caption = "Last Name Char Left: " & Last4.MaxLength - Len(Last4.Text)
End sub

Sub Last5Change(Sender)
    grpLast5.Caption = "Last Name Char Left: " & Last5.MaxLength - Len(Last5.Text)
End sub

Sub Last6Change(Sender)
    grpLast6.Caption = "Last Name Char Left: " & Last6.MaxLength - Len(Last6.Text)
End sub

Sub Drop1Change(Sender)
    grpDrop1.Caption = "Dropline Characters Left: " & Drop1.MaxLength - Len(Drop1.Text)
End sub

Sub Drop2Change(Sender)
    grpDrop2.Caption = "Dropline Characters Left: " & Drop2.MaxLength - Len(Drop2.Text)
End sub

Sub Drop3Change(Sender)
    grpDrop3.Caption = "Dropline Characters Left: " & Drop3.MaxLength - Len(Drop3.Text)
End sub

Sub Drop4Change(Sender)
    grpDrop4.Caption = "Dropline Characters Left: " & Drop4.MaxLength - Len(Drop4.Text)
End sub

Sub Drop5Change(Sender)
    grpDrop5.Caption = "Dropline Characters Left: " & Drop5.MaxLength - Len(Drop5.Text)
End sub

Sub Drop6Change(Sender)
    grpDrop6.Caption = "Dropline Characters Left: " & Drop6.MaxLength - Len(Drop6.Text)
End sub

Sub name1Change(Sender)
    HeadNTLabel1.Caption = "Name - Characters Left: " & name1.MaxLength - Len(name1.Text)
End sub

Sub name2Change(Sender)
    HeadNTLabel2.Caption = "Name - Characters Left: " & name2.MaxLength - Len(name2.Text)
End sub

Sub name3Change(Sender)
    HeadNTLabel3.Caption = "Name - Characters Left: " & name3.MaxLength - Len(name3.Text)
End sub

Sub name4Change(Sender)
    HeadNTLabel4.Caption = "Name - Characters Left: " & name4.MaxLength - Len(name4.Text)
End sub

Sub name5Change(Sender)
    HeadNTLabel5.Caption = "Name - Characters Left: " & name5.MaxLength - Len(name5.Text)
End sub

Sub name6Change(Sender)
    HeadNTLabel6.Caption = "Name - Characters Left: " & name6.MaxLength - Len(name6.Text)
End sub


Sub DATE_CHANGED(Sender)
        index = Right(Sender.Name, 1)
        FindComponent("dateEdit" & index).Text = Sender.Text
End sub


Sub SetYPositioning()
        if subtitleText1.Text <> "" then
                if subtitleText2.Text <> "" then
                        ' second subtitle exists
                        POSY.YValue = 8
                        if headshotCheckbox.checked then
                                select case CInt(numLines.ItemIndex + 1)
                                        case 3
                                                POSY.YValue = 0
                                end select
                        end if

                else
                        ' no second subtitle
                        POSY.YValue = 8
                        if not headshotCheckbox.checked then
                                select case CInt(numLines.ItemIndex + 1)
                                        case 4
                                                POSY.YValue = 2
                                end select
                        end if
                end if
        else
                ' no subtitle
                if headshotCheckbox.checked then
                        POSY.YValue = 25
                                select case CInt(numLines.ItemIndex + 1)
                                        case 1
                                                POSY.YValue = 15
                                        case 2
                                                POSY.YValue = 15
                                end select
                else
                        POSY.YValue = 15
                        select case CInt(numLines.ItemIndex + 1)
                                case 3
                                        POSY.YValue = 25
                                case 4
                                        POSY.YValue = 25
                        end select
                end if
        end if
End Sub

Sub UpdateValues(Sender)
        for i=1 to 6
                for j=1 to 3
                        FindComponent("value" & i & "_" & j)
                next
        next
End Sub



Sub PTBlauncherClick(Sender)

      set wshShell = CreateObject("WSCript.shell")
      wshshell.run "PTPlugin.exe"
      set wshshell = nothing

End sub


Sub LIVE_VIDEO_CHECKED(Sender)
        if liveVideoCheckbox.checked then
                        headshotCheckbox.checked = false
                ' all out and invisible
                PollPos.visible = false

                TVTWLayerControl2.ActiveLayerId = "2220x"
                TVTWLayerControl4.ActiveLayerId = "2250x"
                TVTWLayerControl3.ActiveLayerId = "2240x"

                ' in - live
                MAIN_BG.ActiveLayerId = "2000x"

        else
                ' all in and visible
                PollPos.visible = true

                ' out - live
                MAIN_BG.ActiveLayerId = "2001x"



                IMAGE_TYPE(Sender)
                PositionRadioClick(Sender)

        end if
End sub

Sub sourceTextChange(Sender)
     LinePollSourceLabel.Caption = "Source - Characters Left: " & 20 - Len(sourceText.Text)
End sub