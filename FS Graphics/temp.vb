' Desc: Baseball Head Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 07092014T1130

'EDITS BY: LINDY ZEE - MSNBC September 23, 2014

' initialization
Sub initform
	BaseballCardsCountChange(Sender)
	BgImageCheckboxClick(Sender)
	BG_IMAGE_CHANGED(Sender)
	PLATE_POS(Sender)
	TITLE_BAR(Sender)
	
	overLiveVideoCheckbox.checked = false
	backgroundSelect.checked = true
	TWUniRadioButton2.checked = false
	TWUniRadioButton1.checked = false
	textTitleRadio.checked = true
	CharLeft.caption =  "Characters Left for Selected Baseball Card:"
	BaseballCardsCount.UTF8Text = "Choose Number of Heads"
	
end sub


Function SC_SaveName
	SC_SaveName = Int(Timer) & "_FS_BBCARDS (" & BaseballCardsCount.UTF8Text & ") "
	if imageTitleRadio.checked then
		SC_SaveName = SC_SaveName & "[TITLE IMAGE]"
	else
		SC_SaveName = SC_SaveName & titleText.Text
	end if
end function

' triggered when a textfield has been modified
Sub TEXT_MODIFIED(Sender)
	if titleText.text = "" and textTitleRadio.checked = true then
		titOmo.text = "0"
	end if
	
	if titleText.text <> "" and textTitleRadio.checked = true then
		titOmo.text = "1"
	end if
	
	
	
	
	FindComponent(Sender.Name & "Label").Caption = Sender.Hint &  " - Characters Left: " & Sender.MaxLength - Len(Sender.Text)
	
	if InStr(Sender.Name, "Title") then
		titleTextControl.Text = Sender.Text
	end if
	
	if titleText.Text = "" then
		titleOff.checked = false
	else
		titleOff.checked = true
	end if
End Sub

' =============================== EVENT TRIGGERS ======================================

' image checkbox event trigger
Sub BgImageCheckboxClick(Sender)
	if 1 then 'BgImageCheckbox.Checked = true then
		BP1_LayerDrop.ActiveLayerID = "2221x"
		BgImage.Visible = 1
	else
		BP1_LayerDrop.ActiveLayerID = "2220x"
		BgImage.Visible = 0
	end if
End sub

' backdrop image selection has been made
Sub BG_IMAGE_CHANGED(Sender)
	
	MAIN_BG.ActiveLayerID = "2000x"
	if leftSelect.checked then
		if backGroundSelect.checked then
			BP1_LayerDrop.ActiveLayerID = "2221x"
			LeftLayerDrop.ActiveLayerID = "2250x"
			RightLayerDrop.ActiveLayerID = "2240x"
			MAIN_BG.ActiveLayerID = "2001x"
			LeftImage.Visible = false
			RightImage.Visible = false
			BgImage.Visible = true
		elseif TWUniRadioButton2.checked then
			BP1_LayerDrop.ActiveLayerID = "2220x"
			LeftLayerDrop.ActiveLayerID = "2250x"
			RightLayerDrop.ActiveLayerID = "2241x"
			MAIN_BG.ActiveLayerID = "2001x"
			BgImage.Visible = false
			RightImage.Visible = true
			LeftImage.Visible = false
			
			LeftImage.picFilename = RightImage.picFilename
			LeftImage.thumbFilename = RightImage.thumbFilename
			
		else
			BP1_LayerDrop.ActiveLayerID = "2220x"
			LeftLayerDrop.ActiveLayerID = "2251x"
			RightLayerDrop.ActiveLayerID = "2240x"
			
			RightImage.visible = false
			LeftImage.visible = false
'BgImage.Visible = false
		end if
	elseif rightSelect.checked then
		if backgroundSelect.checked then
' copy thumb and filename to bg image object
			BP1_LayerDrop.ActiveLayerID = "2221x"
			LeftLayerDrop.ActiveLayerID = "2250x"
			RightLayerDrop.ActiveLayerID = "2240x"
			MAIN_BG.ActiveLayerID = "2001x"
			LeftImage.Visible = false
			RightImage.Visible = false
			BgImage.Visible = true
		elseif TWUniRadioButton2.checked then
' copy thumb and filename to left image object
			BP1_LayerDrop.ActiveLayerID = "2220x"
			RightLayerDrop.ActiveLayerID = "2240x"
			LeftLayerDrop.ActiveLayerID = "2251x"
			MAIN_BG.ActiveLayerID = "2001x"
			BgImage.Visible = false
			LeftImage.Visible = true
			RightImage.Visible = false
			
			RightImage.picFilename = LeftImage.picFilename
			RightImage.thumbFilename = LeftImage.thumbFilename
		else
			BP1_LayerDrop.ActiveLayerID = "2220x"
			LeftLayerDrop.ActiveLayerID = "2250x"
			RightLayerDrop.ActiveLayerID = "2241x"
			
			LeftImage.visible = false
			RightImage.Visible = false
'BgImage.Visible = false
		end if
	else
		LeftLayerDrop.ActiveLayerID = "2250x"
		RightLayerDrop.ActiveLayerID = "2240x"
		BP1_LayerDrop.ActiveLayerID = "2220x"
		LIVE_VIDEO_CHECKED(Sender)
	end if
End sub

Sub LIVE_VIDEO_CHECKED(Sender)
	if overLiveVideoCheckbox.checked then
		MAIN_BG.ActiveLayerID = "2000x"
'LeftImage.visible = false
'RightImage.Visible = false
'BgImage.Visible = false
	else
		MAIN_BG.ActiveLayerID = "2001x"
	end if
ENd Sub
' set title bar omo
Sub TITLE_BAR(Sender)
	if titleText.text = "" then
		titleOff.checked = false
		titOmo.text = "0"
	else
		titleOff.checked = true
		titOmo.text = "1"
	end if
End sub

Sub TITLE_CHOICE(Sender)
	
	if imageTitleRadio.checked then
		titleTextLabel.caption = "DRAG SSG MASTHEAD BELOW"
		titleOff.checked = true
		textOrImage.Value = 1
		titleText.visible = false
		Image1.visible = true
		TWImageInf1.visible = true
		textOrImage.Text = "1"
		titOmo.text = "1"
	else
		
		if titleText.text = ""  then
			titleOff.checked = false
		else
			titleOff.checked = true
		end if
		
		titleText.visible = true
		titleTextLabel.caption = titleText.Hint &  " - Characters Left: " & titleText.MaxLength - Len(titleText.Text)
		titleTextLabel.visible = true
		Image1.visible = false
		TWImageInf1.visible = false
		textOrImage.Text = "0"
		titOmo.text = "0"
	end if
End Sub

' Set current active layer id
Sub PLATE_POS(Sender)
	MAIN_BG.ActiveLayerId = "2001x"
	if leftSelect.checked = true then
		panel13.visible = false
		TVTWLayerControl1.ActiveLayerID ="2011x"
		titTypeOmo.text = "0"
		middleOrSideOmo.text = "0"
		if BaseballCardsCount.itemindex > 3 then
			BaseballCardsCount.itemindex = 3
		end if
		BackdropPanel.Visible = true
	elseif middleSelect.checked = true then
		panel13.visible = true
		TVTWLayerControl1.ActiveLayerID ="2014x"
		titTypeOmo.text = "1"
		middleOrSideOmo.text = "1"
		BackdropPanel.Visible = false
		MAIN_BG.ActiveLayerId = "2000x"
	elseif rightSelect.checked = true then
		panel13.visible = false
		TVTWLayerControl1.ActiveLayerID ="2012x"
		titTypeOmo.text = "0"
		middleOrSideOmo.text = "0"
		if BaseballCardsCount.itemindex > 3 then
			BaseballCardsCount.itemindex = 3
'cardChoiceReset()
		end if
		BackdropPanel.Visible = true
	end if
	BG_IMAGE_CHANGED(Sender)
End sub

' visible cards number changed event
Sub BaseballCardsCountChange(Sender)
	numberHSOmo.text = BaseballCardsCount.itemindex+1
	
' set panel visibilities
	if CInt(BaseballCardsCount.UTF8Text) > 4 then
		PositionPanel.Visible = 0
		leftSelect.Visible = 0
		middleSelect.Visible = 1
		rightSelect.Visible = 0
		middleSelect.checked = true
	else
		PositionPanel.Visible = 1
		leftSelect.Visible = 1
		middleSelect.Visible = 1
		rightSelect.Visible = 1
	end if
	
	
	If BaseballCardsCount.UTF8Text > 12 then
		BaseballCardsCount.UTF8Text = "12"
	end if
	
	select case BaseballCardsCount.UTF8Text
	case "1"
		SetObjectParameters "BaseballCardImage001", 235, 73, 200, 200
		SetObjectParameters "BaseballCardLineTextA001", 287, 287, "", 110
		SetObjectParameters "BaseballCardLineTextB001", 310, 287, "", 110
		SetObjectParameters "BaseballCardLineTextC001", 333, 287, "", 110
		SetObjectParameters "BaseballCardLineTextD001", 356, 287, "", 110
		
'hint'
		BaseballCardLineTextA001.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB001.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC001.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD001.hint = "Enter Dropline 2 Content Here"
'Instructions Panel
		SetObjectParameters "Panel12", 467, 17, 41, 475
		
	case "2"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 73, 150, 150
		SetObjectParameters "BaseballCardLineTextA001", 392, 95, "", 110
		SetObjectParameters "BaseballCardLineTextB001", 415, 95, "", 110
		SetObjectParameters "BaseballCardLineTextC001", 438, 95, "", 110
		SetObjectParameters "BaseballCardLineTextD001", 461, 95, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 260, 150, 150
		SetObjectParameters "BaseballCardLineTextA002", 392, 280, "", 110
		SetObjectParameters "BaseballCardLineTextB002", 415, 280, "", 110
		SetObjectParameters "BaseballCardLineTextC002", 438, 280, "", 110
		SetObjectParameters "BaseballCardLineTextD002", 461, 280, "", 110
		
'hint
		BaseballCardLineTextA002.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB002.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC002.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD002.hint = "Enter Dropline 2 Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
		
		
	case "3"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 8, 150, 150
		SetObjectParameters "BaseballCardLineTextA001", 392, 30, "", 110
		SetObjectParameters "BaseballCardLineTextB001", 415, 30, "", 110
		SetObjectParameters "BaseballCardLineTextC001", 438, 30, "", 110
		SetObjectParameters "BaseballCardLineTextD001", 461, 30, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 170, 150, 150
		SetObjectParameters "BaseballCardLineTextA002", 392, 190, "", 110
		SetObjectParameters "BaseballCardLineTextB002", 415, 190, "", 110
		SetObjectParameters "BaseballCardLineTextC002", 438, 190, "", 110
		SetObjectParameters "BaseballCardLineTextD002", 461, 190, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 332, 150, 150
		SetObjectParameters "BaseballCardLineTextA003", 392, 354, "", 110
		SetObjectParameters "BaseballCardLineTextB003", 415, 354, "", 110
		SetObjectParameters "BaseballCardLineTextC003", 438, 354, "", 110
		SetObjectParameters "BaseballCardLineTextD003", 461, 354, "", 110
		
'hint'
		BaseballCardLineTextA003.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB003.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC003.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD003.hint = "Enter Dropline 2 Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "4"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 27, 100, 100
		SetObjectParameters "BaseballCardLineTextA001", 240, 134, "", 110
		SetObjectParameters "BaseballCardLineTextB001", 262, 134, "", 110
		SetObjectParameters "BaseballCardLineTextC001", 284, 134, "", 110
		SetObjectParameters "BaseballCardLineTextD001", 306, 134, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 255, 100, 100
		SetObjectParameters "BaseballCardLineTextA002", 240, 362, "", 110
		SetObjectParameters "BaseballCardLineTextB002", 262, 362, "", 110
		SetObjectParameters "BaseballCardLineTextC002", 284, 362, "", 110
		SetObjectParameters "BaseballCardLineTextD002", 306, 362, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 360, 27, 100, 100
		SetObjectParameters "BaseballCardLineTextA003", 365, 134, "", 110
		SetObjectParameters "BaseballCardLineTextB003", 387, 134, "", 110
		SetObjectParameters "BaseballCardLineTextC003", 409, 134, "", 110
		SetObjectParameters "BaseballCardLineTextD003", 431, 134, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 360, 255, 100, 100
		SetObjectParameters "BaseballCardLineTextA004", 365, 362, "", 110
		SetObjectParameters "BaseballCardLineTextB004", 387, 362, "", 110
		SetObjectParameters "BaseballCardLineTextC004", 409, 362, "", 110
		SetObjectParameters "BaseballCardLineTextD004", 431, 362, "", 110
		
'hint'
		BaseballCardLineTextA004.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB004.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC004.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD004.hint = "Enter Dropline 2 Content Here"
		
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "5"
		
' Head 1
		SetObjectParameters "BaseballCardImage001", 238, 1, 80, 80
		SetObjectParameters "BaseballCardLineTextA001", 235, 84, "", 80
		SetObjectParameters "BaseballCardLineTextB001", 257, 84, "", 80
		SetObjectParameters "BaseballCardLineTextC001", 279, 84, "", 80
		SetObjectParameters "BaseballCardLineTextD001", 301, 84, "", 80
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 238, 166, 80, 80
		SetObjectParameters "BaseballCardLineTextA002", 235, 249, "", 80
		SetObjectParameters "BaseballCardLineTextB002", 257, 249, "", 80
		SetObjectParameters "BaseballCardLineTextC002", 279, 249, "", 80
		SetObjectParameters "BaseballCardLineTextD002", 301, 249, "", 80
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 238, 334, 80, 80
		SetObjectParameters "BaseballCardLineTextA003", 235, 417, "", 80
		SetObjectParameters "BaseballCardLineTextB003", 257, 417, "", 80
		SetObjectParameters "BaseballCardLineTextC003", 279, 417, "", 80
		SetObjectParameters "BaseballCardLineTextD003", 301, 417, "", 80
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 378, 88, 80, 80
		SetObjectParameters "BaseballCardLineTextA004", 375, 171, "", 80
		SetObjectParameters "BaseballCardLineTextB004", 397, 171, "", 80
		SetObjectParameters "BaseballCardLineTextC004", 419, 171, "", 80
		SetObjectParameters "BaseballCardLineTextD004", 441, 171, "", 80
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 378, 253, 80, 80
		SetObjectParameters "BaseballCardLineTextA005", 375, 338, "", 80
		SetObjectParameters "BaseballCardLineTextB005", 397, 338, "", 80
		SetObjectParameters "BaseballCardLineTextC005", 419, 338, "", 80
		SetObjectParameters "BaseballCardLineTextD005", 441, 338, "", 80
		
		
'hint
		BaseballCardLineTextA005.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB005.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC005.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD005.hint = "Enter Dropline 2 Content Here"
		
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "6"
' Head 1
		SetObjectParameters "BaseballCardImage001", 238, 1, 80, 80
		SetObjectParameters "BaseballCardLineTextA001", 235, 84, "", 80
		SetObjectParameters "BaseballCardLineTextB001", 257, 84, "", 80
		SetObjectParameters "BaseballCardLineTextC001", 279, 84, "", 80
		SetObjectParameters "BaseballCardLineTextD001", 301, 84, "", 80
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 238, 166, 80, 80
		SetObjectParameters "BaseballCardLineTextA002", 235, 249, "", 80
		SetObjectParameters "BaseballCardLineTextB002", 257, 249, "", 80
		SetObjectParameters "BaseballCardLineTextC002", 279, 249, "", 80
		SetObjectParameters "BaseballCardLineTextD002", 301, 249, "", 80
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 238, 334, 80, 80
		SetObjectParameters "BaseballCardLineTextA003", 235, 417, "", 80
		SetObjectParameters "BaseballCardLineTextB003", 257, 417, "", 80
		SetObjectParameters "BaseballCardLineTextC003", 279, 417, "", 80
		SetObjectParameters "BaseballCardLineTextD003", 301, 417, "", 80
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 363, 1, 80, 80
		SetObjectParameters "BaseballCardLineTextA004", 361, 84, "", 80
		SetObjectParameters "BaseballCardLineTextB004", 383, 84, "", 80
		SetObjectParameters "BaseballCardLineTextC004", 405, 84, "", 80
		SetObjectParameters "BaseballCardLineTextD004", 427, 84, "", 80
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 363, 166, 80, 80
		SetObjectParameters "BaseballCardLineTextA005", 361, 249, "", 80
		SetObjectParameters "BaseballCardLineTextB005", 383, 249, "", 80
		SetObjectParameters "BaseballCardLineTextC005", 405, 249, "", 80
		SetObjectParameters "BaseballCardLineTextD005", 427, 249, "", 80
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 363, 334, 80, 80
		SetObjectParameters "BaseballCardLineTextA006", 361, 417, "", 80
		SetObjectParameters "BaseballCardLineTextB006", 383, 417, "", 80
		SetObjectParameters "BaseballCardLineTextC006", 405, 417, "", 80
		SetObjectParameters "BaseballCardLineTextD006", 427, 417, "", 80
		
'hint'
		BaseballCardLineTextA006.hint = "Enter Name 1 Content Here"
		BaseballCardLineTextB006.hint = "Enter Name 2 Content Here"
		BaseballCardLineTextC006.hint = "Enter Dropline 1 Content Here"
		BaseballCardLineTextD006.hint = "Enter Dropline 2 Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "7"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 8, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 135, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 255, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 319, 375, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 360, 86, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 445, 69, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 360, 211, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 445, 196, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 360, 332, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 445, 316, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "8"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 8, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 135, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 255, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 319, 375, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 369, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 453, 8, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 369, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 453, 135, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 369, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 453, 255, "", 110
		
' Head 8
		SetObjectParameters "BaseballCardImage008", 369, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB008", 453, 375, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		BaseballCardLineTextB008.hint = "Enter Name Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 500, 17, 41, 475
		
	case "9"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 86, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 69, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 211, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 196, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 332, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 316, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 370, 86, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 453, 69, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 370, 211, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 453, 196, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 370, 332, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 453, 316, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 501, 86, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 584, 69, "", 110
		
' Head 8
		SetObjectParameters "BaseballCardImage008", 501, 211, 80, 80
		SetObjectParameters "BaseballCardLineTextB008", 584, 196, "", 110
		
' Head 9
		SetObjectParameters "BaseballCardImage009", 501, 332, 80, 80
		SetObjectParameters "BaseballCardLineTextB009", 584, 316, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		BaseballCardLineTextB008.hint = "Enter Name Content Here"
		BaseballCardLineTextB009.hint = "Enter Name Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 635, 17, 41, 475
		
	case "10"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 8, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 135, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 255, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 319, 375, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 453, 8, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 453, 135, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 453, 255, "", 110
		
' Head 8
		SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB008", 453, 375, "", 110
		
' Head 9
		SetObjectParameters "BaseballCardImage009", 501, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB009", 584, 135, "", 110
		
' Head 10
		SetObjectParameters "BaseballCardImage010", 501, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB010", 584, 255, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		BaseballCardLineTextB008.hint = "Enter Name Content Here"
		BaseballCardLineTextB009.hint = "Enter Name Content Here"
		BaseballCardLineTextB010.hint = "Enter Name Content Here"
		
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 635, 17, 41, 475
		
	case "11"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 8, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 135, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 255, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 319, 375, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 453, 8, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 453, 135, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 453, 255, "", 110
		
' Head 8
		SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB008", 453, 375, "", 110
		
' Head 9
		SetObjectParameters "BaseballCardImage009", 501, 86, 80, 80
		SetObjectParameters "BaseballCardLineTextB009", 584, 69, "", 110
		
' Head 10
		SetObjectParameters "BaseballCardImage010", 501, 211, 80, 80
		SetObjectParameters "BaseballCardLineTextB010", 584, 196, "", 110
		
' Head 11
		SetObjectParameters "BaseballCardImage011", 501, 332, 80, 80
		SetObjectParameters "BaseballCardLineTextB011", 584, 316, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		BaseballCardLineTextB008.hint = "Enter Name Content Here"
		BaseballCardLineTextB009.hint = "Enter Name Content Here"
		BaseballCardLineTextB010.hint = "Enter Name Content Here"
		BaseballCardLineTextB011.hint = "Enter Name Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 635, 17, 41, 475
		
	case "12"
' Head 1
		SetObjectParameters "BaseballCardImage001", 235, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB001", 319, 8, "", 110
		
' Head 2
		SetObjectParameters "BaseballCardImage002", 235, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB002", 319, 135, "", 110
		
' Head 3
		SetObjectParameters "BaseballCardImage003", 235, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB003", 319, 255, "", 110
		
' Head 4
		SetObjectParameters "BaseballCardImage004", 235, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB004", 319, 375, "", 110
		
' Head 5
		SetObjectParameters "BaseballCardImage005", 370, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB005", 453, 8, "", 110
		
' Head 6
		SetObjectParameters "BaseballCardImage006", 370, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB006", 453, 135, "", 110
		
' Head 7
		SetObjectParameters "BaseballCardImage007", 370, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB007", 453, 255, "", 110
		
' Head 8
		SetObjectParameters "BaseballCardImage008", 370, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB008", 453, 375, "", 110
		
' Head 9
		SetObjectParameters "BaseballCardImage009", 501, 25, 80, 80
		SetObjectParameters "BaseballCardLineTextB009", 584, 8, "", 110
		
' Head 10
		SetObjectParameters "BaseballCardImage010", 501, 150, 80, 80
		SetObjectParameters "BaseballCardLineTextB010", 584, 135, "", 110
		
' Head 11
		SetObjectParameters "BaseballCardImage011", 501, 270, 80, 80
		SetObjectParameters "BaseballCardLineTextB011", 584, 255, "", 110
		
' Head 12
		SetObjectParameters "BaseballCardImage012", 501, 390, 80, 80
		SetObjectParameters "BaseballCardLineTextB012", 584, 375, "", 110
		
'hint'
		BaseballCardLineTextB001.hint = "Enter Name Content Here"
		BaseballCardLineTextB002.hint = "Enter Name Content Here"
		BaseballCardLineTextB003.hint = "Enter Name Content Here"
		BaseballCardLineTextB004.hint = "Enter Name Content Here"
		BaseballCardLineTextB005.hint = "Enter Name Content Here"
		BaseballCardLineTextB006.hint = "Enter Name Content Here"
		BaseballCardLineTextB007.hint = "Enter Name Content Here"
		BaseballCardLineTextB008.hint = "Enter Name Content Here"
		BaseballCardLineTextB009.hint = "Enter Name Content Here"
		BaseballCardLineTextB010.hint = "Enter Name Content Here"
		BaseballCardLineTextB011.hint = "Enter Name Content Here"
		BaseballCardLineTextB012.hint = "Enter Name Content Here"
		
		
'Instructions Panel
		SetObjectParameters "Panel12", 635, 17, 41, 475
	end select
	
' set visibilities on cards
	SetCardVisibilities (CInt(BaseballCardsCount.UTF8Text) + 1), 12
End sub

' =============================== HELPER FUNCTIONS ========================================

' Makes visible any cards from 0->start, hides all cards from start->finish
Sub SetCardVisibilities(start, finish)
	prefix = ""
	visibility = 1
	objectName = ""
	for i=1 to finish
' if a single digit number prepend a "0"
		if i < 10 then
			prefix = "0"
		else
			prefix = ""
		end if
		
' if reached the start index then disable visibility
		if i < start then
			visibility = 1
'FindComponent("BaseballCardImage0" & prefix & i).ImageSources = 8
'FindComponent("BaseballCardImage0" & prefix & i).ImageSources = 12
		else
			visibility = 0
			
' hide elements by setting width to zero
' width
			objectName = "BaseballCardLineTextA0" & prefix & i
			SetObjectParameters objectName, "", "", "", 0
			
			objectName = "BaseballCardLineTextB0" & prefix & i
			SetObjectParameters objectName, "", "", "", 0
			
			objectName = "BaseballCardImage0" & prefix & i
			SetObjectParameters objectName, "", "", "", 0
			
'FindComponent("BaseballCardImage0" & prefix & i).ImageSources = 8
'FindComponent("BaseballCardImage0" & prefix & i).ImageSources = 12
			
			if i < 7 then
				objectName = "BaseballCardLineTextC0" & prefix & i
				SetObjectParameters objectName, "", "", "", 0
				
				objectName = "BaseballCardLineTextD0" & prefix & i
				SetObjectParameters objectName, "", "", "", 0
			end if
		end if
		
' image
		FindComponent("BaseballCardImage0" & prefix & i).Visible = visibility
' fname
		FindComponent("BaseballCardLineTextA0" & prefix & i).Visible = visibility
' lname
		FindComponent("BaseballCardLineTextB0" & prefix & i).Visible = visibility
		
' dropline
		if i < 7 then
			FindComponent("BaseballCardLineTextC0" & prefix & i).Visible = visibility
		end if
		
		if (CInt(BaseballCardsCount.UTF8Text)) >= 7 then
			objectName = "BaseballCardLineTextA0" & prefix & i
			SetObjectParameters objectName, "", "", "", 0
			
			if i < 7 then
				objectName = "BaseballCardLineTextC0" & prefix & i
				SetObjectParameters objectName, "", "", "", 0
				
				objectName = "BaseballCardLineTextD0" & prefix & i
				SetObjectParameters objectName, "", "", "", 0
			end if
		end if
	next
End Sub

' set specific object properties (top, left, height, width)
Sub SetObjectParameters(objectName, top, left, height, width)
'msgbox objectName
	if top <> "" then
		FindComponent(objectName).Top = top
	end if
	if left <> "" then
		FindComponent(objectName).Left = left
	end if
	if height <> "" then
		FindComponent(objectName).Height = height
	end if
	if width <> "" then
		if width = 0 then
			FindComponent(objectName).Visible = 0
'FindComponent(objectName).ImageSources.isFileOpen = 0
		else
			FindComponent(objectName).Width = width
			FindComponent(objectName).Visible = 1
'FindComponent(objectName).ImageSources.isFileOpen = 1
		end if
		
	end if
end sub


Sub PTBbuttonClick(Sender)
	
	set wshShell = CreateObject("WSCript.shell")
	wshshell.run "PTPlugin.exe"
	set wshshell = nothing
	
End sub




'HINTS!--------------------------------------------------------------------------------------
PTBbutton.hint = "Click to Launch Production Tool Box"

BaseballCardsCount.hint = "Choose number of FS Baseball Cards in this dropdown box"
textTitleRadio.hint = "Click and type text for the Title"
imageTitleRadio.hint = "Click and Order/Get SSG Masthead from Production Tool Box"
leftSelect.hint = "Click for content to appear on left side of screen. Option to choose a BG Image (right), BG Headshot (right), or video input"
middleSelect.hint = "Click Center for content to appear in the center of the screen.  Only BG option is video input"
rightSelect.hint = "Click for content to appear on the right side of the screen.  Option to Choose a BG Image (left), BG Headshot (left) or video input"

overLiveVideoCheckbox.hint = "Graphic defaults to show look background, Check and Content will be keyed over video"
TWUniRadioButton1.hint = "Graphic defaults to show look background, Check and Content will be keyed over video"
TWUniRadioButton2.hint = "Choose Headshot for BG"
backgroundSelect.hint = "Choose FS image for BG"

CharLeft.hint = "Characters Left Change Dynamically and show you the amount of characters left in whichever text field you are in."

TWImageInf1.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
RightImage.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
LeftImage.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BGimage.hint =  "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage001.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage002.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage003.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage004.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage005.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage006.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage007.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage008.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage007.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage008.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage009.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage010.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage011.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"
BaseballCardImage012.hint = "Click Launch PTB button on top right corner and drag image onto me, thanks!"





'----------------------------------------------------------------------------------------------

'--------------------------------------------CHARACTER COUNTS------------------------------------------------------------------------------
'Head 1 Characters'
Sub BaseballCardLineTextA001Change(Sender)
	
	
	CharLeft.Caption = "Head 1 Name 1 Characters Left:  " & BaseballCardLineTextA001.MaxLength - Len (BaseballCardLineTextA001.Text)
End sub

Sub BaseballCardLineTextA001Enter(Sender)
	CharLeft.Caption = "Head 1 Name 1 Characters Left:  " & BaseballCardLineTextA001.MaxLength - Len (BaseballCardLineTextA001.Text)
End sub

Sub BaseballCardLineTextB001Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 1 Name Characters Left:  " & BaseballCardLineTextB001.MaxLength - Len (BaseballCardLineTextB001.Text)
	else
		CharLeft.Caption = "Head 1 Name 2 Characters Left:  " & BaseballCardLineTextB001.MaxLength - Len (BaseballCardLineTextB001.Text)
	End if
end sub
Sub BaseballCardLineTextB001Enter(Sender)
	CharLeft.Caption = "Head 1 Name 2 Characters Left:  " & BaseballCardLineTextB001.MaxLength - Len (BaseballCardLineTextB001.Text)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 1 Name Characters Left:  " & BaseballCardLineTextB001.MaxLength - Len (BaseballCardLineTextB001.Text)
	else
		CharLeft.Caption = "Head 1 Name 2 Characters Left:  " & BaseballCardLineTextB001.MaxLength - Len (BaseballCardLineTextB001.Text)
	End if
End sub

Sub BaseballCardLineTextC001Change(Sender)
	CharLeft.Caption = "Head 1 Dropline 1 Characters Left:  " & BaseballCardLineTextC001.MaxLength - Len (BaseballCardLineTextC001.Text)
End sub
Sub BaseballCardLineTextC001Enter(Sender)
	CharLeft.Caption = "Head 1 Dropline 1 Characters Left:  " & BaseballCardLineTextC001.MaxLength - Len (BaseballCardLineTextC001.Text)
End sub

Sub BaseballCardLineTextD001Change(Sender)
	CharLeft.Caption = "Head 1 Dropline 2 Characters Left:  " & BaseballCardLineTextD001.MaxLength - Len (BaseballCardLineTextD001.Text)
End sub
Sub BaseballCardLineTextD001Enter(Sender)
	CharLeft.Caption = "Head 1 Dropline 2 Characters Left:  " & BaseballCardLineTextD001.MaxLength - Len (BaseballCardLineTextD001.Text)
End sub



'Head 2 Characters'
Sub BaseballCardLineTextA002Change(Sender)
	CharLeft.Caption = "Head 2 Name 1 Characters Left:  " & BaseballCardLineTextA002.MaxLength - Len (BaseballCardLineTextA002.Text)
End sub
Sub BaseballCardLineTextA002Enter(Sender)
	CharLeft.Caption = "Head 2 Name 1 Characters Left:  " & BaseballCardLineTextA002.MaxLength - Len (BaseballCardLineTextA002.Text)
End sub

Sub BaseballCardLineTextB002Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 2 Name Characters Left:  " & BaseballCardLineTextB002.MaxLength - Len (BaseballCardLineTextB002.Text)
	else
		CharLeft.Caption = "Head 2 Name 2 Characters Left:  " & BaseballCardLineTextB002.MaxLength - Len (BaseballCardLineTextB002.Text)
	End if
end sub
Sub BaseballCardLineTextB002Enter(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 2 Name Characters Left:  " & BaseballCardLineTextB002.MaxLength - Len (BaseballCardLineTextB002.Text)
	else
		CharLeft.Caption = "Head 2 Name 2 Characters Left:  " & BaseballCardLineTextB002.MaxLength - Len (BaseballCardLineTextB002.Text)
	End if
end sub

Sub BaseballCardLineTextC002Change(Sender)
	CharLeft.Caption = "Head 2 Dropline 1 Characters Left:  " & BaseballCardLineTextC002.MaxLength - Len (BaseballCardLineTextC002.Text)
End sub
Sub BaseballCardLineTextC002Enter(Sender)
	CharLeft.Caption = "Head 2 Dropline 1 Characters Left:  " & BaseballCardLineTextC002.MaxLength - Len (BaseballCardLineTextC002.Text)
End sub

Sub BaseballCardLineTextD002Change(Sender)
	CharLeft.Caption = "Head 2 Dropline 2 Characters Left:  " & BaseballCardLineTextD002.MaxLength - Len (BaseballCardLineTextD002.Text)
End sub
Sub BaseballCardLineTextD002Enter(Sender)
	CharLeft.Caption = "Head 2 Dropline 2 Characters Left:  " & BaseballCardLineTextD002.MaxLength - Len (BaseballCardLineTextD002.Text)
End sub

'Head 3 Characters'
Sub BaseballCardLineTextA003Change(Sender)
	CharLeft.Caption = "Head 3 Name 1 Characters Left:  " & BaseballCardLineTextA003.MaxLength - Len (BaseballCardLineTextA003.Text)
End sub
Sub BaseballCardLineTextA003Enter(Sender)
	CharLeft.Caption = "Head 3 Name 1 Characters Left:  " & BaseballCardLineTextA003.MaxLength - Len (BaseballCardLineTextA003.Text)
End sub

Sub BaseballCardLineTextB003Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 3 Name Characters Left:  " & BaseballCardLineTextB003.MaxLength - Len (BaseballCardLineTextB003.Text)
	else
		CharLeft.Caption = "Head 1 Name 2 Characters Left:  " & BaseballCardLineTextB003.MaxLength - Len (BaseballCardLineTextB003.Text)
	End if
End sub
Sub BaseballCardLineTextB003Enter(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 3 Name Characters Left:  " & BaseballCardLineTextB003.MaxLength - Len (BaseballCardLineTextB003.Text)
	else
		CharLeft.Caption = "Head 3 Name 2 Characters Left:  " & BaseballCardLineTextB003.MaxLength - Len (BaseballCardLineTextB003.Text)
	end if
End sub

Sub BaseballCardLineTextC003Change(Sender)
	CharLeft.Caption = "Head 3 Dropline 1 Characters Left:  " & BaseballCardLineTextC003.MaxLength - Len (BaseballCardLineTextC003.Text)
End sub
Sub BaseballCardLineTextC003Enter(Sender)
	CharLeft.Caption = "Head 3 Dropline 1 Characters Left:  " & BaseballCardLineTextC003.MaxLength - Len (BaseballCardLineTextC003.Text)
End sub

Sub BaseballCardLineTextD003Change(Sender)
	CharLeft.Caption = "Head 3 Dropline 2 Characters Left:  " & BaseballCardLineTextD003.MaxLength - Len (BaseballCardLineTextD003.Text)
End sub
Sub BaseballCardLineTextD003Enter(Sender)
	CharLeft.Caption = "Head 3 Dropline 2 Characters Left:  " & BaseballCardLineTextD003.MaxLength - Len (BaseballCardLineTextD003.Text)
End sub

'Head 4 Characters'
Sub BaseballCardLineTextA004Change(Sender)
	CharLeft.Caption = "Head 4 Name 1 Characters Left:  " & BaseballCardLineTextA004.MaxLength - Len (BaseballCardLineTextA004.Text)
End sub
Sub BaseballCardLineTextA004Enter(Sender)
	CharLeft.Caption = "Head 4 Name 1 Characters Left:  " & BaseballCardLineTextA004.MaxLength - Len (BaseballCardLineTextA004.Text)
End sub

Sub BaseballCardLineTextB004Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 4 Name Characters Left:  " & BaseballCardLineTextB004.MaxLength - Len (BaseballCardLineTextB004.Text)
	else
		CharLeft.Caption = "Head 4 Name 2 Characters Left:  " & BaseballCardLineTextB004.MaxLength - Len (BaseballCardLineTextB004.Text)
	end if
End sub
Sub BaseballCardLineTextB004Enter(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 4 Name Characters Left:  " & BaseballCardLineTextB004.MaxLength - Len (BaseballCardLineTextB004.Text)
	else
		CharLeft.Caption = "Head 4 Name 2 Characters Left:  " & BaseballCardLineTextB004.MaxLength - Len (BaseballCardLineTextB004.Text)
	end If
end sub
Sub BaseballCardLineTextC004Change(Sender)
	CharLeft.Caption = "Head 4 Dropline 1 Characters Left:  " & BaseballCardLineTextC004.MaxLength - Len (BaseballCardLineTextC004.Text)
End sub
Sub BaseballCardLineTextC004Enter(Sender)
	CharLeft.Caption = "Head 4 Dropline 1 Characters Left:  " & BaseballCardLineTextC004.MaxLength - Len (BaseballCardLineTextC004.Text)
End sub

Sub BaseballCardLineTextD004Change(Sender)
	CharLeft.Caption = "Head 4 Dropline 2 Characters Left:  " & BaseballCardLineTextD004.MaxLength - Len (BaseballCardLineTextD004.Text)
End sub
Sub BaseballCardLineTextD004Enter(Sender)
	CharLeft.Caption = "Head 4 Dropline 2 Characters Left:  " & BaseballCardLineTextD004.MaxLength - Len (BaseballCardLineTextD004.Text)
End sub

'Head 5 Characters'
Sub BaseballCardLineTextA005Change(Sender)
	CharLeft.Caption = "Head 5 Name 1 Characters Left:  " & BaseballCardLineTextA005.MaxLength - Len (BaseballCardLineTextA005.Text)
End sub
Sub BaseballCardLineTextA005Enter(Sender)
	CharLeft.Caption = "Head 5 Name 1 Characters Left:  " & BaseballCardLineTextA005.MaxLength - Len (BaseballCardLineTextA005.Text)
End sub

Sub BaseballCardLineTextB005Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 5 Name Characters Left:  " & BaseballCardLineTextB005.MaxLength - Len (BaseballCardLineTextB005.Text)
	else
		CharLeft.Caption = "Head 5 Name 2 Characters Left:  " & BaseballCardLineTextB005.MaxLength - Len (BaseballCardLineTextB005.Text)
	end If
end Sub

Sub BaseballCardLineTextB005Enter(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 5 Name Characters Left:  " & BaseballCardLineTextB005.MaxLength - Len (BaseballCardLineTextB005.Text)
	else
		CharLeft.Caption = "Head 5 Name 2 Characters Left:  " & BaseballCardLineTextB005.MaxLength - Len (BaseballCardLineTextB005.Text)
	end If
End sub

Sub BaseballCardLineTextC005Change(Sender)
	CharLeft.Caption = "Head 5 Dropline 1 Characters Left:  " & BaseballCardLineTextC005.MaxLength - Len (BaseballCardLineTextC005.Text)
End sub
Sub BaseballCardLineTextC005Enter(Sender)
	CharLeft.Caption = "Head 5 Dropline 1 Characters Left:  " & BaseballCardLineTextC005.MaxLength - Len (BaseballCardLineTextC005.Text)
End sub

Sub BaseballCardLineTextD005Change(Sender)
	CharLeft.Caption = "Head 5 Dropline 2 Characters Left:  " & BaseballCardLineTextD005.MaxLength - Len (BaseballCardLineTextD005.Text)
End sub
Sub BaseballCardLineTextD005Enter(Sender)
	CharLeft.Caption = "Head 5 Dropline 2 Characters Left:  " & BaseballCardLineTextD005.MaxLength - Len (BaseballCardLineTextD005.Text)
End sub

'Head 6 Characters'
Sub BaseballCardLineTextA006Change(Sender)
	CharLeft.Caption = "Head 6 Name 1 Characters Left:  " & BaseballCardLineTextA006.MaxLength - Len (BaseballCardLineTextA006.Text)
End sub
Sub BaseballCardLineTextA006Enter(Sender)
	CharLeft.Caption = "Head 6 Name 1 Characters Left:  " & BaseballCardLineTextA006.MaxLength - Len (BaseballCardLineTextA006.Text)
End sub
Sub BaseballCardLineTextB006Change(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 6 Name Characters Left:  " & BaseballCardLineTextB006.MaxLength - Len (BaseballCardLineTextB006.Text)
	else
		CharLeft.Caption = "Head 6 Name 2 Characters Left:  " & BaseballCardLineTextB006.MaxLength - Len (BaseballCardLineTextB006.Text)
	end If
end Sub

Sub BaseballCardLineTextB006Enter(Sender)
	if BaseballCardsCount.UTF8Text > 6 then
		CharLeft.Caption = "Head 6 Name Characters Left:  " & BaseballCardLineTextB006.MaxLength - Len (BaseballCardLineTextB006.Text)
	else
		CharLeft.Caption = "Head 6 Name 2 Characters Left:  " & BaseballCardLineTextB006.MaxLength - Len (BaseballCardLineTextB006.Text)
	end If
End sub

Sub BaseballCardLineTextC006Change(Sender)
	CharLeft.Caption = "Head 6 Dropline 1 Characters Left:  " & BaseballCardLineTextC006.MaxLength - Len (BaseballCardLineTextC006.Text)
End sub
Sub BaseballCardLineTextC006Enter(Sender)
	CharLeft.Caption = "Head 6 Dropline 1 Characters Left:  " & BaseballCardLineTextC006.MaxLength - Len (BaseballCardLineTextC006.Text)
End sub

Sub BaseballCardLineTextD006Change(Sender)
	CharLeft.Caption = "Head 6 Dropline 2 Characters Left:  " & BaseballCardLineTextD006.MaxLength - Len (BaseballCardLineTextD006.Text)
End sub
Sub BaseballCardLineTextD006Enter(Sender)
	CharLeft.Caption = "Head 6 Dropline 2 Characters Left:  " & BaseballCardLineTextD006.MaxLength - Len (BaseballCardLineTextD006.Text)
End sub



Sub BaseballCardLineTextB007Change(Sender)
	CharLeft.Caption = "Head 7 Name Characters Left:  " & BaseballCardLineTextB007.MaxLength - Len (BaseballCardLineTextB007.Text)
End sub

Sub BaseballCardLineTextB007Enter(Sender)
	CharLeft.Caption = "Head 7 Name Characters Left:  " & BaseballCardLineTextB007.MaxLength - Len (BaseballCardLineTextB007.Text)
End sub

Sub BaseballCardLineTextB008Change(Sender)
	CharLeft.Caption = "Head 8 Name Characters Left:  " & BaseballCardLineTextB008.MaxLength - Len (BaseballCardLineTextB008.Text)
End sub

Sub BaseballCardLineTextB008Enter(Sender)
	CharLeft.Caption = "Head 8 Name Characters Left:  " & BaseballCardLineTextB008.MaxLength - Len (BaseballCardLineTextB008.Text)
End sub

Sub BaseballCardLineTextB009Change(Sender)
	CharLeft.Caption = "Head 9 Name Characters Left:  " & BaseballCardLineTextB009.MaxLength - Len (BaseballCardLineTextB009.Text)
End sub

Sub BaseballCardLineTextB009Enter(Sender)
	CharLeft.Caption = "Head 9 Name Characters Left:  " & BaseballCardLineTextB009.MaxLength - Len (BaseballCardLineTextB009.Text)
End sub

Sub BaseballCardLineTextB010Change(Sender)
	CharLeft.Caption = "Head 10 Name Characters Left:  " & BaseballCardLineTextB010.MaxLength - Len (BaseballCardLineTextB010.Text)
End sub

Sub BaseballCardLineTextB010Enter(Sender)
	CharLeft.Caption = "Head 10 Name Characters Left:  " & BaseballCardLineTextB010.MaxLength - Len (BaseballCardLineTextB010.Text)
End sub

Sub BaseballCardLineTextB011Change(Sender)
	CharLeft.Caption = "Head 11 Name Characters Left:  " & BaseballCardLineTextB011.MaxLength - Len (BaseballCardLineTextB011.Text)
End sub

Sub BaseballCardLineTextB011Enter(Sender)
	CharLeft.Caption = "Head 11 Name Characters Left:  " & BaseballCardLineTextB011.MaxLength - Len (BaseballCardLineTextB011.Text)
End sub

Sub BaseballCardLineTextB012Change(Sender)
	CharLeft.Caption = "Head 12 Name Characters Left:  " & BaseballCardLineTextB012.MaxLength - Len (BaseballCardLineTextB012.Text)
End sub

Sub BaseballCardLineTextB012Enter(Sender)
	CharLeft.Caption = "Head 12 Name Characters Left:  " & BaseballCardLineTextB012.MaxLength - Len (BaseballCardLineTextB012.Text)
End sub

'A Exit'
Sub BaseballCardLineTextA0Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA001Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA002Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA003Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA004Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA005Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextA006Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

'B Exit'
Sub BaseballCardLineTextB0Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB001Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB002Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB003Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB004Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB005Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB006Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB007Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB008Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB009Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB010Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB011Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextB012Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub


'C Exit'
Sub BaseballCardLineTextC0Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC001Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC002Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC003Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC004Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC005Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextC006Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub


'D Exit'
Sub BaseballCardLineTextD0Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD001Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD002Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD003Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD004Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD005Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

Sub BaseballCardLineTextD006Exit(Sender)
	CharLeft.Caption = "Characters Left for Selected Baseball Card:"
End sub

