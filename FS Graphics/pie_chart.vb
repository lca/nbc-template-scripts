' Desc: Pie Chart Template Script
' Author: Luis M Calderon - Vizrt NYC | NBC Development Team
' Timestamp: 1411578075

Sub InitForm
	PiesPositionRadio(Sender)
	PieColorComboChange(Sender)
	BACKGROUND_CHECKBOX_EVENT(Sender)
	RevealChecked(Sender)
End Sub

Function SC_SaveName
	SC_SaveName = "FS_PIE CHART"
	
	if value <> "100" then
		msgbox("WARNING!!!!" & vbNewLine & vbNewLine & "THE PIE CHART MUST ADD UP TO 100. CURRENT VALUE = " & value)
	end if
	
	
	if imageOrTitleCheckbox.checked then
		SC_SaveName = SC_SaveName & "*** IMAGE TITLE ***"
	else
		if PiesCenter.checked then
			SC_SaveName = SC_SaveName & "***" & PieTitle30.Text & "***"
		else
			SC_SaveName = SC_SaveName & "***" & PieTitle25.Text & "***"
		end if
	end if
	
	SC_SaveName = SC_SaveName & " - " & PieSubtxt.Text & " : " & PieSourceTxt.Text & " - " & ChartLabelPieTxt.Text
	
	nextValue = GetSaveCount()
	
' split old save name to get the generated # prefix
	oldSaveNameSplit = Split(SCV_OldSaveName, "_")
	
	if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
		SC_SaveName = oldSaveNameSplit(0) & "_" & GetWeekday() & "_" & SC_SaveName
	else
' if the template is new then generate a new prefix
		SC_SaveName = (nextValue + 1) & "_" & GetWeekday() & "_" & SC_SaveName
	end if
	
' increments db sequence value
	'IncrementSequenceValue()
end function

'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"
	
	newId = "10000"
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	newId = 0
	
	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"
	
	Set recordset = DBConnection.Execute(Query)
	
	if not recordset.EOF then
		newId = recordset("ID")
	end if
	
	IncrementSequenceValue = CStr(newId)
End Function

' triggered when pie chart label radio buttons hit
Sub PIE_LABELS(Sender)
	if pieLabels.checked then
' set position to 55
		positionControl.XValue = 55
		
		pieLabelsControl.checked = true
		legendLabelsControl.checked = false
	else
' set position to 6
		positionControl.XValue = 6
		
		pieLabelsControl.checked = false
		legendLabelsControl.checked = true
		
	end if
End sub

' triggered when number of headshots changed
Sub HeadshotRadio(Sender)
	rightHeadshot1.visible = true
	rightHeadshot2.visible = true
	leftHeadshot1.visible = true
	leftHeadshot2.visible = true
	
	if singleRadio.checked then
		if PiesPicLeft.checked then
			rightHeadshots.visible = true
			leftHeadshots.visible = false
			rightPlateCountControl.Text = "0"
			rightHeadshot2.visible = false
		else
			rightHeadshots.visible = false
			leftHeadshots.visible = true
			leftPlateCountControl.Text = "0"
			leftHeadshot2.visible = false
		end if
	elseif doubleRadio.checked then
		if PiesPicLeft.checked then
			rightHeadshots.visible = true
			leftHeadshots.visible = false
			rightPlateCountControl.Text = "1"
			rightHeadshot2.visible = true
		else
			leftPlateCountControl.Text = "1"
			leftHeadshots.visible = true
			rightHeadshots.visible = false
			leftHeadshot2.visible = true
		end if
	else
		if PiesCenter.checked then
		else
			leftPlateControl.ActiveLayerId = "2250x"
			rightPlateControl.ActiveLayerId = "2240x"
		end if
		
	end if
	
	if PiesCenter.checked or noRadio.checked then
		rightHeadshots.visible = false
		leftHeadshots.visible = false
	end if
End Sub

' activate line highlights
Sub ActivateLineHighlight(Sender)
	index = Right(Sender.name, 1)
	if Sender.checked then
		FindComponent("DragEdit" & index + 7).color = clYellow
		FindComponent("highlightCheckbox" & index).checked = true
		if index < 4 then
			FindComponent("sideHighlight" & index).checked = true
		end if
	else
		FindComponent("DragEdit" & index + 7).color = clInactiveCaption
		FindComponent("highlightCheckbox" & index).checked = false
		if index < 4 then
			FindComponent("sideHighlight" & index).checked = false
		end if
	end if
End Sub

' set colors
Sub SetColors(Sender)
	index = Right(Sender.Name, 1)
	if PieColorCombo.itemIndex = 0 then
		FindComponent("materialColor" & CStr(index)).picFilename = FindComponent("defaultColor" & CStr(index)).picFilename
		FindComponent("materialColor" & CStr(index)).thumbFilename = FindComponent("defaultColor" & CStr(index)).picFilename
	else
		FindComponent("materialColor" & CStr(index)).picFilename = FindComponent("politicalColor" & CStr(index)).picFilename
		FindComponent("materialColor" & CStr(index)).thumbFilename = FindComponent("politicalColor" & CStr(index)).picFilename
	end if
End Sub

' when title changes
Sub TITLE_CHANGED(Sender)
	titleTextControl.Text = Sender.text
	PieTitleCenter.Caption = "Pie Title - Characters Left: " & PieTitle30.MaxLength - Len(PieTitle30.Text)
	
End Sub

' pie value changed
Sub PIE_VALUE_CHANGED(Sender)
	
	pieValues = ""
	totalValue = 0
	for i=1 to NumPiesCombo.ItemIndex + 1
		if (FindComponent("DragEdit" & i+7).Text <> "") then
			totalValue = totalValue + CInt(FindComponent("DragEdit" & i+7).Text)
			if i <> NumPiesCombo.ItemIndex + 1 then
				pieValues = pieValues & FindComponent("DragEdit" & i+7).Text & ","
			else
				pieValues = pieValues & FindComponent("DragEdit" & i+7).Text
			end if
		end if
	next
	
	
	
	pieValuesControl.text = pieValues
End Sub

Sub ClearLabels()
	for i = CInt(NumPiesCombo.UTF8Text) + 1 to 5
		FindComponent("LabelSlice" & i).Text = ""
		FindComponent("DragEdit" & i+7).Value = 0
	next
End Sub
'======================================================================================================
'===================================== NBC ORIGINAL ===================================================
'======================================================================================================

'pie chart-------------------------------------------------------------------------------------------------

Sub NumPiesComboChange(Sender)
	svAL =  NumPiesCombo.UTF8Text
	
	Select Case svAL
	Case "1"
		grpPieLabel1.visible = true
		grpPieLabel2.visible = false
		grpPieLabel3.visible = false
		grpPieLabel4.visible = false
		grpPieLabel5.visible = false
		grpPiePos.Visible = true
		
		if not revealRadio.checked then
			SetVisibility "extrudeRadio", true, 0, 1
			SetVisibility "extrudeRadio", false, 2, 5
		end if

		
	Case "2"
		grpPieLabel1.visible = true
		grpPieLabel2.visible = true
		grpPieLabel3.visible = false
		grpPieLabel4.visible = false
		grpPieLabel5.visible = false
		grpPiePos.Visible = true
		
		if not revealRadio.checked then
			SetVisibility "extrudeRadio", true, 0, 2
			SetVisibility "extrudeRadio", false, 3, 5
		end if
		
	Case "3"
		grpPieLabel1.visible = true
		grpPieLabel2.visible = true
		grpPieLabel3.visible = true
		grpPieLabel4.visible = false
		grpPieLabel5.visible = false
		
		grpPiePos.Visible = true
		if not revealRadio.checked then
			SetVisibility "extrudeRadio", true, 0, 3
			SetVisibility "extrudeRadio", false, 4, 5
		end if
		
		
	Case "4"
		backPlateControl.ActiveLayerId = "2220x"
		PiesCenter.checked = true
		grpPieLabel1.visible = true
		grpPieLabel2.visible = true
		grpPieLabel3.visible = true
		grpPieLabel4.visible = true
		grpPieLabel5.visible = false
		
		grpPiePos.Visible = false
		if not revealRadio.checked then
			SetVisibility "extrudeRadio", true, 0, 4
			SetVisibility "extrudeRadio", false, 5, 5
		end if
		
	Case "5"
		backPlateControl.ActiveLayerId = "2220x"
		PiesCenter.checked = true
		grpPieLabel1.visible = true
		grpPieLabel2.visible = true
		grpPieLabel3.visible = true
		grpPieLabel4.visible = true
		grpPieLabel5.visible = true
		
		grpPiePos.Visible = false
		if not revealRadio.checked then
			SetVisibility "extrudeRadio", true, 0, 5
		end if

	End Select
	ClearLabels()
	
	PIE_VALUE_CHANGED(Sender)
	PiesPositionRadio(Sender)
End Sub

Sub RevealChecked(Sender)
	if revealRadio.checked then
		SetVisibility "extrudeRadio", false, 0, 5
		SetChecked "highlightCheckbox", true, 1, 5
		SetChecked "sideHighlight", true, 1, 3
	else
		' highlight appropriate values
		SetVisibility "extrudeRadio", true, 0, 5
		for i=1 to 5
			FindComponent("highlightCheckbox" & i).checked = FindComponent("extrudeRadio" & i).checked
			if i < 4 then
				FindComponent("sideHighlight" & i).checked = FindComponent("extrudeRadio" & i).checked
			end if
		next
	end if
	PiesPositionRadio(Sender)
	NumPiesComboChange(Sender)
	BACKGROUND_CHECKBOX_EVENT(Sender)
End Sub

Sub PiesPositionRadio(Sender)
	MAIN_BG.ActiveLayerId = "2001x"
	if PiesPicLeft.checked then
		PieTitleLR.visible = true
		PieTitleCenter.visible = false
		
		fsPlateControl.ActiveLayerId = "2011x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2241x"
		
' hide headshots and controls
		HeadshotsChoice.visible = true
		rightHeadshots.visible = true
		leftHeadshots.visible = false
		
' set pie type
		if revealRadio.checked then
			pieSizeControl.ActiveLayerId = "1115b"
		else
			pieSizeControl.ActiveLayerId = "1115x"
		end if
		
' set title border
		titleBannerControl.Text = "4"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitle25"))
		
	elseif PiesCenter.checked then
		bgImageCheckbox.checked = false
		PieBGimage.visible = false
		
		PieTitleCenter.visible = true
		PieTitleLR.visible = false
		
		fsPlateControl.ActiveLayerId = "2014x"
		backPlateControl.ActiveLayerId = "2220x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		
' hide headshots and controls
		HeadshotsChoice.visible = false
		rightHeadshots.visible = false
		leftHeadshots.visible = false
		
' set pie type
		if revealRadio.checked then
			pieSizeControl.ActiveLayerId = "1114b"
		else
			pieSizeControl.ActiveLayerId = "1114x"
		end if
		
' set title border
		titleBannerControl.Text = "3"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitle30"))
		
	else
		PieTitleLR.visible = true
		PieTitleCenter.visible = false
		
		fsPlateControl.ActiveLayerId = "2012x"
		leftPlateControl.ActiveLayerId = "2251x"
		rightPlateControl.ActiveLayerId = "2240x"
		
' hide headshots and controls
		HeadshotsChoice.visible = true
		rightHeadshots.visible = false
		leftHeadshots.visible = true
		
' set pie type
		if revealRadio.checked then
			pieSizeControl.ActiveLayerId = "1115b"
		else
			pieSizeControl.ActiveLayerId = "1115x"
		end if
		
' set title border
		titleBannerControl.Text = "4"
		
' set title text
		TITLE_CHANGED(FindComponent("PieTitle25"))
	end if
	HeadshotRadio(Sender)
	if bgImageCheckbox.checked then
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		HeadshotsChoice.visible = false
		rightHeadshots.visible = false
		leftHeadshots.visible = false
	end if
	if liveVideoCheckbox.checked then
		LIVE_VIDEO_CHECKED(Sender)
	end if

	leftHeadshot1.enabled = true
	leftHeadshot2.enabled = true
	rightHeadshot1.enabled = true
	rightHeadshot2.enabled = true
End Sub

Sub PieTitle25Change(Sender)
	PieTitleLR.Caption = "Pie Title - Characters Left: " & PieTitle25.MaxLength - Len(PieTitle25.Text)
	TITLE_CHANGED(FindComponent("PieTitle25"))
end Sub

Sub PieTitle30Change(Sender)
	PieTitleCenter.Caption = "Pie Title - Characters Left: " & PieTitle30.MaxLength - Len(PieTitle30.Text)
	TITLE_CHANGED(FindComponent("PieTitle30"))
End sub

Sub PieSubTxtChange(Sender)
	PieSubLabel.Caption = "Subtitle - Characters Left: " & PieSubTxt.MaxLength - Len(PieSubTxt.Text)
End sub

Sub PieSourceTxtChange(Sender)
	PieSourceLabel.Caption = "Source - Characters Left: " & PieSourceTxt.MaxLength - Len(PieSourceTxt.Text)
	
End sub

Sub ChartLabelPieTxtChange(Sender)
	PieChartLabel.Caption = "Chart Label - Characters Left: " & ChartLabelPieTxt.MaxLength - Len(ChartLabelPieTxt.Text)
End sub

Sub ImageOrTitle(Sender)
	if imageOrTitleCheckbox.checked = true then
		titleTypeControl.Text = "1"
		GetTitleImagePie.visible = true
		PieTitleLR.visible = false
		PieTitleCenter.visible = false
		
	else
		titleTypeControl.Text = "0"
		imageOrTitleCheckbox.checked = false
		GetTitleImagePie.visible = false
		PieTitleLR.visible = true
		PieTitleCenter.visible = true
	End if
end sub

Sub PieColorComboChange (Sender)
	svAL =  PieColorCombo.UTF8Text
	Select Case svAL
	Case "Default"
		PieDefaultColor1.Visible = true
		PieDefaultColor2.Visible = true
		PieDefaultColor3.Visible = true
		PieDefaultColor4.Visible = true
		PieDefaultColor5.Visible = true
		PiePolColor1.visible = false
		PiePolColor2.visible = false
		PiePolColor3.visible = false
		PiePolColor4.visible = false
		PiePolColor5.visible = false
		
		for i = 1 to 5
			FindComponent("materialColor" & CStr(i)).picFilename = FindComponent("defaultColor" & CStr(i)).picFilename
			FindComponent("materialColor" & CStr(i)).thumbFilename = FindComponent("defaultColor" & CStr(i)).picFilename
		next
		
	Case "Political"
		PiePolColor1.visible = true
		PiePolColor2.visible = true
		PiePolColor3.visible = true
		PiePolColor4.visible = true
		PiePolColor5.visible = true
		PieDefaultColor1.Visible = false
		PieDefaultColor2.Visible = false
		PieDefaultColor3.Visible = false
		PieDefaultColor4.Visible = false
		PieDefaultColor5.Visible = false
		
		for i = 1 to 5
			FindComponent("materialColor" & CStr(i)).picFilename = FindComponent("politicalColor" & CStr(i)).picFilename
			FindComponent("materialColor" & CStr(i)).thumbFilename = FindComponent("politicalColor" & CStr(i)).picFilename
		next
	end select
end sub

Sub LabelSlice1Change(Sender)
	PieLabel1.Caption = "Label - Characters Left: " & LabelSlice1.MaxLength - Len(LabelSlice1.Text)
End sub

Sub LabelSlice2Change(Sender)
	PieLabel2.Caption = "Label - Characters Left: " & LabelSlice2.MaxLength - Len(LabelSlice2.Text)
End sub

Sub LabelSlice3Change(Sender)
	PieLabel3.Caption = "Label - Characters Left: " & LabelSlice3.MaxLength - Len(LabelSlice3.Text)
End sub

Sub LabelSlice4Change(Sender)
	PieLabel4.Caption = "Label - Characters Left: " & LabelSlice4.MaxLength - Len(LabelSlice4.Text)
End sub

Sub LabelSlice5Change(Sender)
	PieLabel5.Caption = "Label - Characters Left: " & LabelSlice5.MaxLength - Len(LabelSlice5.Text)
End sub



Sub PTBlaunchClick(Sender)
	
	set wshShell = CreateObject("WSCript.shell")
	wshshell.run "PTPlugin.exe"
	set wshshell = nothing
	
End sub

Sub BACKGROUND_CHECKBOX_EVENT(Sender)
	if bgImageCheckbox.checked then
		HeadshotsChoice.visible = false
		rightHeadshots.visible = false
		leftHeadshots.visible = false
		PieBGimage.visible = true
		backPlateControl.ActiveLayerId = "2221x"
		
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		
	else
		HeadshotsChoice.visible = true
		PiesPositionRadio(Sender)
		backPlateControl.ActiveLayerId = "2220x"
		PieBGimage.visible = false
	end if
End sub

Sub LIVE_VIDEO_CHECKED(Sender)
	if liveVideoCheckbox.checked then
' all out and invisible
		leftHeadshots.visible = false
		rightHeadshots.visible = false
		HeadshotsChoice.visible = false
		PieBGimage.visible = false
		bgImageCheckbox.visible = false
		
		backPlateControl.ActiveLayerId = "2220x"
		leftPlateControl.ActiveLayerId = "2250x"
		rightPlateControl.ActiveLayerId = "2240x"
		
' in - live
		MAIN_BG.ActiveLayerId = "2000x"
		
	else
' all in and visible
		HeadshotsChoice.visible = true
		PieBGimage.visible = true
		bgImageCheckbox.visible = true
		
' out - live
		MAIN_BG.ActiveLayerId = "2001x"
		
		
		PiesPositionRadio(Sender)
		BACKGROUND_CHECKBOX_EVENT(Sender)
		
	end if
End sub

Sub EXTRUDE_CHECKBOX_CLICK(Sender)
	Dim index
	index = Right(Sender.Name, 1)
	
	if index = "0" then
		extrusionControl.Value = -1
	else
		extrusionControl.Value = CInt(index) - 1
	end if
	
	if index = 0 then
			SetChecked "highlightCheckbox", false, 1, 5
			SetChecked "sideHighlight", false, 1, 3
	else
			SetChecked "highlightCheckbox", false, 1, CInt(index) - 1
			SetChecked "highlightCheckbox", true, CInt(index), CInt(index)
			SetChecked "highlightCheckbox", false, CInt(index) + 1, 5
			
			if index < 4 then
				SetChecked "sideHighlight", false, 1, CInt(index) - 1
				SetChecked "sideHighlight", true, CInt(index), CInt(index) 
				SetChecked "sideHighlight", false, CInt(index) + 1, 3
			end if
	end if
	
End sub


Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

Sub SetChecked(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).checked = value
	next
End Sub

' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
	for i=start to finish
		ConnectVTWEvent [_scripter], FindComponent(componentName & CStr(i)), eventType, eventName
	next
End Sub
