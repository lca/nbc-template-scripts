' msgbox GetIntFromString("123tomato4")



'======================================================================================================
'===================================== ORACLE TOOLS ===================================================
'======================================================================================================

'SAVE NAME CONVENTION:
'Template Name + Number of Pages + Title + Subtitle'

Dim templateName : templateName = ""

Function SC_SaveName
		Dim prefix : prefix = ""
		SC_SaveName = "FS_QUOTE"

		if textTitleRadio.checked then
				SC_SaveName = SC_SaveName & " " & pageNumOmo.text & " " & titleText.Text & " " & subtitleText.Text
		else
				SC_SaveName = SC_SaveName & " " & pageNumOmo.text
		end if

		nextValue = GetSaveCount()

' split old save name to get the generated # prefix
		oldSaveNameSplit = Split(SCV_OldSaveName, "_")

		if SCV_OldSaveName <> "" then
' if the template has already been saved once, use the old prefix
				SC_SaveName = oldSaveNameSplit(0) & "_" & oldSaveNameSplit(1) & "_" & SC_SaveName
				newTemplate = false
		else
' if the template is new then generate a new prefix
			nextValue = nextValue + 1

			if Len(nextValue) < 5 then
				select case Len(nextValue)
					case 1
						prefix = "0000"
					case 2
						prefix = "000"
					case 3
						prefix = "00"
					case 4
						prefix = "0"
				end select
			end if

			SC_SaveName = GetWeekday() & "_" & prefix & nextValue & "_" & SC_SaveName
		end if

' increments db sequence value
		'IncrementSequenceValue()
end function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
		connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

		newId = "00001"
		Set DBConnection = CreateObject("ADODB.Connection")
		DBConnection.Open connectStr

		newId = 0

		Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

		Set recordset = DBConnection.Execute(Query)

		if not recordset.EOF then
				newId = recordset("ID")
		end if

		GetSaveCount = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
		connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

		newId = "00001"
		Set DBConnection = CreateObject("ADODB.Connection")
		DBConnection.Open connectStr

		newId = 0

		Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

		Set recordset = DBConnection.Execute(Query)

		if not recordset.EOF then
				newId = recordset("ID")
		end if

		IncrementSequenceValue = CStr(newId)
End Function

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
	Dim number : number = ""
	Dim prefix : prefix = "_"
    if Len(savedOver) = 0 or savedOver = True then
		'msgbox CStr(saveId) & " - OVERWRITE HIT"
    elseif savedOver = false then
		'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
		
		
		' if not a new template then overwrite the recently saved element with a 
		if not newTemplate then
			oldSaveNameSplit = Split(SCV_OldSaveName, "_")
			oldSaveName = SCV_OldSaveName
			number = CStr(GetSaveCount() + 1)
			
			if Len(number) < 5 then
				select case Len(number)
					case 1
						prefix = "_0000"
					case 2
						prefix = "_000"
					case 3
						prefix = "_00"
					case 4
						prefix = "_0"
				end select
			end if

			if textTitleRadio.checked then
				newSaveName = GetWeekday() & prefix & number & "_" & templateName & " " & pageNumOmo.text & " " & titleText.Text & " " & subtitleText.Text
			else
				newSaveName = GetWeekday() & prefix & number & "_" & templateName & " " & pageNumOmo.text
			end if

			
			
			'msgbox "Save As New hit on a previously saved template, update id"
			RenameDatabaseEntry newSaveName, oldSaveName
		end if
		
		IncrementSequenceValue()
    end if

End Sub

Function RenameDatabaseEntry(newSaveName, oldSaveName)
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	' get id of the most recently saved element that matches this description
	Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
	

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
		newId = recordset("DNR")
		
		'msgbox "FOUND DNR: " & newId
		' update that element
		Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
		
		msgbox "QUERY: " & Query
		
		DBConnection.Execute(Query)
	end if
End Function


' ===========================================================================================
' ================================== HELPER FUNCTIONS =======================================
' ===========================================================================================


' ---------------------------------------------------------------
' -------- RETURNS 2 LETTER WEEKDAY ABBREV ----------------------
' ---------------------------------------------------------------
Function GetWeekday()
	Dim dayAbbv : dayAbbv = Array("N/A", "SU", "MO", "TU", "WE", "TH", "FR", "SA")
	GetWeekday = dayAbbv(Weekday(Date))
End Function

Sub OnMaterialClick(Sender)
	this.VizStartDir  = "MATERIAL*VIZRT_DEV/_GLOBALS/MATERIALS/"
	' Add one of these depending on material	"POLITICS/" : "NORMAL/"
End Sub

' SETS THE VISIBILITY OF A RANGE OF COMPONENTS
Sub SetVisibility(componentName, value, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).visible = value
	next
End Sub

' CLEARS THE TEXT OF A RANGE OF COMPONENTS
Sub ClearTexts(componentName, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Text = ""
	next
End Sub


' APPLY AN EVENT TO A RANGE OF COMPONENTS
Sub ApplyEvent(componentName, eventName, eventType, start, finish)
	Dim comp
	for i=start to finish
		comp = FindComponent(componentName & CStr(i))
		ConnectVTWEvent [_scripter], comp, eventType, eventName
	next
End Sub

' CHARACTER COUNT MONITOR
Sub CharsLeft(Sender)
	FindComponent(Sender.Name & "_CharCount").Caption = "Characters Left: " & Sender.MaxLength - Len(Sender.Text)
End Sub

' ADD HINTS
Sub AddHints(componentName, hint, start, finish)
	for i=start to finish
		FindComponent(componentName & CStr(i)).Hint = hint
	next
End Sub

' RIPS ALL INTEGER FROM A STRING IF SOME ARE FOUND
Function GetIntFromString(inputString)
	Dim resultString : resultString = ""
	Dim currentChar

	for i=0 to Len(inputString) - 1
		currentChar = Mid(inputString, i+1, 1)
		if Asc(currentChar) > 47 and Asc(currentChar) < 58 then
			resultString = resultString + currentChar
		end if
	next

	GetIntFromString = resultString
End Function

Sub UpdateCases(Sender)
    ' list of text fields referenced by 'global' functions
    Dim textFields : textFields = Array("BaseballCardLineTextA001", "BaseballCardLineTextB001", _
                                        "BaseballCardLineTextC001", "BaseballCardLineTextD001", _
                                        "BaseballCardLineTextA002", "BaseballCardLineTextB002", _
                                        "BaseballCardLineTextC002", "BaseballCardLineTextD002", _
                                        "BaseballCardLineTextA003", "BaseballCardLineTextB003", _
                                        "BaseballCardLineTextC003", "BaseballCardLineTextD003", _
                                        "BaseballCardLineTextA004", "BaseballCardLineTextB004", _
                                        "BaseballCardLineTextC004", "BaseballCardLineTextD004", _
                                        "BaseballCardLineTextA005", "BaseballCardLineTextB005", _
                                        "BaseballCardLineTextC005", "BaseballCardLineTextD005", _
                                        "BaseballCardLineTextA006", "BaseballCardLineTextB006", _
                                        "BaseballCardLineTextC006", "BaseballCardLineTextD006", _
                                        "BaseballCardLineTextA007", "BaseballCardLineTextA008", _
                                        "BaseballCardLineTextA009", "BaseballCardLineTextA010", _
                                        "BaseballCardLineTextA011", "BaseballCardLineTextA012")

    Dim caseType : caseType = -1
    if upperCaseRadio.checked then
        caseType = 1
    elseif normalCaseRadio.checked then
        caseType = 0
    end if

    for i = 0 to UBound(textFields)
        FindComponent(textFields(i)).CharCase = caseType
        if caseType = -1 then
            FindComponent(textFields(i)).Text = LCase(FindComponent(textFields(i)).Text)
        end if

        'logText.Text = logText.Text & textFields(i) & vbNewLine
    next

    ' title/subtitles/source
    if includeMainTextsCheckbox.checked then
        titleText.CharCase = caseType
        if caseType = -1 then
            titleText.Text = LCase(titleText.Text)
        end if
    end if
End Sub