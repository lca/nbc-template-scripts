Dim oXMLHTTP, oStream
Dim xmlDoc, entryNodeList, titleNodeList, idNodeList, pathSplit, filenames, filenameSplit
Dim nodeIndex, uuid, fileType, host, port, requestString, curId, curTitle, curIdSplit, itemCount
Dim tryCount : tryCount = 0

host = "http://3.199.23.43"
port = ":8080"
uuid = ""

'path
'data\MSNBC\PROJECTS\SHOWS\001_ANDREA_MITCHELL\SHOW_GRAPHICS\concept=001_ANDREA_MITCHELL\FULLSCREEN\SCENES

'InitializeSearch

' INITIALIZES GH SEARCH
Sub InitializeSearch(Sender)
	' clear dropdown
	resultsDropdown.Clear
	resultsDropdown.ItemIndex = -1

	RecursiveFolderSearch pathValue.Text, searchType.UTF8Text, Sender

	'PerformSearch "/folders/", "IMAGE"
	'PerformSearch("/files/", "IMAGE")
End Sub

' SEARCHES FOR A FOLDER UUID BY PATH
' takes a comma seperated string as the 'path' and searches 'recursively' until folder is found. If no folder is found an error is thrown.
Function RecursiveFolderSearch(pathString, fileCategory, Sender)
	pathSplit = Split(pathString, "\")

	itemCount = 0
	for i=0 to UBound(pathSplit)
		uuid = PerformSearch("/folders/", pathSplit(i), uuid, fileCategory)
	next

	if uuid <> "" then
		'WScript.StdOut.Write "FOLDER UUID: " & uuid & vbNewLine
		' found uuid, get array of filenames
		filenames = PerformSearch("/files/", "", uuid, fileCategory)
		filenameSplit = Split(filenames, ",")
		'WScript.StdOut.Write "FILENAMES FOUND: " & vbNewLine & vbNewLine
		' add items to dropdown
		for i=0 to UBound(filenameSplit) - 1
			'msgbox vbTab & filenameSplit(i) & vbNewLine
			resultsDropdown.Items.Add filenameSplit(i)
			itemCount = itemCount + 1
		next
		
		resultsDropdown.ItemIndex = 0
	else
		' try again but notify (later only notify after 3rd failure)
		if tryCount = 3 then
			msgbox "Issue requesting GH data"
			tryCount = 0
			
			if itemCount = 0 then
				'resultsDropdown.Items.Add "NO RESULTS FOUND"
				'msgbox "No items found of the selected file type"
			else
				'msgbox "Found " & itemCount & " items"
			end if
		else
			tryCount = tryCount + 1
			InitializeSearch Sender
		end if
	end if
	
	
	
	
End Function

' SEARCHES THE GH
Function PerformSearch(requestType,	itemName, uuid, fileCategory)
	curId = ""

	' REST REQUEST PARAMETERS
	fileType = "?term=" & fileCategory

	' rest request string
	requestString = host & port & requestType' & uuid' & fileType

	' if uuid is not blank append it
	if uuid <> "" then
		requestString = requestString & uuid
	end if
	
	if InStr(requestType, "files") then
		requestString = requestString & fileType
	end if
	'WScript.StdOut.Write "SEARCH URL: " & requestString & vbNewLIne & vbNewLIne

	' build xml http request object
	Set oXMLHTTP = CreateObject("MSXML2.ServerXMLHTTP.6.0")

	oXMLHTTP.Open "GET", requestString, False, "Admin", "VizDb"
	oXMLHTTP.Send

	' if we have a successful response, process the result
	If oXMLHTTP.Status = 200 Then
		' prep and load buffer for an xml document
		Set xmlDoc = CreateObject("Msxml2.DOMDocument")
		xmlDoc.LoadXml(oXMLHTTP.responseXML.xml)

		'WScript.StdOut.Write oXMLHTTP.responseXML.xml

		Set entryNodeList = xmlDoc.getElementsByTagName("entry")

		If entryNodeList.length > 0 then
			for each node in entryNodeList
				' check if this is the node is what we are searching for
				Set titleNodeList = node.getElementsByTagName("title")
				Set idNodeList = node.getElementsByTagName("id")
				
				curTitle = titleNodeList.Item(0).Text
				
				' searching for folders
				if InStr(requestType, "folders") then
					if curTitle = itemName then
						curId = Split(idNodeList.Item(0).Text, ":")(2)
						'WScript.StdOut.Write vbTab & curTitle & " : " & curId & vbNewLIne
					end if
				else
					'searching for files
					curId = curId & curTitle & ","
				end if
			next
		end if
		
	else
		msgbox "Status Error: " & oXMLHTTP.Status
	end If

	PerformSearch = curId
End Function