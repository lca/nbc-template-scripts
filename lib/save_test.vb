Dim oldSaveNameSplit, oldSaveName, newSaveName, newTemplate
newTemplate = true

Sub InitForm()
	'CreateDialog()
End Sub

Sub CreateDialog()
	Set objExplorer = CreateObject("InternetExplorer.Application")

	objExplorer.Navigate "file:///c:\Users\luis\Documents\bitbucket\scripts\vbscript_window.htm"
	objExplorer.ToolBar = 0
	objExplorer.StatusBar = 0
	objExplorer.Width = 400
	objExplorer.Height = 250
	objExplorer.Visible = 1

	Do While (objExplorer.Document.All.ButtonClicked.Value = "")
		Wscript.Sleep 250
	Loop

	strSelection = objExplorer.Document.All.ButtonClicked.Value

	objExplorer.Quit

	Wscript.Sleep 250

	Select Case strSelection
		Case "Option 1" 
			Wscript.Echo "You selected Option 1."
		Case "Option 2" 
			Wscript.Echo "You selected Option 2."
		Case "Option 3" 
			Wscript.Echo "You selected Option 3."
		Case "Cancelled"
			Wscript.Quit
	End Select
End Sub
' ------------------------------------------
' -------- CALLED ON TEMPLATE SAVE ---------
' ------------------------------------------
Function SC_SaveName
        
        Dim nextValue
        Dim titleText, subtitleText
		
        nextValue = GetSaveCount()
		
        ' split old save name to get the generated # prefix
        oldSaveNameSplit = Split(SCV_OldSaveName, "_")

		
        if SCV_OldSaveName <> "" then
                ' if the template has already been saved once, use the old prefix
                'msgbox "Template has already been saved at least once"
                SC_SaveName = oldSaveNameSplit(0) & "_PREVIOUSLY_SAVED" 
				newTemplate = false
        else
                ' if the template is new then generate a new prefix
                'msgbox "Template is 'new'"
                SC_SaveName = (nextValue + 1) & "_NEWLY_SAVED" 
        end if

        ' increments db sequence value
        'IncrementSequenceValue()
end function

' ----------------------------------------------------------------------------
' -------- CONNECTS TO ORACLE DB TO PULL NEXT GENERATED SEQUENCE INT ---------
' ----------------------------------------------------------------------------
Function GetSaveCount()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	newId = 10000
	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	Query = "SELECT last_number as ID FROM all_sequences WHERE sequence_owner = 'PILOT' AND sequence_name = 'SEQ_FULLSCREENS'"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	GetSaveCount = CStr(newId)
End Function


' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Sub SC_AfterSave(savedId, savedOver)
    if Len(savedOver) = 0 or savedOver = True then
		'msgbox CStr(saveId) & " - OVERWRITE HIT"
    elseif savedOver = false then
		'msgbox CStr(saveId) & " - SAVE AS NEW HIT --- " & SCV_OldSaveName
		
		
		' if not a new template then overwrite the recently saved element with a 
		if not newTemplate then
			oldSaveNameSplit = Split(SCV_OldSaveName, "_")
			oldSaveName = SCV_OldSaveName
			newSaveName = CStr(GetSaveCount() + 1) & "_"
			
			for i=1 to UBound(oldSaveNameSplit)
				if i <> UBound(oldSaveNameSplit) then
					newSaveName = newSaveName & oldSaveNameSplit(i) & "_"
				else
					newSaveName = newSaveName & oldSaveNameSplit(i)
				end if
			next
			
			msgbox "Save As New hit on a previously saved template, update id"
			RenameDatabaseEntry newSaveName, oldSaveName
		end if
		
		IncrementSequenceValue()
    end if

End Sub

' ----------------------------------------------------------------------------
' ----------------------- UPDATES ORACLE DB SEQUENCE VALUE -------------------
' ----------------------------------------------------------------------------
Function IncrementSequenceValue()
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr

	newId = 10000

	Query = "SELECT SEQ_FULLSCREENS.NEXTVAL as ID FROM DUAL"

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
			newId = recordset("ID")
	end if

	IncrementSequenceValue = CStr(newId)
End Function



Function RenameDatabaseEntry(newSaveName, oldSaveName)
	connectstr = "Provider=OraOLEDB.Oracle.1;Password=pilot;Persist Security Info=True;User ID=pilot;Data Source=vizconpilot-scan.nbcuni.ge.com/vizrtdb"

	Set DBConnection = CreateObject("ADODB.Connection")
	DBConnection.Open connectStr
	
	' get id of the most recently saved element that matches this description
	Query = "SELECT DNR FROM (SELECT DNR FROM PLAKAT_DATA WHERE DESCRIPTION='" & oldSaveName & "' ORDER BY SAVED_DATE DESC) WHERE ROWNUM <= 1"
	

	Set recordset = DBConnection.Execute(Query)

	if not recordset.EOF then
		newId = recordset("DNR")
		
		'msgbox "FOUND DNR: " & newId
		' update that element
		Query = "UPDATE PLAKAT_DATA SET DESCRIPTION='" & newSaveName & "' WHERE DNR=" & newId & ""
		
		'msgbox "QUERY: " & Query
		
		DBConnection.Execute(Query)
	end if
	
	
End Function